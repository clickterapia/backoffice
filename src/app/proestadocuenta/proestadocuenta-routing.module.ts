import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProEstadoCuentaComponent } from './proestadocuenta.component';

const routes: Routes = [
    {
        path: '',
        component: ProEstadoCuentaComponent,
        data: {
            title: 'Estado de Cuenta',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Estado de Cuenta'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProEstadoCuentaRoutingModule { }
