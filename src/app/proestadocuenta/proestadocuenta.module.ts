import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// relative import
import { ProEstadoCuentaRoutingModule } from './proestadocuenta-routing.module';
import { ProEstadoCuentaComponent } from './proestadocuenta.component';

@NgModule({
    imports: [
        CommonModule,
        ProEstadoCuentaRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot()
    ],
    declarations: [
        ProEstadoCuentaComponent ]
})
export class ProEstadoCuentaModule { }
