import { Component, OnInit } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import {
  addDays,
  compareAsc
 } from 'date-fns';

import { EstadoCuenta, SesionService,
    NgbDateCustomParserFormatter, TWUtils } from '../shared';

@Component({
    selector: 'app-form',
    templateUrl: './proestadocuenta.component.html',
    styleUrls: ['./proestadocuenta.component.scss'],
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        SesionService]
})

export class ProEstadoCuentaComponent implements OnInit {
    closeResult: string;
    modalMessage: string;
    modalTitle = 'Estado de Cuenta';

    selectedID = '';
    selectedEntity: EstadoCuenta;
    fecInicial: {};
    fecFinal: {};
    minDate: {};
    minDateFin: {};
    cantidad = '';
    monto = '';

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private sesionService: SesionService) {
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        let now = new Date();
        now.setFullYear(now.getFullYear() - 2);
        this.minDate = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
        now = addDays(now, 1);
        this.minDateFin = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
    }

    onGenerar() {
        this.selectedEntity = new EstadoCuenta();
        this.selectedEntity.idprofesional = this.selectedID;
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicial);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFinal);
        this.sesionService.getEstadoCuentaByIDProfesional(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    // TODO: agregar formato a cantidad y monto.
                    this.cantidad = this.selectedEntity.cantidad + '';
                    this.monto = this.selectedEntity.monto + '';
                } else {
                    this.showMessage('No se pudo obtener los datos de paquetes premium!!');
                }
            }
            ).catch(e => this.handleError(e));
    }

    changeInicio(event: any) {
        const fec = TWUtils.arrayDateToString(event);
        let now = new Date(fec);
        now = addDays(now, 1);
        this.minDateFin = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
