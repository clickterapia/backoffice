import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { DashboardAdmComponent } from './dashboard-adm/dashboard-adm.component';
import { DashboardProComponent } from './dashboard-pro/dashboard-pro.component';
import { DashboardPacComponent } from './dashboard-pac/dashboard-pac.component';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { Dashboard3Component } from './dashboard3/dashboard3.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardRoutes } from './dashboard.routing';
import { ChartistModule} from 'ng-chartist';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AvatarModule } from 'ngx-avatar';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { CountdownModule } from 'ngx-countdown';

import { FiltrosComponent } from './dashboard-adm/components';
import { SinAprobarComponent } from './dashboard-adm/components';
import { MensajesComponent } from './dashboard-adm/components';

import { RegresivaComponent } from './dashboard-pro/components';
import { ProximasComponent } from './dashboard-pro/components';
import { RendimientoComponent } from './dashboard-pro/components';
import { MensajesProComponent } from './dashboard-pro/components';
import { ResumenesComponent } from './dashboard-pro/components';

import { BannersComponent, ResumenesPacComponent } from './dashboard-pac/components';
import { RegresivaPacComponent } from './dashboard-pac/components';
import { UltimasComponent } from './dashboard-pac/components';
import { MensajesPacComponent } from './dashboard-pac/components';

import { IncomeCounterComponent } from './dashboard-components/income-counter/income-counter.component';
import { ProjectCounterComponent } from './dashboard-components/project-counter/project-counter.component';
import { ProjectComponent } from './dashboard-components/project/project.component';
import { RecentcommentComponent } from './dashboard-components/recent-comment/recent-comment.component';
import { RecentmessageComponent } from './dashboard-components/recent-message/recent-message.component';
import { SocialSliderComponent } from './dashboard-components/social-slider/social-slider.component';
import { TodoComponent } from './dashboard-components/to-do/todo.component';
import { ProfileComponent } from './dashboard-components/profile/profile.component';
import { PageAnalyzerComponent } from './dashboard-components/page-analyzer/pa.component';
import { WidgetComponent } from './dashboard-components/widget/widget.component';
import { CustomerSupportComponent } from './dashboard-components/customer-support/cs.component';
import { TotalEarningComponent } from './dashboard-components/total-earnings/te.component';
import { FeedsComponent } from './dashboard-components/feeds/feeds.component';
import { EarningComponent } from './dashboard-components/earning-report/earning-report.component';
import { ActivityComponent } from './dashboard-components/activity-timeline/activity.component';

registerLocaleData(localeEs);

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        NgbModule,
        ChartsModule,
        ChartistModule,
        Ng2SmartTableModule,
        AvatarModule,
        RouterModule.forChild(DashboardRoutes),
        CountdownModule
    ],
        declarations: [
        DashboardAdmComponent,
        DashboardProComponent,
        DashboardPacComponent,
        Dashboard1Component,
        Dashboard2Component,
        Dashboard3Component,

        FiltrosComponent,
        SinAprobarComponent,
        MensajesComponent,

        RegresivaComponent,
        ProximasComponent,
        RendimientoComponent,
        MensajesProComponent,
        ResumenesComponent,

        BannersComponent,
        RegresivaPacComponent,
        UltimasComponent,
        MensajesPacComponent,
        ResumenesPacComponent,

        IncomeCounterComponent,
        ProjectCounterComponent,
        ProjectComponent,
        RecentcommentComponent,
        RecentmessageComponent,
        SocialSliderComponent,
        TodoComponent,
        ProfileComponent,
        PageAnalyzerComponent,
        WidgetComponent,
        CustomerSupportComponent,
        TotalEarningComponent,
        FeedsComponent,
        EarningComponent,
        ActivityComponent
    ]
})
export class DashboardModule { }
