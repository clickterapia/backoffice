import { Component, OnInit } from '@angular/core';
import {
    FiltroUsado, FiltroUsadoService } from '../../../../shared';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
@Component({
    selector: 'app-filtros',
    templateUrl: './filtros.component.html',
    providers: [ FiltroUsadoService ]
})
export class FiltrosComponent implements OnInit {
    public modalTitle = 'Dashboard adminisrativo';
    selectedEntity: FiltroUsado;
    porcSexo = 0;
    porcEdad = 0;
    porcEspecialidad = 0;
    porcHorario = 0;
    total = 0;

    constructor(
        private modal: Modal,
        private filtroUsadoService: FiltroUsadoService) {
            this.selectedEntity = new FiltroUsado();
    }

    public ngOnInit(): void {
        this.getData();
    }

    getData() {
        return this.filtroUsadoService.getList().then(
            response => {
                if (response) {
                    this.selectedEntity = response[0];
                    this.total = this.selectedEntity.total;
                    this.porcSexo = this.selectedEntity.sexo * 100 / this.total;
                    this.porcEdad = this.selectedEntity.edad * 100 / this.total;
                    this.porcEspecialidad = this.selectedEntity.especialidad * 100 / this.total;
                    this.porcHorario = this.selectedEntity.horario * 100 / this.total;
                } else {
                    this.selectedEntity = new FiltroUsado();
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
