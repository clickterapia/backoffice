import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef
} from '@angular/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { TWUtils, CurrencyAR, NgbDateCustomParserFormatter,
    ProfesionalService } from '../../../../shared';
import { Router } from '@angular/router';

@Component({
    selector: 'app-sinaprobar',
    // styleUrls: ['./sinaprobar.component.scss'],
    templateUrl: './sinaprobar.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        ProfesionalService
    ]
})
export class SinAprobarComponent implements OnInit {
    public modalTitle = 'Sin aprobar';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataProfesionales: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            titulo: { title: 'Título', filter: false, },
            apellido: { title: 'Apellido', filter: false, },
            nombre: { title: 'Nombre', filter: false, },
            // especialidades: { title: 'Especialidades', filter: false, },
            fecregistro: {
                title: 'Registro', filter: false,
                type: 'html',
                width: '20%',
                valuePrepareFunction: (value) => {
                    if (!TWUtils.isNullDate(value)) {
                        return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                    }
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(
        public router: Router,
        public modal: Modal,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private profesionalService: ProfesionalService) {
    }

    public ngOnInit(): void {
        this.getProfesionales();
    }

    getProfesionales(): any {
        return this.profesionalService.getPendientes().then(
            response => {
                if (response) {
                    this.dataProfesionales = response;
                } else {
                    this.dataProfesionales = Array<any>();
                }
                this.source = new LocalDataSource(this.dataProfesionales);
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/admprofesional', event.data.id]);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
