import { Component, OnInit } from '@angular/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ProfesionalService, Profesional  } from '../../shared';

@Component({
    templateUrl: './dashboard-pro.component.html',
    styleUrls: ['./dashboard-pro.component.scss'],
    providers: [ ProfesionalService ]
})
export class DashboardProComponent implements OnInit {
    public modalTitle = 'Dashboard Prefesionales';
    subtitle: string;
    selectedEntity: Profesional;
    selectedID;
    constructor(
        private modal: Modal,
        private profesionalService: ProfesionalService) {
            this.selectedEntity = new Profesional();
        this.subtitle = 'This is some text within a card block.';
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    getData() {
        return this.profesionalService.getByID(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                } else {
                    this.selectedEntity = new Profesional();
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
