import { Component, OnInit, AfterViewChecked, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { 
    ProfesionalService,
    FraseProfesionalService,
    TWUtils, NgbDateCustomParserFormatter, Profesional } from '../../../../shared';

@Component({
    selector: 'app-rendimiento',
    // styleUrls: ['./rendimiento.component.scss'],
    templateUrl: './rendimiento.component.html',
    providers: [
        ProfesionalService,
        FraseProfesionalService,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    ]
})
export class RendimientoComponent implements OnInit, OnDestroy { // , AfterViewChecked {
    public modalTitle = 'Rendimiento profesional';

    selectedID: any;
    frase: string;
    profesional: Profesional;
    visualizaciones: string;

    dataFrasesProfesionales;
    dataReport;

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        barThickness : 5
    };

    public barChartLabels: string[] = [
        '2011',
        '2012',
        '2013',
        '2014',
        '2015',
        '2016',
        '2017'
    ];
    public barChartType = 'bar';
    public barChartLegend = true;

    public barChartData: any[] = [
        { data: [], label: 'Pagos' },
    ];
    public barChartColors: Array<any> = [
        {backgroundColor: '#55ce63'},
    ];

    public constructor(public router: Router,
        public modal: Modal,
        private datepipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private profesionalService: ProfesionalService,
        private fraseProfesionalService: FraseProfesionalService) {
            this.profesional = new Profesional();
            this.visualizaciones = '0';
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    ngOnDestroy() {
        this.cdRef.detach();
    }

    // ngAfterViewChecked() {
    // }

    public getData() {
        const p0 = this.fraseProfesionalService.getList();
        const p1 = this.profesionalService.getByID(this.selectedID);
        Promise.all([p0, p1]).then( ([
            fraseProfesional, profesional]) => {
            this.dataFrasesProfesionales = fraseProfesional;
            this.profesional = profesional;
            this.getRendimiento()
        }).catch(e => this.handleError(e));
    }

    getRendimiento() {
        if (this.selectedID === undefined) {
            return;
        }
        const fecHasta = TWUtils.stringDateToJson(this.datepipe.transform(new Date(2017, 1, 1), 'yyyy-MM-dd'));
        const fecDesde = TWUtils.stringDateToJson(this.datepipe.transform(new Date(), 'yyyy-MM-dd'));
        const desde = TWUtils.arrayDateToStringISO(fecHasta);
        const hasta = TWUtils.arrayDateToStringISO(fecDesde);
        return this.profesionalService.getReporteRendimientoProfesional(this.selectedID, desde, hasta).then(
            response => {
                if (response) {
                    this.dataReport = response;
                    this.processData();
                } else {
                    this.showMessage('No se han podido obtener datos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    processData() {
        const data = Array<any>();
        const labels = Array<any>();
        this.dataReport.forEach(item => {
            data.unshift(item.cantidad);
            labels.unshift('$ ' + item.precio);
        });

        if (this.dataFrasesProfesionales.length > 0) {
            this.frase = this.dataFrasesProfesionales[0].frase;
        }
        this.barChartData[0] = { data: data, label: 'Pagos' };
        this.barChartLabels = labels;
        this.cdRef.detectChanges();
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
