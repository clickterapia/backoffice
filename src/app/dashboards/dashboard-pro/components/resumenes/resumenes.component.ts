import { Component, OnInit } from '@angular/core';
import {
    Resumenes, ProfesionalService } from '../../../../shared';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
@Component({
    selector: 'app-resumenes',
    templateUrl: './resumenes.component.html',
    providers: [ ProfesionalService ]
})
export class ResumenesComponent implements OnInit {
    public modalTitle = 'Dashboard adminisrativo';
    selectedID;
    selectedEntity: Resumenes;

    constructor(
        private modal: Modal,
        private profesionalService: ProfesionalService) {
            this.selectedEntity = new Resumenes();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    getData() {
        return this.profesionalService.getResumenes(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                } else {
                    this.selectedEntity = new Resumenes();
                }
            }
        ).catch(e => this.handleError(e));
    }

    hasAusencias() {
        return +this.selectedEntity.ausencias > 0;
    }

    hasIncompleto() {
        return +this.selectedEntity.incompleto > 0;
    }

    hasPlazo() {
        return +this.selectedEntity.plazo > 0;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
