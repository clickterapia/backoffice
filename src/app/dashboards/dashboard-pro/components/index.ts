export * from './proximas/proximas.component';
export * from './regresiva/regresiva.component';
export * from './rendimiento/rendimiento.component';
export * from './mensajes/mensajespro.component';
export * from './resumenes/resumenes.component';
