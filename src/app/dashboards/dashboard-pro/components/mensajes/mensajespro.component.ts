import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { NgbModal, ModalDismissReasons, NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { Mensaje, MensajeService } from '../../../../shared';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-mensajespro',
  templateUrl: './mensajespro.component.html',
  styleUrls: ['./mensajespro.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ MensajeService ]
})

export class MensajesProComponent implements OnInit  {
    public modalTitle = 'Mensajes';
    modalRef: NgbModalRef;
    closeResult: string;
    dataMensajes: Array<any> = Array<any>();

    selectedIDProfesional: string;
    selectedID: string;
    selectedEntity: Mensaje;
    messageOpen = false;
    newMensajeToSend = '';
    newMensajeDestino = '';
    bloqueo = true;

    constructor(
        public router: Router, private route: ActivatedRoute,
        private datepipe: DatePipe,
        private sanitizer: DomSanitizer,
        public modal: Modal,
        private mensajeService: MensajeService,
        private modalService: NgbModal) {}

  ngOnInit(): void {
    // TODO: agregar recuperación de estado de bloqueo Profesional a paciente.
    // TODO: agregar recuperación de estado de bloqueo paciente a profesional.
    // TODO: bloquear envío de mensajes en caso de que el paciente haya bloqueado al profesional.
    this.selectedID = localStorage.getItem('user');
    this.route.params.subscribe(params => {
        // this.selectedIDProfesional =  params['id'];
        this.getMessages();
    });
  }

  getMessages(): void {
    this.mensajeService.getListProfesional(this.selectedID)
      .then(response => {
          this.dataMensajes = response;
          if (this.dataMensajes.length > 0) {
            this.selectedEntity = this.dataMensajes[0];
          }
    }).catch(e => this.handleError(e));
  }

  getSafeURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  onSelect(message: Mensaje): void {
    this.selectedEntity = message;
    this.newMensajeDestino = (+this.selectedEntity.tipoorigen === 2) ? this.selectedEntity.destino : this.selectedEntity.origen;
  }

  // This is for the email compose
  open2(content) {
    this.modalRef = this.modalService.open(content);
    this.modalRef.result
      .then((result) => {
          this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  isFromAdm(tipoorigen) {
    return +tipoorigen === 0;
  }

  isFromPac(tipoorigen) {
    return +tipoorigen === 1;
  }

  isFromPro(tipoorigen) {
    return +tipoorigen === 2;
  }

  public onSave() {
    const newMensaje = new Mensaje();
    newMensaje.fechahora = this.datepipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    newMensaje.idorigen = this.selectedID;
    newMensaje.tipodestino = (+this.selectedEntity.tipoorigen === 2) ? this.selectedEntity.tipodestino : this.selectedEntity.tipoorigen;
    newMensaje.tipoorigen = 2;
    newMensaje.mensaje = this.newMensajeToSend;
    newMensaje.leido = 0;
    newMensaje.iddestino = (+this.selectedEntity.tipoorigen === 2) ? this.selectedEntity.iddestino : this.selectedEntity.idorigen;

    this.mensajeService.save(newMensaje).then(
      response => {
        if (response) {
          this.showMessage('Los cambios se guardaron correctamente');
          this.modalRef.close();
          this.getMessages();
        } else {
          this.showMessage('No se ha podido guardar los datos filiatorios!!');
        }
      }
    ).catch(e => this.handleError(e));
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  showMessage(message) {
    const msg = `<p class="font-weight-normal">` + message + `</p>`;
    const dialogRef = this.modal.alert()
              .size('sm')
              .showClose(false)
              .isBlocking(true)
              .title(this.modalTitle)
              .body(msg)
              .okBtn('Aceptar')
              .open();
  }

  private handleError(error: any): Promise<any> {
      this.showMessage('Error: ' + error);
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
  }
}
