import { Routes } from '@angular/router';

import { DashboardAdmComponent } from './dashboard-adm/dashboard-adm.component';
import { DashboardPacComponent } from './dashboard-pac/dashboard-pac.component';
import { DashboardProComponent } from './dashboard-pro/dashboard-pro.component';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { Dashboard3Component } from './dashboard3/dashboard3.component';

export const DashboardRoutes: Routes = [
  {
    path: '',
    children: [
    {
      path: 'dashboard-adm',
      component: DashboardAdmComponent,
      data: {
        title: 'Dashboard Administrativo',
        urls: [{title: 'Dashboard', url: '/dashboard-adm'}, {title: 'Dashboard Administrativo'}]
      }
    },
    {
      path: 'dashboard-pac',
      component: DashboardPacComponent,
      data: {
        title: 'Dashboard Pacientes',
        urls: [{title: 'Dashboard', url: '/dashboard-pac'}, {title: 'Dashboard Pacientes'}]
      }
    },
    {
      path: 'dashboard-pro',
      component: DashboardProComponent,
      data: {
        title: 'Dashboard Profesional',
        urls: [{title: 'Dashboard', url: '/dashboard-pro'}, {title: 'Dashboard Profesional'}]
      }
    },
    {
      path: 'dashboard1',
      component: Dashboard1Component,
      data: {
        title: 'Modern Dashboard',
        urls: [{title: 'Dashboard', url: '/dashboard'}, {title: 'Modern Dashboard'}]
      }
    }, {
      path: 'dashboard2',
      component: Dashboard2Component,
      data: {
        title: 'Classic Dashboard',
        urls: [{title: 'Dashboard', url: '/dashboard'}, {title: 'Classic Dashboard'}]
      }
    }, {
      path: 'dashboard3',
      component: Dashboard3Component,
      data: {
        title: 'Analytical Dashboard',
        urls: [{title: 'Dashboard', url: '/dashboard'}, {title: 'Analytical Dashboard'}]
      }
    }]
  }
];
