import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { PacienteService, Paciente  } from '../../shared';

@Component({
    templateUrl: './dashboard-pac.component.html',
    styleUrls: ['./dashboard-pac.component.scss']
})
export class DashboardPacComponent implements OnInit, AfterViewInit {
    public modalTitle = 'Dashboard Pacientes';
    selectedEntity: Paciente;
    subtitle: string;
    selectedID;
    constructor(
        private modal: Modal,
        private pacientesService: PacienteService) {
        this.selectedEntity = new Paciente();
        this.subtitle = 'This is some text within a card block.';
    }

    ngAfterViewInit() {
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    getData() {
        return this.pacientesService.getByID(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                } else {
                    this.selectedEntity = new Paciente();
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
