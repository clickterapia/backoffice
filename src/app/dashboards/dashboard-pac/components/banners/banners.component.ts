import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { BannerService } from '../../../../shared';
import { DomSanitizer } from '@angular/platform-browser';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    selector: 'app-banners',
    templateUrl: './banners.component.html',
    providers: [ 
        NgbCarouselConfig,
        BannerService ]
})

export class BannersComponent implements OnInit {
    public modalTitle = 'Banners';
    dataBanners: Array<any> = Array<any>();

    constructor(
        config: NgbCarouselConfig,
        private sanitizer: DomSanitizer,
        public modal: Modal,
        private bannerService: BannerService) {
        config.interval = 10000;
        config.wrap = true;
        config.keyboard = false;
    }

    ngOnInit(): void {
        this.getBanners();
    }

    getBanners(): void {
        this.bannerService.getListForPacientes()
            .then(response => {
                this.dataBanners = response;
            }).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
