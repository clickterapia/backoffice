import { Component, OnInit } from '@angular/core';
import {
    ResumenesPac, PacienteService } from '../../../../shared';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
@Component({
    selector: 'app-resumenespac',
    templateUrl: './resumenespac.component.html',
    providers: [ PacienteService ]
})
export class ResumenesPacComponent implements OnInit {
    public modalTitle = 'Dashboard pacientes';
    selectedID;
    selectedEntity: ResumenesPac;

    constructor(
        private modal: Modal,
        private pacienteService: PacienteService) {
            this.selectedEntity = new ResumenesPac();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    getData() {
        return this.pacienteService.getResumenes(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                } else {
                    this.selectedEntity = new ResumenesPac();
                }
            }
        ).catch(e => this.handleError(e));
    }

    hasAusencias() {
        return +this.selectedEntity.ausencias > 0;
    }

    hasIncompleto() {
        return +this.selectedEntity.incompleto > 0;
    }

    hasPlazo() {
        return +this.selectedEntity.plazo > 0;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
