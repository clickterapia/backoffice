import {
    Component, OnInit,
    TemplateRef,
    ViewEncapsulation,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { SafePipe } from '../../../../shared';
import { LOCALE_ID } from '@angular/core';
import { SesionService, Sesion, SesionState } from '../../../../shared';
import { compareAsc } from 'date-fns';
import { Config } from 'ngx-countdown';
import * as OT from '@opentok/client';
import { CountdownComponent } from 'ngx-countdown';
import * as moment from 'moment';

import { SafeResourceUrl } from '@angular/platform-browser';

@Component({
    selector: 'app-regresivapac',
    // styleUrls: ['./regresivapac.component.scss'],
    templateUrl: './regresivapac.component.html',
    providers: [
        SafePipe,
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        SesionService
    ],
    encapsulation: ViewEncapsulation.None
})

export class RegresivaPacComponent implements OnInit {
    @ViewChild(CountdownComponent) counter: CountdownComponent;

    public modalTitle = 'Sesión Terapéutica';
    modalContent: TemplateRef<any>;

    selectedEntity = new Sesion();
    selectedID = '';
    tiempo;
    sesionUrl: SafeResourceUrl;

    config: Config;

    public constructor(public router: Router,
        public modal: Modal,
        private sp: SafePipe,
        private sesionService: SesionService) {
            this.selectedEntity = new Sesion();
            this.sesionUrl = '';
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getNextSesion();
    }

    getConfig(): Config {
        return  {
            demand: false,
            stopTime: new Date(this.selectedEntity.fecinicio).getTime(),
        };
    }

    getNextSesion() {
        return this.sesionService.getNextSesion(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    if (this.selectedEntity) {
                        if (this.hasStarted()) {
                            this.onStartSesion();
                        } else {
                            this.config = this.getConfig();
                        }
                    }
                } else {
                    this.showMessage('No existen sesiones programadas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    hasSession() {
        const estado = this.getSesionState();
        return (estado === SesionState.iniciada || estado === SesionState.noIniciada);
    }

    hasStarted() {
        return this.getSesionState() === +SesionState.iniciada;
    }

    getSesionState() {
        const fecinicio = new Date(this.selectedEntity.fecinicio);
        const fecfin = new Date(this.selectedEntity.fecfin);
        const now = new Date();
        const compinicio = compareAsc(now, fecinicio);
        const compfin = compareAsc(fecfin, now);
        if (this.selectedEntity.id.length === 0) {
            return +SesionState.inexistente;
        } else if (+compinicio >= 0) {
            if (+compfin >= 0) {
                return +SesionState.iniciada;
            } else {
                return +SesionState.finalizada;
            }
        } else {
            return +SesionState.noIniciada;
        }
    }

    getSessionDate() {
        return this.selectedEntity.fecinicio;
    }

    onStartSesion() {
        // if (this.hasStarted()) {
        //     const url = config.SESION_URL.replace('DEFAULT_ROOM', this.selectedEntity.id.toString());
        //     this.sesionUrl = this.sp.transform(url);
        // }
    }

    onIniciar() {
        this.router.navigate(['/pacsesion']);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
