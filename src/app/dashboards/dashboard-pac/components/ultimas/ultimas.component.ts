import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef
} from '@angular/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyAR, NgbDateCustomParserFormatter,
    SesionService } from '../../../../shared';
import { Router } from '@angular/router';

@Component({
    selector: 'app-ultimas',
    // styleUrls: ['./ultimas.component.scss'],
    templateUrl: './ultimas.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        CurrencyPipe,
        SesionService
    ]
})
export class UltimasComponent implements OnInit {
    public modalTitle = 'Ultimas';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataSesiones: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        hideSubHeader: true,
        pager: { display: true, perPage: 10 },
        columns: {
            profesional: { title: 'Profesional', filter: false, },
            fecinicio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
           precio:  {
                title: 'Precio',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'ARS', true, '1.2-2');
                }
            },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable header_subrayed',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(
        public router: Router,
        public modal: Modal,
        private datePipe: DatePipe,
        private cp: CurrencyPipe,
        private sesionService: SesionService) {
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getUltimas();
    }

    getUltimas(): any {
        return this.sesionService.getByIDPaciente(this.selectedID).then(
            response => {
                if (response) {
                    this.dataSesiones = response;
                } else {
                    this.dataSesiones = Array<any>();
                }
                this.source = new LocalDataSource(this.dataSesiones);
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/pacsesiondetalle', event.data.id]);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
