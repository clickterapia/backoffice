import { Routes } from '@angular/router';

import { NotFoundComponent } from './404/not-found.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';
import { LoginProComponent } from './login-pro/login-pro.component';
import { LoginPacComponent } from './login-pac/login-pac.component';
import { LoginAdmComponent } from './login-adm/login-adm.component';
import { Login2Component } from './login2/login2.component';
import { SignupComponent } from './signup/signup.component';
import { SignupProComponent } from './signup-pro/signup-pro.component';
import { SignupPacComponent } from './signup-pac/signup-pac.component';
import { Signup2Component } from './signup2/signup2.component';

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [{
      path: '404',
      component: NotFoundComponent
    }, {
      path: 'lock',
      component: LockComponent
    }, {
      path: 'login',
      component: LoginComponent
    }, {
      path: 'login-pro',
      component: LoginProComponent
    }, {
      path: 'login-pac',
      component: LoginPacComponent
    }, {
      path: 'login-adm',
      component: LoginAdmComponent
    }, {
      path: 'login2',
      component: Login2Component
    }, {
      path: 'signup',
      component: SignupComponent
    }, {
      path: 'signup-pro',
      component: SignupProComponent
    }, {
      path: 'signup-pac',
      component: SignupPacComponent
    }, {
      path: 'signup2',
      component: Signup2Component
    }]
  }
];
