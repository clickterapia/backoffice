import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from '../../shared/guard/auth.guard';
import { Profesional, ProfesionalService, AuthService } from '../../shared';

@Component({
    selector: 'app-login-pro',
    templateUrl: './login-pro.component.html',
    styleUrls: ['./login-pro.component.scss'],
    providers: [ProfesionalService, AuthService]
})
export class LoginProComponent implements OnInit, AfterViewInit {
    public error = false;
    recuerdame = false;

    constructor(public router: Router,
        private profesionalService: ProfesionalService,
        private auth0: AuthService) {}

    ngOnInit() {
        localStorage.removeItem('isSingingUp');
        localStorage.clear();
        localStorage.setItem('profile', Profile.professional.toString());
        localStorage.setItem('social', '0');
    }

    ngAfterViewInit() {
        /*$(function() {
            $(".preloader").fadeOut();
        });

        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });*/
    }

    onLoggedin(user, pass) {
        const profesional = new Profesional();
        profesional.usuario = user;
        profesional.contrasena = pass;
        this.profesionalService.login(profesional).then(
            response => {
                if (response) {
                    const id = response.id;
                    localStorage.setItem('user', id);
                    localStorage.setItem('isLoggedin', 'true');
                    localStorage.setItem('profile', Profile.professional.toString());
                    this.router.navigate(['/dashboard/dashboard-pro']);
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onLoggedinSocial(social) {
        localStorage.setItem('isSingingUp', '0');
        this.auth0.login(Profile.professional);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}
