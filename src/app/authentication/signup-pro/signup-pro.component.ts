import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import { DatePipe } from '@angular/common';
import { Profile } from '../../shared/guard/auth.guard';
import { NgbDateCustomParserFormatter, TWUtils,
    Pais, PaisService,
    TipoLogin, Profesional, ProfesionalService,
    Provincia, ProvinciaService,
    Sexo, SexoService, AuthService } from '../../shared';

@Component({
    selector: 'app-signup-pro',
    templateUrl: './signup-pro.component.html',
    styleUrls: ['./signup-pro.component.scss'],
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        PaisService, ProfesionalService,
        ProvinciaService, SexoService ]
})

export class SignupProComponent implements OnInit {
    modalTitle = 'Mensajes';

    private success = new Subject<string>();
    staticAlertClosed = false;
    errorMessage: string;

    public dataPaises: Array<any> = Array<any>();
    public dataProvincias: Array<any> = Array<any>();
    public dataProvinciasFilt: Array<any> = Array<any>();
    public dataSexos: Array<any> = Array<any>();

    public selectedEntity: Profesional;
    public repetirContrasena: string;
    public minDate: {};
    public fecNacimiento: {};
    public notificaciones: boolean;
    public terminos: boolean;

    isSocial = false;

    public constructor(public router: Router, private route: ActivatedRoute,
        private datepipe: DatePipe,
        public modal: Modal,
        private paisService: PaisService,
        private profesionalService: ProfesionalService,
        private provinciaService: ProvinciaService,
        private sexoService: SexoService,
        private auth0: AuthService) {
        this.selectedEntity = new Profesional();
        this.notificaciones = false;
        this.terminos = true;
    }

    ngOnInit() {
        this.isSocial = (+localStorage.getItem('social') === 1);
        if (this.isSocial) {
            this.selectedEntity.issocial = 1;
            this.selectedEntity.id = localStorage.getItem('user');
            this.selectedEntity.usuario = localStorage.getItem('usuario');
            this.selectedEntity.apellido = localStorage.getItem('apellido');
            this.selectedEntity.nombre = localStorage.getItem('nombre');
            this.selectedEntity.fotourl = localStorage.getItem('fotourl');
        }
        this.getData();

        setTimeout(() => this.staticAlertClosed = true, 20000);
        this.success.subscribe((message) => this.errorMessage = message);
        this.success.debounceTime(15000).subscribe(() => this.errorMessage = null);

        const now = new Date();
        now.setFullYear(now.getFullYear() - 21);
        this.minDate = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
    }

    public getData() {
        this.getPaises();
        this.getProvincias();
        this.getSexos();
    }

    public getPaises() {
        return this.paisService.getList().then(
            response => {
                if (response) {
                    this.dataPaises = response;
                    const def = new Pais();
                    def.id = -1;
                    def.pais = 'País...';
                    this.dataPaises.unshift(def);
                } else {
                    alert('No se ha podido obtener datos de países!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getProvincias() {
        return this.provinciaService.getList().then(
            response => {
                if (response) {
                    this.dataProvincias = response;
                    const def = new Provincia();
                    def.id = -1;
                    def.provincia = 'Provincia...';
                    this.dataProvincias.unshift(def);
                    this.dataProvinciasFilt = this.dataProvincias;
                } else {
                    alert('No se ha podido obtener datos de provincias!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getSexos() {
        return this.sexoService.getList().then(
            response => {
                if (response) {
                    this.dataSexos = response;
                    const def = new Sexo();
                    def.id = -1;
                    def.sexo = 'Sexo...';
                    this.dataSexos.unshift(def);
                } else {
                    alert('No se ha podido obtener datos de sexos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onPaisChange() {
        this.dataProvinciasFilt = this.dataProvincias.filter(
            prov =>
            +prov.idpais === +this.selectedEntity.idpais || +prov.idpais === -1);
        if (this.dataProvinciasFilt.length === 0) {
            alert('País sin provincias');
        }
        this.selectedEntity.idprovincia = -1;
    }

    onSignUp() {
        if (this.selectedEntity.issocial === 0) {
            if (this.selectedEntity.contrasena !== this.repetirContrasena) {
                this.showMessage('Las contraseñas no coinciden. Por favor corrija.');
                return;
            }
        } else {
            localStorage.setItem('isSingingUp', '1');
        }
        this.selectedEntity.notificacion = this.notificaciones ? 1 : 0;
        this.selectedEntity.fecnacimiento = TWUtils.arrayDateToString(this.fecNacimiento);
        this.profesionalService
            .save(this.selectedEntity)
            .then(
                response => {
                    if (response === ProfesionalService.ERROR_CLAVE_DUPLICADA) {
                        this.success.next(`Nombre de usuario no disponible.`);
                        localStorage.setItem('user', this.selectedEntity.id);
                        localStorage.setItem('isLoggedin', 'true');
                        localStorage.setItem('profile', Profile.professional.toString());
                    } else {
                        this.success.next(`Se registró correctamente.`);
                        localStorage.setItem('user', response);
                        localStorage.setItem('isLoggedin', 'true');
                        localStorage.setItem('profile', Profile.professional.toString());
                    }

                    if (this.selectedEntity.issocial === +TipoLogin.normal) {
                        this.router.navigate(['/perfilprofesional']);
                    }
                })
            .catch(e => this.handleError(e));
    }

    isValid() {
        const valid = (this.selectedEntity.idpais === -1
            || this.selectedEntity.idprovincia === -1
            || this.selectedEntity.idsexo === -1
            || this.terminos === false);
        return valid;
    }

    showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
                .size('sm')
                .showClose(false)
                .isBlocking(true)
                .title(this.modalTitle)
                .body(msg)
                .okBtn('Aceptar')
                .open();
    }

    onSignUpSocial(social) {
        this.selectedEntity.issocial = 1;
        this.onSignUp();
        this.auth0.signUp(Profile.professional);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        this.showMessage('error');
        return Promise.reject(error.message || error);
    }
}
