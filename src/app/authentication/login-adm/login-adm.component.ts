import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from '../../shared/guard/auth.guard';
import { AuthService, UsuarioService, Usuario } from '../../shared';

@Component({
    selector: 'app-login-adm',
    templateUrl: './login-adm.component.html',
    styleUrls: ['./login-adm.component.css'],
    providers: [
        UsuarioService,
        AuthService]
})
export class LoginAdmComponent implements OnInit, AfterViewInit {
    public error = false;

    constructor(public router: Router,
        private usuarioService: UsuarioService,
        private auth0: AuthService) {}

    ngOnInit() {
        localStorage.clear();
        localStorage.setItem('profile', Profile.admin.toString());
        localStorage.setItem('social', '0');
    }

    ngAfterViewInit() {
        /*$(function() {
            $(".preloader").fadeOut();
        });

        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });*/
    }

    onLoggedin(user, pass) {
        const usuario = new Usuario();
        usuario.usuario = user;
        usuario.contrasena = pass;
        this.usuarioService.login(usuario).then(
            response => {
                if (response) {
                    const id = response.id;
                    localStorage.setItem('user', id);
                    localStorage.setItem('isLoggedin', 'true');
                    localStorage.setItem('profile', Profile.admin.toString());
                    this.router.navigate(['/dashboard/dashboard-adm']);
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
