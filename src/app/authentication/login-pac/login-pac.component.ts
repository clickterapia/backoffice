import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from '../../shared/guard/auth.guard';
import { Paciente, PacienteService, AuthService } from '../../shared';

@Component({
    selector: 'app-login-pac',
    templateUrl: './login-pac.component.html',
    styleUrls: ['./login-pac.component.scss'],
    providers: [
        PacienteService,
        AuthService]
})
export class LoginPacComponent implements OnInit, AfterViewInit {
    public error = false;
    recuerdame = false;
    agendar: string;
    disponibilidad: string;

    constructor(public router: Router,
        private pacienteService: PacienteService,
        private auth0: AuthService) {}

    ngOnInit() {
        localStorage.removeItem('isSingingUp');
        if (localStorage.getItem('agendar') !== null) {
            this.agendar = localStorage.getItem('agendar');
            this.disponibilidad = localStorage.getItem('agendardisp');
        }
        localStorage.clear();
        localStorage.setItem('profile', Profile.patient.toString());
        localStorage.setItem('social', '0');
    }

    ngAfterViewInit() {
    }

    onLoggedin(user, pass) {
        const profesional = new Paciente();
        profesional.usuario = user;
        profesional.contrasena = pass;
        this.pacienteService.login(profesional).then(
            response => {
                if (response) {
                    const id = response.id;
                    localStorage.setItem('user', id);
                    localStorage.setItem('isLoggedin', 'true');
                    localStorage.setItem('profile', Profile.patient.toString());
                    if (this.agendar === undefined || this.agendar === null) {
                        this.router.navigate(['/dashboard/dashboard-pac']);
                    } else {
                        this.router.navigate(['/pacsolicitudsesion', this.agendar, this.disponibilidad]);
                    }
                } else {
                    this.error = true;
                }
            }
        ).catch(e => this.handleError(e));
    }

    onLoggedinSocial(social) {
        localStorage.setItem('isSingingUp', '0');
        this.auth0.login(Profile.patient);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
