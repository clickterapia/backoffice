import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NotFoundComponent } from './404/not-found.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';
import { LoginProComponent } from './login-pro/login-pro.component';
import { LoginPacComponent } from './login-pac/login-pac.component';
import { LoginAdmComponent } from './login-adm/login-adm.component';
import { Login2Component } from './login2/login2.component';
import { SignupComponent } from './signup/signup.component';
import { SignupProComponent } from './signup-pro/signup-pro.component';
import { SignupPacComponent } from './signup-pac/signup-pac.component';
import { Signup2Component } from './signup2/signup2.component';

import { AuthenticationRoutes } from './authentication.routing';

import { NavbarWebModule } from '../shared/navbar-web/navbar-web.module';
import { TopBarModule } from '../shared/topbar/topbar.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthenticationRoutes),
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NavbarWebModule,
    TopBarModule
  ],
  declarations: [
    NotFoundComponent,
    LoginComponent,
    LoginProComponent,
    LoginPacComponent,
    LoginAdmComponent,
    SignupComponent,
    SignupProComponent,
    SignupPacComponent,
    LockComponent,
    Login2Component,
    Signup2Component
  ]
})

export class AuthenticationModule {}
