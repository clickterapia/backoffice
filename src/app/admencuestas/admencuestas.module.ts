import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AdmEncuestasComponent } from './admencuestas.component';
import { AdmEncuestasRoutingModule } from './admencuestas-routing.module';
import { EncuestaComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    Ng2SmartTableModule,
    ScrollToModule.forRoot(),
    AdmEncuestasRoutingModule
  ],
  declarations: [
    AdmEncuestasComponent,
    EncuestaComponent
    ],
  exports: [AdmEncuestasComponent]
})
export class AdmEncuestasModule {}
