import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Encuesta, EncuestaService, EncuestaTipoService, EncuestaTipo } from '../../../shared';

@Component({
    selector: 'app-encuesta',
    templateUrl: './encuesta.component.html',
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        EncuestaService,
        EncuestaTipoService ]
})

export class EncuestaComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Encuesta;
    @Output() entityUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Encuesta';

    dataEncuestasTipos: Array<any> = Array<any>();

    selectedID: string;
    fecInicio = {};
    fecFin = {};
    apacientes: boolean;
    aprofesionales: boolean;

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private encuestaService: EncuestaService,
        private encuestaTipoService: EncuestaTipoService) {
            this.selectedEntity = new Encuesta();
            this.fecInicio = {};
            this.fecFin = {};
            this.apacientes = false;
            this.aprofesionales = false;
    }

    public ngOnInit(): void {
        this.getData();
        this.onCancelar();
    }

    getData() {
        const p0 = this.encuestaTipoService.getList();
        Promise.all([p0])
            .then(([tipos]) => {
                const seleccionar = new EncuestaTipo();
                this.dataEncuestasTipos = tipos;
                // this.dataEncuestasTipos.splice(0, 0, seleccionar);
            }, err => this.handleError(err));
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;

            if (!TWUtils.isNullDate(this.selectedEntity.fecinicio)) {
                this.fecInicio = TWUtils.stringDateToJson(this.selectedEntity.fecinicio);
            }
            if (!TWUtils.isNullDate(this.selectedEntity.fecfin)) {
                this.fecFin = TWUtils.stringDateToJson(this.selectedEntity.fecfin);
            }
            this.apacientes = (+this.selectedEntity.apacientes === 1);
            this.aprofesionales = (+this.selectedEntity.aprofesionales === 1);
        }
    }

    isBooleano() {
        const tipoEnc: EncuestaTipo = this.dataEncuestasTipos.find(tipo =>
            tipo.id === this.selectedEntity.idencuestatipo);
        return (tipoEnc !== undefined) ? (+tipoEnc.booleano === 1) : false;
    }

    hasEntity(): boolean {
        return this.selectedEntity.id !== '' && this.selectedEntity.id !== undefined;
    }

    onSave() {
        this.selectedEntity.apacientes = (this.apacientes) ? 1 : 0;
        this.selectedEntity.aprofesionales = (this.aprofesionales) ? 1 : 0;
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicio);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFin);
        if (this.selectedEntity.fecinicio.length === 0 || this.selectedEntity.fecfin.length === 0) {
            this.showMessage('Faltan completar datos!!');
            return;
        }
        this.encuestaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.selectedEntity.id = response;
                    this.entityUpdated.emit(true);
                    this.onCancelar();
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onEliminar() {
        this.encuestaService.delete(this.selectedEntity).then(
            response => {
                const resp = Number(response);
                if ( resp > 0) {
                    this.showMessage('El registro se ha eliminado correctamente');
                    this.onCancelar();
                    this.entityUpdated.emit(true);
                } else if (resp === -1) {
                    this.showMessage('El registro no se ha eliminado porque se está utilizando.');
                } else {
                    this.showMessage('El registro no se ha eliminado por error de sistema.');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onCancelar() {
        this.selectedEntity = new Encuesta();
        this.fecInicio = {};
        this.fecFin = {};
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
