import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmEncuestasComponent } from './admencuestas.component';

const routes: Routes = [
    {
        path: '',
        component: AdmEncuestasComponent,
        data: {
            title: 'Encuestas',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Encuestas'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmEncuestasRoutingModule { }
