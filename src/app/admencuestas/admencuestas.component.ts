import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyAR, NgbDateCustomParserFormatter,
    Encuesta, EncuestaService } from '../shared';

@Component({
    selector: 'app-admencuestas',
    // styleUrls: ['./admencuestas.component.scss'],
    templateUrl: './admencuestas.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        EncuestaService
    ]
})
export class AdmEncuestasComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Encuestas';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataEncuestas: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            encuesta: { title: 'Encuesta', filter: false, },
            encuestatipo: { title: 'Tipo', filter: false, },
            fecinicio: {
                title: 'Desde', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            fecfin: {
                title: 'Hasta', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(public router: Router,
        private scrollToService: ScrollToService,
        public modal: Modal,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private encuestaService: EncuestaService) {
            this.selectedEntity = new Encuesta();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getEncuestas();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        this.getEncuestas();
    }

    getEncuestas(): any {
        return this.encuestaService.getList().then(
            response => {
                if (response) {
                    this.dataEncuestas = response;
                    this.source = new LocalDataSource(this.dataEncuestas);
                } else {
                    this.showMessage('No se ha podido obtener datos de encuestas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Encuesta;
        const config: ScrollToConfigOptions = {
                target: 'item'
            };
        this.scrollToService.scrollTo(config);
    }

    onNew() {
        this.selectedEntity = new Encuesta();
        const config: ScrollToConfigOptions = {
                target: 'item'
            };
        this.scrollToService.scrollTo(config);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'encuesta', search: query },
            { field: 'encuestatipo', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
