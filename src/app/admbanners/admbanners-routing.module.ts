import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmBannersComponent } from './admbanners.component';

const routes: Routes = [
    {
        path: '',
        component: AdmBannersComponent,
        data: {
            title: 'Banners',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Banners'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmBannersRoutingModule { }
