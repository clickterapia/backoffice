import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyAR, NgbDateCustomParserFormatter, BannerService, Banner } from '../shared';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
// import { BannerComponent } from './components';

@Component({
    selector: 'app-admbanners',
    // styleUrls: ['./admbanners.component.scss'],
    templateUrl: './admbanners.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        BannerService
    ]
})
export class AdmBannersComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Banner';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataBanners: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            titulo: { title: 'Usuario', filter: false, },
            fecinicio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
            fecfin: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private cdRef: ChangeDetectorRef,
        private datePipe: DatePipe,
        private bannerService: BannerService) {
            this.selectedEntity = new Banner();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getBanners(undefined);
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        const tempEntity = this.selectedEntity;
        this.getBanners(tempEntity);
    }

    getBanners(tempEntity: any): any {
        return this.bannerService.getList().then(
            response => {
                if (response) {
                    this.dataBanners = response;
                    this.source = new LocalDataSource(this.dataBanners);
                    if (tempEntity !== undefined) {
                        this.selectedEntity = tempEntity;
                    }
                } else {
                    this.showMessage('No se ha podido obtener datos de banners!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Banner;
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'titulo', search: query },
            { field: 'fecinicio', search: query },
            { field: 'fecfin', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
