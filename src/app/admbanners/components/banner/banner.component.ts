import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Banner, BannerService } from '../../../shared';

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.scss'],
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        BannerService ]
})

export class BannerComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Banner;
    @Output() entityUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Banner';

    public selectedID: string;
    public fecInicio = {};
    public fecFin = {};

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private cdRef: ChangeDetectorRef,
        private bannerService: BannerService) {
        this.selectedEntity = new Banner();
    }

    public ngOnInit(): void {
        this.onCancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;

            if(!TWUtils.isNullDate(this.selectedEntity.fecinicio)) {
                this.fecInicio = TWUtils.stringDateToJson(this.selectedEntity.fecinicio);
            }
            if(!TWUtils.isNullDate(this.selectedEntity.fecfin)) {
                this.fecFin = TWUtils.stringDateToJson(this.selectedEntity.fecfin);
            }
        }
    }

    isSeted(url: string) {
        if  (url === undefined) {
            return false;
        } else if (url === null) {
            return false;
        } else if (url.length < 1) {
            return false;
        }

        return true;
    }

    hasEntity(): boolean {
        return this.selectedEntity.id !== '' && this.selectedEntity.id !== undefined;
    }

    onEliminar() {
        this.bannerService.delete(this.selectedEntity).then(
            response => {
                const resp = Number(response);
                if ( resp > 0) {
                    this.showMessage('El registro se ha eliminado correctamente');
                    this.onCancelar();
                    this.entityUpdated.emit(true);
                } else if (resp === -1) {
                    this.showMessage('El registro no se ha eliminado porque se está utilizando.');
                } else {
                    this.showMessage('El registro no se ha eliminado por error de sistema.');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onCancelar() {
        this.selectedEntity = new Banner();
        this.fecInicio = {};
        this.fecFin = {};
    }

    fileChangeImagen(event: any): void {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            if (this.selectedEntity.id !== '') {
                const file: File = fileList[0];
                this.bannerService.uploadImagen(this.selectedEntity.id, file).then(
                    response => {
                        this.entityUpdated.emit(true);
                        const url = this.selectedEntity.imagenurl;
                        this.selectedEntity.imagenurl = '';
                        this.cdRef.detectChanges();
                        this.selectedEntity.imagenurl = url;
                        this.cdRef.detectChanges();
                    }).catch(e => this.handleError(e));
            } else {
                this.showMessage('Primero debe guardar antes de ingresar imagen.');
            }
        }
    }

    onSave() {
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicio);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFin);
        if (this.selectedEntity.fecinicio.length === 0 || this.selectedEntity.fecfin.length === 0) {
            this.showMessage('Faltan completar datos!!');
            return;
        }
        this.bannerService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.selectedEntity.id = response;
                    this.entityUpdated.emit(true);
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
