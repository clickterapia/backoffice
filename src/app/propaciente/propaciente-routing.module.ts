import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProPacienteComponent } from './propaciente.component';

const routes: Routes = [
    {
        path: '',
        component: ProPacienteComponent,
        data: {
            title: 'Paciente',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Paciente'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProPacienteRoutingModule { }
