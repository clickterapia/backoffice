import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';

import {
    Paciente, PacienteService, TWUtils } from '../../../shared';

@Component({
    selector: 'app-generales',
    templateUrl: './generales.component.html',
    styleUrls: ['./generales.component.scss'],
    providers: [
        DatePipe,
        PacienteService
    ]
})

export class GeneralesComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Paciente';

    public selectedEntity: Paciente;
    public selectedIDProfesional: string;
    public selectedID: string;
    fecPrimera: string;
    fecUltima: string;

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private pacienteService: PacienteService) {
        this.selectedEntity = new Paciente();
    }

    public ngOnInit(): void {
        this.selectedIDProfesional = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.onSetEntityData();
        });
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.pacienteService.getByIdeIdProfesional(this.selectedID, this.selectedIDProfesional).then(
                response => {
                    this.selectedEntity = response as Paciente;
                    let fecha = new Date(this.selectedEntity.primera);
                    this.fecPrimera = this.datepipe.transform(fecha, 'dd-MM-yyyy HH:mm');
                    fecha = new Date(this.selectedEntity.ultima);
                    this.fecUltima = this.datepipe.transform(fecha, 'dd-MM-yyyy HH:mm');
                }
            ).catch(e => this.handleError(e));
        }
    }

    focusOutFunction() {
        this.pacienteService.putHistorial(this.selectedEntity).then(
            response => {
                if (response) {
                } else {
                    this.showMessage('No se ha podido guardar la história clínica!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
