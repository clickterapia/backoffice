import { Component, OnInit,
    TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SesionService } from '../../../shared';

@Component({
selector: 'app-sesiones',
styleUrls: ['./sesiones.component.scss'],
templateUrl: './sesiones.component.html',
providers: [
    DatePipe,
    CurrencyPipe,
    {provide: LOCALE_ID, useValue: 'es-AR'},
    SesionService
]
})
export class SesionesComponent implements OnInit {
    public modalTitle = 'Sesiones';
    modalContent: TemplateRef<any>;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fecinicio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
            observaciones: { title: 'Observaciones', filter: false },
            precio:  {
                title: 'Precio',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'ARS', true, '1.2-2');
                }
            }
      },
      actions: { add: false, edit: false, delete: false, },
      attr: {
          class: 'table dataTable table-striped table-bordered',
      }
    };
    source: LocalDataSource;

    dataSesiones: Array<any> = Array<any>();
    selectedIDProfesional;
    selectedIDPaciente;

    public constructor(
        public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private datePipe: DatePipe,
        private cp: CurrencyPipe,
        private sesionesService: SesionService) {
    }

    public ngOnInit(): void {
        this.selectedIDProfesional = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.selectedIDPaciente =  params['id'];
            this.getSesiones();
        });
    }

    getSesiones() {
        return this.sesionesService.getByIDPacienteeIDProfesional(this.selectedIDPaciente, this.selectedIDProfesional).then(
            response => {
                if (response) {
                  this.dataSesiones = response;
                  this.source = new LocalDataSource(this.dataSesiones);
                } else {
                  this.showMessage('No se han podido obtener datos de sesiones para el paciente seleccionado!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'fecinicio', search: query },
            { field: 'observaciones', search: query },
            { field: 'precio', search: query }
        ], false);
    }

    showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
              .size('sm')
              .showClose(false)
              .isBlocking(true)
              .title(this.modalTitle)
              .body(msg)
              .okBtn('Aceptar')
              .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
