import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { AvatarModule } from 'ngx-avatar';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

// relative import
import { ProPacienteRoutingModule } from './propaciente-routing.module';
import { ProPacienteComponent } from './propaciente.component';
import {
    GeneralesComponent,
    MensajesComponent,
    SesionesComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        QuillModule,
        AvatarModule,
        Ng2SmartTableModule,
        ProPacienteRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot()
    ],
    declarations: [
        ProPacienteComponent,
        GeneralesComponent,
        MensajesComponent,
        SesionesComponent
    ]
})
export class ProPacienteModule { }
