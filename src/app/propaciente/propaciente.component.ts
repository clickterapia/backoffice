import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { Paciente, PacienteService, TWUtils } from '../shared';

@Component({
    selector: 'app-form',
    templateUrl: './propaciente.component.html',
    styleUrls: ['./propaciente.component.scss'],
    providers: [ PacienteService ]
})

export class ProPacienteComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Paciente';

    public selectedID = '';
    public selectedEntity: Paciente;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private pacienteService: PacienteService) {
        this.selectedEntity = new Paciente();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            // this.onGetEntityData();
        });
    }

    // public onGetEntityData(): any {
    //     if (this.selectedID !== '') {
    //         this.pacienteService.getByID(this.selectedID).then(
    //             response => {
    //                 this.selectedEntity = response as Profesional;
    //                 this.locationUrl = this.selectedEntity.provincia + '+'
    //                     + this.selectedEntity.pais;
    //             }
    //         ).catch(e => this.handleError(e));
    //     }
    // }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
