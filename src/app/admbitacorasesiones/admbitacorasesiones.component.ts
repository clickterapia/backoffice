import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyAR, NgbDateCustomParserFormatter, TWUtils } from '../shared';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SesionService, EventoService, ProfesionalService, PacienteService } from '../shared';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-admbitacorasesiones',
    // styleUrls: ['./pacsesiones.component.scss'],
    templateUrl: './admbitacorasesiones.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        SesionService,
        EventoService,
        ProfesionalService,
        PacienteService
    ]
})
export class AdmBitacoraSesionesComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Bitácora de sesiones';
    modalContent: TemplateRef<any>;

    selectedProf: any;
    selectedPac: any;
    fecha: string;

    dataProfesionales: Array<any> = Array<any>();
    dataPacientes: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fecinicio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
            usuario: { title: 'Paciente', filter: false, },
            profesional: { title: 'Profesional', filter: false, },
            eventotipo: { title: 'Tipo de evento', filter: false, },
            evento: { title: 'Descripción', filter: false, },
            horaevento: { title: 'Hora del evento', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    dataEventos: Array<any> = Array<any>();
    selectedID;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private cdRef: ChangeDetectorRef,
        private datePipe: DatePipe,
        private cp: CurrencyAR,
        private sesionService: SesionService,
        private profesionalService: ProfesionalService,
        private eventoService: EventoService,
        private pacienteService: PacienteService) {
            const date = new Date();
            // this.fecha = TWUtils.stringDateToJson(this.datePipe.transform(date, 'yyyy-MM-dd'));
            this.selectedPac = undefined;
            this.selectedProf = undefined;
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        // this.route.params.subscribe(params => {
            // this.getEventos();
        // });
        this.source = new LocalDataSource();
        this.getProfesionales();
        this.getPacientes();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    getEventos() {
        if (this.selectedPac === undefined) {
            this.showMessage('Falta ingresar el paciente!!');
            return;
        }
        if (this.selectedProf === undefined) {
            this.showMessage('Falta ingresar el profesional!!');
            return;
        }
        if (this.fecha === undefined) {
            this.showMessage('Falta ingresar la fecha!!');
            return;
        }
        const fecha = TWUtils.arrayDateToStringISO(this.fecha);
        if (fecha === '') {
            this.showMessage('No se ha ingresado una fecha correcta. intente de nuevo por favor.');
            return;
        }
        return this.eventoService.getEventosXData(this.selectedPac.id, this.selectedProf.id, fecha).then(
            response => {
                if (response) {
                    this.dataEventos = response;
                    if (this.dataEventos.length === 0) {
                        this.showMessage('No se registran datos de sesiones!!');
                    }
                    this.source = new LocalDataSource(this.dataEventos);
                } else {
                    this.showMessage('No se han podido obtener datos de sesiones!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    getProfesionales(): any {
        return this.profesionalService.getList().then(
            response => {
                if (response) {
                    this.dataProfesionales = response;
                } else {
                    this.showMessage('No se ha podido obtener datos de profesionales!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    getPacientes(): any {
        return this.pacienteService.getList().then(
            response => {
                if (response) {
                    this.dataPacientes = response;
                } else {
                    this.showMessage('No se ha podido obtener datos de pacientes!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        // this.router.navigate(['/pacsesiondetalle', event.data.id]);
    }

    searchProf = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.dataProfesionales.filter(prof =>
            prof.apellido.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
            prof.nombre.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
            prof.dni.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    formatterProf = (x: any) => x.nombre + '';
    formatMatchesProf = (value: any) => value.apellido + ', ' + value.nombre;

    searchPac = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.dataPacientes.filter(pac =>
            pac.usuario.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    formatterPac = (x: any) => x.usuario + '';
    formatMatchesPac = (value: any) => value.usuario;

    filtrar() {
        this.getEventos();
        // const filtered = this.dataReservas.filter(sesion =>
        //     (this.verifyPaciente(sesion.idpaciente) &&
        //     this.verifyProfesional(sesion.idprofesional) &&
        //     this.verifyFecha(sesion.fecha)));
        // this.source = new LocalDataSource(filtered);
    }

    verifyPaciente(idpaciente: string): boolean {
        return (this.selectedPac.id !== undefined) ? idpaciente === this.selectedPac.id : true;
    }

    verifyProfesional(idprofesional: string): boolean {
        return (this.selectedProf.id !== undefined) ? idprofesional === this.selectedProf.id : true;
    }

    verifyFecha(fecha: string): boolean {
        return (this.fecha !== undefined) ? fecha === this.fecha : true;
    }

    limpiar() {
        this.fecha = undefined;
        this.selectedPac = undefined;
        this.selectedProf = undefined;
        this.source = new LocalDataSource(this.dataEventos);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'fecinicio', search: query },
            { field: 'profesional', search: query },
            { field: 'usuario', search: query },
            { field: 'eventotipo', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
