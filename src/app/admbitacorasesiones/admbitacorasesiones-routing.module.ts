import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmBitacoraSesionesComponent } from './admbitacorasesiones.component';

const routes: Routes = [
    {
        path: '',
        component: AdmBitacoraSesionesComponent,
        data: {
            title: 'Bitácora de sesiones',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Bitácora de sesiones'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmBitacoraSesionesRoutingModule { }
