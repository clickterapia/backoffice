import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProPacientesComponent } from './propacientes.component';

const routes: Routes = [
    {
        path: '',
        component: ProPacientesComponent,
        data: {
            title: 'Pacientes',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Pacientes'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProPacientesRoutingModule { }
