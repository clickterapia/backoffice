import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';
import { ContextMenuModule } from 'ngx-contextmenu';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ProPacientesComponent } from './propacientes.component';
import { ProPacientesRoutingModule } from './propacientes-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    ProPacientesRoutingModule,
    ContextMenuModule.forRoot({
      useBootstrap4: true
    })
  ],
  declarations: [
    ProPacientesComponent],
  exports: [ProPacientesComponent]
})
export class ProPacientesModule {}
