import { Component, OnInit,
  TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { TWUtils, PacienteService } from '../shared';

@Component({
  selector: 'app-propacientes',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./propacientes.component.scss'],
  templateUrl: './propacientes.component.html',
  providers: [
    DatePipe,
    {provide: LOCALE_ID, useValue: 'es-AR'},
    PacienteService
  ]
})
export class ProPacientesComponent implements OnInit {
    public modalTitle = 'Pacientes';
    modalContent: TemplateRef<any>;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            usuario: { title: 'Paciente', filter: false, },
            primera: {
                title: 'Primera Sesión', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            ultima:  {
                title: 'Última Sesión', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            cantidad: { title: 'Cantidad', filter: false, },
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    dataPacientes: Array<any> = Array<any>();
    public selectedID = '';

    public constructor(public router: Router,
        public modal: Modal,
        private datePipe: DatePipe,
        private pacientesService: PacienteService) {
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getPacientes();
    }

    getPacientes() {
        return this.pacientesService.getByIDProfesional(this.selectedID).then(
            response => {
                if (response) {
                    this.dataPacientes = response;
                    this.source = new LocalDataSource(this.dataPacientes);
                } else {
                    this.showMessage('No se han podido obtener datos de pacientes!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/propaciente', event.data.idpaciente]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'usuario', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                .size('sm')
                .showClose(false)
                .isBlocking(true)
                .title(this.modalTitle)
                .body(msg)
                .okBtn('Aceptar')
                .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
