import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';

import { registerLocaleData } from '@angular/common';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';

import { NavigationComponent } from './shared/header-navigation/navigation.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpinnerComponent } from './shared/spinner.component';

import { AuthGuard } from './shared/guard/auth.guard';
import { CallbackComponent } from './callback/callback.component';
import { AvatarModule } from 'ngx-avatar';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

import { SafePipe } from './shared/formatters/safe-pipe';
import { CurrencyAR } from './shared/formatters/currency-ar';
import { CountdownModule } from 'ngx-countdown';
import { AuthService, ProfesionalService, PacienteService } from './shared';

import { ExtBuscarProfesionalModule } from './extbuscarprofesional/extbuscarprofesional.module';
import { ExtPerfilProfesionalModule } from './extperfilprofesional/extperfilprofesional.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true,
};

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    BlankComponent,
    NavigationComponent,
    BreadcrumbComponent,
    SidebarComponent,
    CallbackComponent,
    SafePipe,
    CurrencyAR
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    PerfectScrollbarModule,
    AppRoutingModule,
    BootstrapModalModule,
    NgbModule.forRoot(),
    ModalModule.forRoot(),
    AvatarModule,
    CountdownModule,
    ExtBuscarProfesionalModule,
    ExtPerfilProfesionalModule
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    AuthGuard,
    AuthService,
    ProfesionalService,
    PacienteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
