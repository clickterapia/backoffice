import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyAR, NgbDateCustomParserFormatter,
    EncuestaXSesionService, EncuestaXSesion } from '../shared';


@Component({
    selector: 'app-admencuestasresultados',
    styleUrls: ['./admencuestasresultados.component.scss'],
    templateUrl: './admencuestasresultados.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        EncuestaXSesionService
    ]
})
export class AdmEncuestasResultadosComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Encuestas';
    modalContent: TemplateRef<any>;

    selectedEntity: EncuestaXSesion;

    dataEncuestas: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            encuesta: { title: 'Encuesta', filter: false, },
            paciente: { title: 'Paciente', filter: false, },
            profesional: { title: 'Profesional', filter: false, },
            fecha: {
                title: 'Desde', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            encuestatipo: { title: 'Tipo', filter: false, },
            calificacion: { title: 'Calificación', filter: false, },
            observaciones: { title: 'Observaciones', filter: false, },
            valorbooleano: { title: 'Valor booleano', filter: false, },
            publicado: { title: 'Publicado', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    if (+value === 0) {
                        return '<div class="cell_center">NO</div>';
                    } else {
                        return '<div class="cell_center">SI</div>';
                    }
                }
            }
        },
        defaultStyle: false,
        rowClassFunction : function(row) {
                const publicar = +row.data.publicar;
                if (publicar === 1) {
                    return 'depositado';
                } if (publicar === 0) {
                    return 'encartera';
                } else {
                    return '';
                }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(public router: Router,
        public modal: Modal,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private encuestaXSesionService: EncuestaXSesionService) {
            this.selectedEntity = new EncuestaXSesion();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getEncuestas();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        this.getEncuestas();
    }

    getEncuestas(): any {
        return this.encuestaXSesionService.getList().then(
            response => {
                if (response) {
                    this.dataEncuestas = response;
                    this.source = new LocalDataSource(this.dataEncuestas);
                } else {
                    this.showMessage('No se ha podido obtener datos de encuestas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as EncuestaXSesion;
        this.publicar(this.selectedEntity.id);
    }

    publicar(id: string) {
        this.encuestaXSesionService.publicar(this.selectedEntity.id).then(
            response => {
                if (response) {
                    this.getEncuestas();
                } else {
                    this.showMessage('No se ha podido publicar la encuesta!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'encuesta', search: query },
            { field: 'paciente', search: query },
            { field: 'profesional', search: query },
            { field: 'encustatipo', search: query },
            { field: 'calificacion', search: query },
            { field: 'observaciones', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
