import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmEncuestasResultadosComponent } from './admencuestasresultados.component';

const routes: Routes = [
    {
        path: '',
        component: AdmEncuestasResultadosComponent,
        data: {
            title: 'Encuestas',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Encuestas'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmEncuestasResultadosRoutingModule { }
