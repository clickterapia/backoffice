import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AvatarModule } from 'ngx-avatar';
import { LightboxModule } from 'ngx-lightbox';

// relative import
import { AdmProfesionalRoutingModule } from './admprofesional-routing.module';
import { AdmProfesionalComponent } from './admprofesional.component';
import {
    FiliatoriosComponent,
    DocumentacionComponent,
    SancionesComponent,
    MensajesComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        AdmProfesionalRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        Ng2SmartTableModule,
        NgbModule.forRoot(),
        AvatarModule,
        LightboxModule
    ],
    declarations: [
      AdmProfesionalComponent,
      FiliatoriosComponent,
      DocumentacionComponent,
      SancionesComponent,
      MensajesComponent
    ]
})
export class AdmProfesionalModule { }
