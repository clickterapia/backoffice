import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { Profesional, ProfesionalService, TWUtils } from '../shared';
import { environment } from '../../environments/environment';

import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './admprofesional.component.html',
    // styleUrls: ['./admprofesional.component.scss'],
    providers: [ProfesionalService]
})

export class AdmProfesionalComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil Profesional';

    public selectedID = '';
    public selectedEntity: Profesional;
    public urlMap: string;
    public habilitado: string;
    public notificaciones: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.urlMap = environment.urlMap + this.selectedEntity.provincia + '+'
                        + this.selectedEntity.pais;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public hasImage() {
        return (this.selectedEntity.fotourl.trim().length > 0);
    }

    aprobar() {
        this.selectedEntity.fecaprobado = TWUtils.arrayDateToString(new Date());
        this.profesionalService.setAprobacion(this.selectedEntity).then(
            response => {
                if (response) {
                    this.onGetEntityData();
                    this.showMessage('El profesional fue aprobado correctamente');
                } else {
                    this.showMessage('No se ha podido aprobar al profesional!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    isBloqueado(value: any) {
        return +this.selectedEntity.bloqueoadministrativo === +value;
    }

    setBloqueado(activo: any) {
        const tempProf = this.selectedEntity;
        tempProf.bloqueoadministrativo = +activo;
        this.profesionalService.setBloqueo(tempProf).then(
            response => {
                if (response) {
                    this.onGetEntityData();
                    // this.showMessage('El profesional fue activado correctamente');
                } else {
                    this.showMessage('No se ha podido activar/desactivar al profesional!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    // public getMap() {
    //     const map = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBiDmX5nFGBGp-XYjYNhmC8iQWTxHz7-Sg&q=' + this.locationUrl;
    //     return this.sanitizer.bypassSecurityTrustResourceUrl(map);
    // }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
