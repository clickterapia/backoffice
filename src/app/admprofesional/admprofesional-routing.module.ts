import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmProfesionalComponent } from './admprofesional.component';

const routes: Routes = [
    {
        path: '',
        component: AdmProfesionalComponent,
        data: {
            title: 'Perfil del Profesional',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmProfesionalRoutingModule { }
