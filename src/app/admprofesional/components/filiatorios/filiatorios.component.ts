import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    Profesional, ProfesionalService,
    EspecialidadService } from '../../../shared';

@Component({
    selector: 'app-filiatorios',
    templateUrl: './filiatorios.component.html',
    providers: [
        ProfesionalService,
        EspecialidadService
    ]
})

export class FiliatoriosComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    modalMessage: string;
    modalTitle = 'Profesional';

    dataEspecialidades: Array<any> = Array<any>();
    especialidades: string;

    selectedEntity: Profesional;
    selectedID: string;

    constructor(private route: ActivatedRoute,
        private modal: Modal,
        private profesionalService: ProfesionalService,
        private especialidadService: EspecialidadService) {
        this.selectedEntity = new Profesional();
        this.especialidades = '';
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.getData();
        });
    }

    public getData() {
        const p0 = this.especialidadService.getList();
        Promise.all([
            p0
        ]).then(([especialidades]) => {
            this.dataEspecialidades = especialidades;
            this.onSetEntityData();
        }, err => this.handleError(err));
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.processEspecialidades();
                }
            ).catch(e => this.handleError(e));
        }
    }

    processEspecialidades(): any {
        if (this.selectedEntity.especialidades.length < 1) {
            return;
        }
        const selectedItems = JSON.parse(this.selectedEntity.especialidades);
        selectedItems.forEach(esp => {
            this.especialidades += esp.especialidad;
        });
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}

interface City {
    name: string;
    code: string;
}
