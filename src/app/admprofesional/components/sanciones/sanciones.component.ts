import {
    Component, OnInit,
    TemplateRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { Problema, ProblemaService } from '../../../shared';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
    selector: 'app-sanciones',
    // styleUrls: ['./pacsesiones.component.scss'],
    templateUrl: './sanciones.component.html',
    providers: [
        DatePipe,
        ProblemaService
    ]
})
export class SancionesComponent implements OnInit {
    public modalTitle = 'Registros de Problemas';
    modalContent: TemplateRef<any>;

    selectedEntity: any;
    dataSanciones: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fecultmodif: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
            paciente: { title: 'Paciente', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(
        private route: ActivatedRoute,
        private modal: Modal,
        private datePipe: DatePipe,
        private problemaService: ProblemaService) {
            this.selectedEntity = new Problema();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.getSanciones();
        });
    }

    getSanciones(): any {
        return this.problemaService.getSancionesByID(this.selectedID).then(
            response => {
                if (response) {
                    this.dataSanciones = response;
                    this.source = new LocalDataSource(this.dataSanciones);
                } else {
                    this.showMessage('No se ha podido obtener datos de sanciones!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        // this.router.navigate(['/pacsesiondetalle', event.data.id]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'fecultmodif', search: query },
            { field: 'paciente', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
