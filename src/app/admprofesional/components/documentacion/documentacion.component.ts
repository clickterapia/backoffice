import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Lightbox } from 'ngx-lightbox';

import {
    Profesional, ProfesionalService } from '../../../shared';

@Component({
    selector: 'app-documentacion',
    templateUrl: './documentacion.component.html',
    providers: [
        ProfesionalService]
})

export class DocumentacionComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Documentación';

    hasBaseDropZoneOver = false;
    hasAnotherDropZoneOver = false;

    public selectedEntity: Profesional;
    public selectedID: string;

    private album: Array<any> = Array<any>();
    dniUrl: any;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private lightbox: Lightbox,
        private profesionalService: ProfesionalService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
        });
        this.onSetEntityData();
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.dniUrl = this.selectedEntity.dniurl;
                    const imageDni = {
                        src: this.selectedEntity.dniurl,
                        caption: 'DNI',
                        thumb: this.selectedEntity.dniurl
                    };
                    this.album.push(imageDni);
                    const imageTitulo = {
                        src: this.selectedEntity.titulourl,
                        caption: 'DNI',
                        thumb: this.selectedEntity.titulourl
                    };
                    this.album.push(imageTitulo);
                    const imageMatricula = {
                        src: this.selectedEntity.matriculaurl,
                        caption: 'DNI',
                        thumb: this.selectedEntity.matriculaurl
                    };
                    this.album.push(imageMatricula);
                }
            ).catch(e => this.handleError(e));
        }
    }

    isSeted(url: string) {
        if  (url === undefined) {
            return false;
        } else if (url === null) {
            return false;
        } else if (url.length < 1) {
            return false;
        }

        return true;
    }

    open(imagen: number) {
        let show = true;
        switch(imagen) {
            case 0:
                show = this.isSeted(this.selectedEntity.dniurl);
                break;
            case 1:
                show = this.isSeted(this.selectedEntity.titulourl);
                break;
            case 2:
                show = this.isSeted(this.selectedEntity.matriculaurl);
                break;
        }
        if (show) {
            this.lightbox.open(this.album, imagen, { wrapAround: true, showImageNumberLabel: false, enableTransition: false } );
        }
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
