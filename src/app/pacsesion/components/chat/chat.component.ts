import { Component, OnInit, Input } from '@angular/core';
import { ChatEngine, Sesion } from '../../../shared';

declare var deepstream: any;

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    providers: [ChatEngine]
})
export class ChatComponent implements OnInit {
    @Input() selectedEntity: Sesion;
    title = 'deepChat';
    username;
    text;
    chats;
    chatArray = [];

    constructor(private ce: ChatEngine) {
        this.selectedEntity = new Sesion();
    }

    ngOnInit() {
        this.ce.instance.connect(new Date().getTime(), {}, 'auth-key');
        this.ce.instance.on('$.ready', (data) => {
            this.ce.me = data.me;
        });
    }

    addChat() {

    }
}
