import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacSesionComponent } from './pacsesion.component';

const routes: Routes = [
    {
        path: '',
        component: PacSesionComponent,
        data: {
            title: 'Sesión Terapéutica',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pac'},
                {title: 'Sesión Terapéutica'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacSesionRoutingModule { }
