import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';

import { PacSesionComponent } from './pacsesion.component';
import { PacSesionRoutingModule } from './pacsesion-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

import { CountdownModule } from 'ngx-countdown';
import { ChatComponent, ConferenciaComponent, FinalizarComponent } from './components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PacSesionRoutingModule,
    CountdownModule,
    NgbModule
  ],
  declarations: [
    PacSesionComponent,
    ConferenciaComponent,
    ChatComponent,
    FinalizarComponent],
  exports: [PacSesionComponent]
})
export class PacSesionModule {}
