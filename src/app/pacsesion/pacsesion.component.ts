import {
    Component, OnInit,
    TemplateRef,
    ViewEncapsulation,
    ChangeDetectorRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { SesionService, Sesion, SesionState } from '../shared';
import { compareAsc } from 'date-fns';

import { SafePipe } from '../shared';
import { OpentokService } from '../shared';
import * as OT from '@opentok/client';

import { Config } from 'ngx-countdown';
import config from '../shared/opentok/config';
import { SafeResourceUrl } from '@angular/platform-browser';

@Component({
    selector: 'app-pacsesion',
    styleUrls: ['./pacsesion.component.scss'],
    templateUrl: './pacsesion.component.html',
    providers: [
        SafePipe,
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        OpentokService,
        SesionService
    ],
    encapsulation: ViewEncapsulation.None
})

export class PacSesionComponent implements OnInit {
    public modalTitle = 'Sesión Terapéutica';
    modalContent: TemplateRef<any>;

    selectedEntity = new Sesion();
    selectedID = '';
    tiempo;
    sesionUrl: SafeResourceUrl;

    config: Config;
    //  = {
    //     stopTime: new Date(this.selectedEntity.fecinicio).getSeconds(),
    //     repaint: function () {
    //         const me: any = this;
    //         let content: string;

    //         me.hands.forEach((hand: any) => {
    //             if (hand.lastValue !== hand.value) {
    //                 content = '';
    //                 const cur = me.toDigitals(hand.value + 1, hand.bits).join(''),
    //                     next = me.toDigitals(hand.value, hand.bits).join('');

    //                 hand.node.innerHTML = `
    //                     <span class="count curr top">${cur}</span>
    //                     <span class="count next top">${next}</span>
    //                     <span class="count next bottom">${next}</span>
    //                     <span class="count curr bottom">${cur}</span>`;
    //                 hand.node.parentElement.className = 'time';
    //                 setTimeout(() => {
    //                     hand.node.parentElement.className = 'time flip';
    //                 });
    //             }
    //         });
    //     },
    // };

    public constructor(public router: Router,
        public modal: Modal,
        private sp: SafePipe,
        private sesionService: SesionService) {
            this.selectedEntity = new Sesion();
            this.sesionUrl = '';
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getNextSesion();
    }

    getConfig(): Config {
        return  {
            demand: false,
            stopTime: new Date(this.selectedEntity.fecinicio).getTime(),
        };
    }

    getNextSesion() {
        return this.sesionService.getNextSesion(this.selectedID).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                    if (this.hasStarted()) {
                        this.onStartSesion();
                    } else {
                        this.config = this.getConfig();
                    }
                }
            }
        ).catch(e => this.handleError(e));
    }

    hasSession() {
        const estado = this.getSesionState();
        return (estado === SesionState.iniciada || estado === SesionState.noIniciada);
    }

    hasStarted() {
        return this.getSesionState() === +SesionState.iniciada;
    }

    finished() {
        return this.getSesionState() === +SesionState.finalizada;
    }

    getSesionState() {
        const fecinicio = new Date(this.selectedEntity.fecinicio);
        const fecfin = new Date(this.selectedEntity.fecfin);
        const now = new Date();
        const compinicio = compareAsc(now, fecinicio);
        const compfin = compareAsc(fecfin, now);
        if (this.selectedEntity.id.length === 0) {
            return +SesionState.inexistente;
        } else if (+compinicio >= 0) {
            if (+compfin >= 0) {
                return +SesionState.iniciada;
            } else {
                return +SesionState.finalizada;
            }
        } else {
            return +SesionState.noIniciada;
        }
    }

    getSessionDate() {
        return this.selectedEntity.fecinicio;
    }

    onStartSesion() {
        if (this.hasStarted()) {
            const url = config.SESION_URL.replace('DEFAULT_ROOM', this.selectedEntity.id.toString());
            this.sesionUrl = this.sp.transform(url);
        }
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
        .size('sm')
        .showClose(false)
        .isBlocking(true)
        .title(this.modalTitle)
        .body(msg)
        .okBtn('Aceptar')
        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
