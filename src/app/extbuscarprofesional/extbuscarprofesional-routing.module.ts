import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExtBuscarProfesionalComponent } from './extbuscarprofesional.component';

const routes: Routes = [
    {
        path: '',
        component: ExtBuscarProfesionalComponent,
        data: {
            title: 'Profesionales',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard1'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExtBuscarProfesionalRoutingModule { }
