import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { AvatarModule } from 'ngx-avatar';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

import { NavbarWebModule } from '../shared/navbar-web/navbar-web.module';
import { TopBarModule } from '../shared/topbar/topbar.module';
import { FooterWebModule } from '../shared/footer-web/footer-web.module';

// relative import
import { ExtBuscarProfesionalRoutingModule } from './extbuscarprofesional-routing.module';
import { ExtBuscarProfesionalComponent } from './extbuscarprofesional.component';

@NgModule({
    imports: [
        CommonModule,
        ExtBuscarProfesionalRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2CompleterModule,
        ImageCropperModule,
        AngularMultiSelectModule,
        FileUploadModule,
        AvatarModule,
        NavbarWebModule,
        TopBarModule,
        FooterWebModule
    ],
    declarations: [
        ExtBuscarProfesionalComponent
    ]
})
export class ExtBuscarProfesionalModule { }
