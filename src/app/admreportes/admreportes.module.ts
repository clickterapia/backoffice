import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

// relative import
import { AdmReportesRoutingModule } from './admreportes-routing.module';
import { AdmReportesComponent } from './admreportes.component';
import { ChartsRoutes } from './charts.routing';

import { ProfesionalesComponent,
    PremiumsComponent,
    BasicosComponent,
    RendimientoGeneralComponent,
    RendimientoProfesionalComponent,
    HorasOfrecidasComponent } from './components';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        AdmReportesRoutingModule,
        FormsModule,
        NgbModule.forRoot(),
        RouterModule.forChild(ChartsRoutes),
        ReactiveFormsModule,
        ChartsModule
    ],
    declarations: [
        AdmReportesComponent,
        ProfesionalesComponent,
        PremiumsComponent,
        BasicosComponent,
        RendimientoGeneralComponent,
        RendimientoProfesionalComponent,
        HorasOfrecidasComponent
    ]
})
export class AdmReportesModule { }
