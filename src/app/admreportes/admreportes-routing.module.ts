import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmReportesComponent } from './admreportes.component';

const routes: Routes = [
    {
        path: '',
        component: AdmReportesComponent,
        data: {
            title: 'Reportes',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmReportesRoutingModule { }
