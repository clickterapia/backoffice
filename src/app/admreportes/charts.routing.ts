import { Routes } from '@angular/router';

import { RendimientoGeneralComponent } from './components/rendimientogeneral/rendimientogeneral.component';
import { RendimientoProfesionalComponent } from './components/rendimientoprofesional/rendimientoprofesional.component';

export const ChartsRoutes: Routes = [
  {
    path: '',
    children: [
    {
      path: 'chartistjs',
      component: RendimientoGeneralComponent,
      data: {
        title: 'Chartis js',
        urls: [
            {title: 'Dashboard',url: '/dashboard'},
            {title: 'Chartis js Page'}]
      }
    }, 
    {
      path: 'chartjs',
      component: RendimientoProfesionalComponent,
      data: {
        title: 'Chart js',
        urls: [
            {title: 'Dashboard',url: '/dashboard'},
            {title: 'Chart js Page'}]
      }
    }]
  }
];
