import { Component, OnInit,
    TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR } from '../shared';

import { ProfesionalService,
    EspecialidadService,
    DisponibilidadService
    } from '../shared';

export enum TiposReportes {
    seleccionar = '-1',
    profesionales = '0',
    premiums = '1',
    basicos = '2',
    rendimientogeneral = '3',
    rendimientoprofesional = '4',
    cantidadhoras = '5',

    strseleccionar = 'Seleccionar',
    strprofesionales = 'Profesionales',
    strpremiums = 'Premiums',
    strbasicos = 'Básicos',
    strrendimientogeneral = 'Rendimiento general',
    strrendimientoprofesional = 'Rendimiento de un profesional',
    strcantidadhoras = 'Cantidad de horas ofrecidas'
}

@Component({
    selector: 'app-admreportes',
    templateUrl: './admreportes.component.html',
    styleUrls: ['./admreportes.component.scss'],
    providers: [
        DatePipe,
        CurrencyPipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        ProfesionalService,
        EspecialidadService,
        DisponibilidadService]
})

export class AdmReportesComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Reportes';

    public dataTiposReportes = [
        {'id': TiposReportes.seleccionar, 'tiporeporte': TiposReportes.strseleccionar},
        {'id': TiposReportes.profesionales, 'tiporeporte': TiposReportes.strprofesionales},
        {'id': TiposReportes.premiums, 'tiporeporte': TiposReportes.strpremiums},
        {'id': TiposReportes.basicos, 'tiporeporte': TiposReportes.strbasicos},
        {'id': TiposReportes.rendimientogeneral, 'tiporeporte': TiposReportes.strrendimientogeneral},
        {'id': TiposReportes.rendimientoprofesional, 'tiporeporte': TiposReportes.strrendimientoprofesional},
        {'id': TiposReportes.cantidadhoras, 'tiporeporte': TiposReportes.strcantidadhoras}];

    selectedReporte = TiposReportes.seleccionar;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private modal: Modal) {
    }

    public ngOnInit(): void {
    }

    public getData() {
    }

    onChange() {
        // this.dataProfesionalesFilt = this.dataProfesionales.filter(profesional =>
        //     this.testNombre(profesional.nombre + profesional.apellido + profesional.titulo) &&
        //     this.testSexo(profesional.idsexo) &&
        //     this.testEdad(profesional.fecnacimiento) &&
        //     this.testPrecio(profesional.precio) &&
        //     this.testEspecialidades(profesional.especialidades) &&
        //     this.testFranjaHoraria(profesional.id)
        // );
        // this.processProfesionales(false);
    }

    isProfesionales() {
        return this.selectedReporte === TiposReportes.profesionales;
    }

    isPremiums() {
        return this.selectedReporte === TiposReportes.premiums;
    }

    isBasicos() {
        return this.selectedReporte === TiposReportes.basicos;
    }

    isRendimientoGral() {
        return this.selectedReporte === TiposReportes.rendimientogeneral;
    }

    isRendimientoProfesional() {
        return this.selectedReporte === TiposReportes.rendimientoprofesional;
    }

    isHorasOfrecidas()  {
        return this.selectedReporte === TiposReportes.cantidadhoras;
    }

    showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
              .size('sm')
              .showClose(false)
              .isBlocking(true)
              .title(this.modalTitle)
              .body(msg)
              .okBtn('Aceptar')
              .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
