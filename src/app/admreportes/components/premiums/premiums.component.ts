import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { ProfesionalService, Reporte, TWUtils } from '../../../shared';

@Component({
    selector: 'app-premiums',
    // styleUrls: ['./premiums.component.scss'],
    templateUrl: './premiums.component.html',
    providers: [
        ProfesionalService
    ]
})
export class PremiumsComponent implements OnInit {
    public modalTitle = 'Reporte - Premiums';

    selectedEntity = new Reporte();

    fecDesde: string;
    fecHasta: string;

    public constructor(public router: Router,
        public modal: Modal,
        private datepipe: DatePipe,
        private profesionalService: ProfesionalService) {
            const date = new Date();
            this.fecHasta = TWUtils.stringDateToJson(this.datepipe.transform(date, 'yyyy-MM-dd'));
            this.fecDesde = TWUtils.stringDateToJson(this.datepipe.transform(date.setMonth(date.getMonth() - 1), 'yyyy-MM-dd'));
    }

    public ngOnInit(): void {
    }

    obtener() {
        this.getData();
    }

    getData() {
        const desde = TWUtils.arrayDateToStringISO(this.fecDesde);
        const hasta = TWUtils.arrayDateToStringISO(this.fecHasta);
        return this.profesionalService.getReportePremiums(desde, hasta).then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                } else {
                    this.showMessage('No se han podido obtener datos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
