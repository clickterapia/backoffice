import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import {
    ProfesionalService,
    TWUtils,
    NgbDateCustomParserFormatter } from '../../../shared';

@Component({
    selector: 'app-rendimientogeneral',
    // styleUrls: ['./profesionales.component.scss'],
    templateUrl: './rendimientogeneral.component.html',
    providers: [
        ProfesionalService,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    ]
})
export class RendimientoGeneralComponent implements AfterViewInit {
    public modalTitle = 'Reporte - Rendimiento general';

    fecDesde: string;
    fecHasta: string;

    dataReport;

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        barThickness : 5
    };

    public barChartLabels: string[] = [
        '2011',
        '2012',
        '2013',
        '2014',
        '2015',
        '2016',
        '2017'
    ];
    public barChartType = 'bar';
    public barChartLegend = true;

    public barChartData: any[] = [
        { data: [], label: 'Pagos' },
        // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Iphone X' }
    ];
    public barChartColors: Array<any> = [
        {backgroundColor: '#55ce63'},
        // {backgroundColor: '#009efb'}
    ];

    public constructor(public router: Router,
        public modal: Modal,
        private datepipe: DatePipe,
        private profesionalService: ProfesionalService) {
            const date = new Date();
            this.fecHasta = TWUtils.stringDateToJson(this.datepipe.transform(date, 'yyyy-MM-dd'));
            this.fecDesde = TWUtils.stringDateToJson(this.datepipe.transform(date.setMonth(date.getMonth() - 1), 'yyyy-MM-dd'));
    }

    ngAfterViewInit() {}

    obtener() {
        this.getData();
    }

    getData() {
        const desde = TWUtils.arrayDateToStringISO(this.fecDesde);
        const hasta = TWUtils.arrayDateToStringISO(this.fecHasta);
        return this.profesionalService.getReporteRendimientoGral(desde, hasta).then(
            response => {
                if (response) {
                    this.dataReport = response;
                    this.processData();
                } else {
                    this.showMessage('No se han podido obtener datos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    processData() {
        const data = Array<any>();
        const labels = Array<any>();
        data.unshift(0);
        this.dataReport.forEach(item => {
            data.unshift(item.cantidad);
            labels.unshift('$ ' + item.precio);
        });

        this.barChartData[0] = { data: data, label: 'Pagos' };
        this.barChartLabels = labels;
    }

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
