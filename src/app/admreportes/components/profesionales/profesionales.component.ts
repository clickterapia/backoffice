import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ProfesionalService, Reporte } from '../../../shared';

@Component({
    selector: 'app-profesionales',
    // styleUrls: ['./profesionales.component.scss'],
    templateUrl: './profesionales.component.html',
    providers: [
        ProfesionalService
    ]
})
export class ProfesionalesComponent implements OnInit {
    public modalTitle = 'Reporte - Profesionales';

    selectedEntity = new Reporte();

    public constructor(public router: Router,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
    }

    public ngOnInit(): void {
        this.getProfesionales();
    }

    getProfesionales() {
        return this.profesionalService.getReporteProfesionales().then(
            response => {
                if (response) {
                    this.selectedEntity = response;
                } else {
                    this.showMessage('No se han podido obtener datos de recursos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    getPConSesiones() {
        return (+this.selectedEntity.consesiones) * 100 / (+this.selectedEntity.total);
    }

    getPSinSesiones() {
        return (+this.selectedEntity.sinsesiones) * 100 / (+this.selectedEntity.total);
    }

    getPNoAprobados() {
        return (+this.selectedEntity.noaprobados) * 100 / (+this.selectedEntity.total);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
