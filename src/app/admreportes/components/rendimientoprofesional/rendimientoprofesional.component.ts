import { Component, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ProfesionalService, TWUtils, NgbDateCustomParserFormatter } from '../../../shared';

@Component({
    selector: 'app-rendimientoprofesional',
    // styleUrls: ['./rendimientoprofesional.component.scss'],
    templateUrl: './rendimientoprofesional.component.html',
    providers: [
        ProfesionalService,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    ]
})
export class RendimientoProfesionalComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Reporte - Rendimiento profesional';

    selectedID: any;
    fecDesde: string;
    fecHasta: string;

    dataProfesionales: Array<any> = Array<any>();
    dataReport;

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        barThickness : 5
    };

    public barChartLabels: string[] = [
        '2011',
        '2012',
        '2013',
        '2014',
        '2015',
        '2016',
        '2017'
    ];
    public barChartType = 'bar';
    public barChartLegend = true;

    public barChartData: any[] = [
        { data: [], label: 'Pagos' },
        // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Iphone X' }
    ];
    public barChartColors: Array<any> = [
        {backgroundColor: '#55ce63'},
        // {backgroundColor: '#009efb'}
    ];

    public constructor(public router: Router,
        public modal: Modal,
        private datepipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private profesionalService: ProfesionalService) {
            const date = new Date();
            this.fecHasta = TWUtils.stringDateToJson(this.datepipe.transform(date, 'yyyy-MM-dd'));
            this.fecDesde = TWUtils.stringDateToJson(this.datepipe.transform(date.setMonth(date.getMonth() - 1), 'yyyy-MM-dd'));
    }

    public ngOnInit(): void {
        this.getProfesionales();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }


    getProfesionales(): any {
        return this.profesionalService.getList().then(
            response => {
                if (response) {
                    this.dataProfesionales = response;
                } else {
                    this.showMessage('No se ha podido obtener datos de profesionales!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.dataProfesionales.filter(prof =>
            prof.apellido.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
            prof.nombre.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
            prof.dni.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    formatter = (x: any) => x.nombre + '';
    formatMatches = (value: any) => value.apellido + ', ' + value.nombre;

    obtener() {
        this.getData();
    }

    getData() {
        if (this.selectedID === undefined) {
            return;
        }
        const desde = TWUtils.arrayDateToStringISO(this.fecDesde);
        const hasta = TWUtils.arrayDateToStringISO(this.fecHasta);
        return this.profesionalService.getReporteRendimientoProfesional(this.selectedID.id, desde, hasta).then(
            response => {
                if (response) {
                    this.dataReport = response;
                    this.processData();
                } else {
                    this.showMessage('No se han podido obtener datos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    processData() {
        const data = Array<any>();
        const labels = Array<any>();
        this.dataReport.forEach(item => {
            data.unshift(item.cantidad);
            labels.unshift('$ ' + item.precio);
        });

        this.barChartData[0] = { data: data, label: 'Pagos' };
        this.barChartLabels = labels;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
