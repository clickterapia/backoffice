import { Component, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ProfesionalService, TWUtils, Profesional, Reporte } from '../../../shared';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import * as moment from 'moment';

@Component({
    selector: 'app-horasofrecidas',
    // styleUrls: ['./rendimientoprofesional.component.scss'],
    templateUrl: './horasofrecidas.component.html',
    providers: [
        ProfesionalService
    ]
})
export class HorasOfrecidasComponent implements OnInit, AfterViewChecked  {
    public modalTitle = 'Reporte - Horas ofrecidas';

    selectedID: any;
    fecDesde: string;
    fecHasta: string;

    reporte: Reporte;
    dataProfesionales: Array<any> = Array<any>();

    public constructor(public router: Router,
        public modal: Modal,
        private datepipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private profesionalService: ProfesionalService) {
            const date = new Date();
            this.fecHasta = TWUtils.stringDateToJson(this.datepipe.transform(date, 'yyyy-MM-dd'));
            this.fecDesde = TWUtils.stringDateToJson(this.datepipe.transform(date.setMonth(date.getMonth() - 1), 'yyyy-MM-dd'));
    }

    public ngOnInit(): void {
        this.getProfesionales();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    getProfesionales(): any {
        return this.profesionalService.getList().then(
            response => {
                if (response) {
                    this.dataProfesionales = response;
                } else {
                    this.showMessage('No se ha podido obtener datos de profesionales!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.dataProfesionales.filter(prof =>
            prof.apellido.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
            prof.nombre.toLowerCase().indexOf(term.toLowerCase()) > -1 ||
            prof.dni.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    formatter = (x: any) => x.nombre + '';
    formatMatches = (value: any) => value.apellido + ', ' + value.nombre;

    obtener() {
        this.getData();
    }

    getData() {
        if (this.selectedID === undefined) {
            return;
        }
        const desde = TWUtils.arrayDateToStringISO(this.fecDesde);
        const hasta = TWUtils.arrayDateToStringISO(this.fecHasta);
        return this.profesionalService.getReporteHorasOfrecidas(this.selectedID.id, desde, hasta).then(
            response => {
                if (response) {
                    this.reporte = response;
                } else {
                    this.showMessage('No se han podido obtener datos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    getPPagas() {
        const secTotal = moment.duration(this.reporte.total).asSeconds();
        const secPagas = moment.duration(this.reporte.pagas).asSeconds();
        return (+secPagas * 100) / +secTotal;
    }

    getPGratuitas() {
        const secTotal = moment.duration(this.reporte.total).asSeconds();
        const secGratuitas = moment.duration(this.reporte.gratuitas).asSeconds();
        return (+secGratuitas * 100) / +secTotal;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
