export * from './profesionales/profesionales.component';
export * from './premiums/premiums.component';
export * from './basicos/basicos.component';
export * from './rendimientogeneral/rendimientogeneral.component';
export * from './rendimientoprofesional/rendimientoprofesional.component';
export * from './horasofrecidas/horasofrecidas.component';
