import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmRecursosComponent } from './admrecursos.component';

const routes: Routes = [
    {
        path: '',
        component: AdmRecursosComponent,
        data: {
            title: 'Recursos',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Recursos'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmRecursosRoutingModule { }
