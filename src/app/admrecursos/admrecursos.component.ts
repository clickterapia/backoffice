import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyAR, NgbDateCustomParserFormatter,
    Recurso, RecursoService } from '../shared';

@Component({
    selector: 'app-admrecursos',
    // styleUrls: ['./admrecursos.component.scss'],
    templateUrl: './admrecursos.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        RecursoService
    ]
})
export class AdmRecursosComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Recursos';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataFrases: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            titulo: { title: 'Título', filter: false, },
            descripcion: { title: 'Descripción', filter: false, },
            primeros: {
                title: 'Primeros pasos', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    if (+value === 1) {
                        return '<i class="cell_center mdi mdi-check"></i>';
                    }
                }
            },
            comerciales: {
                title: 'Comerciales', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    if (+value === 1) {
                        return '<i class="cell_center mdi mdi-check"></i>';
                    }
                }
            },
            tecnicos: {
                title: 'Técnicos', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    if (+value === 1) {
                        return '<i class="cell_center mdi mdi-check"></i>';
                    }
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(public router: Router,
        private scrollToService: ScrollToService,
        public modal: Modal,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private recursoService: RecursoService) {
            this.selectedEntity = new Recurso();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getRecursos();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        this.getRecursos();
    }

    getRecursos(): any {
        return this.recursoService.getList().then(
            response => {
                if (response) {
                    this.dataFrases = response;
                    this.source = new LocalDataSource(this.dataFrases);
                } else {
                    this.showMessage('No se ha podido obtener datos de recursos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Recurso;
        const config: ScrollToConfigOptions = {
                target: 'item'
            };
        this.scrollToService.scrollTo(config);
    }

    onNew() {
        this.selectedEntity = new Recurso();
        const config: ScrollToConfigOptions = {
                target: 'item'
            };
        this.scrollToService.scrollTo(config);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'titulo', search: query },
            { field: 'descripcion', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
