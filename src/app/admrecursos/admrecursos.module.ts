import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AdmRecursosComponent } from './admrecursos.component';
import { AdmRecursosRoutingModule } from './admrecursos-routing.module';
import { RecursoComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    Ng2SmartTableModule,
    ScrollToModule.forRoot(),
    AdmRecursosRoutingModule
  ],
  declarations: [
    AdmRecursosComponent,
    RecursoComponent
    ],
  exports: [AdmRecursosComponent]
})
export class AdmRecursosModule {}
