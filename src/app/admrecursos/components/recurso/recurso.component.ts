import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Recurso, RecursoService } from '../../../shared';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-recurso',
    templateUrl: './recurso.component.html',
    styleUrls: ['./recurso.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        RecursoService ]
})

export class RecursoComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Recurso;
    @Output() entityUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Recurso';

    selectedID: string;
    idClase: string;
    dataClases = [
        { 'id': '-1', 'clase': 'Seleccione' },
        { 'id': '0', 'clase': 'Primeros pasos' },
        { 'id': '1', 'clase': 'Comerciales' },
        { 'id': '2', 'clase': 'Técnicos' }];

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private cdr: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private recursoService: RecursoService) {
            this.selectedEntity = new Recurso();
            this.idClase = '-1';
    }

    public ngOnInit(): void {
        this.onCancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;
            if (+this.selectedEntity.primeros === 1) {
                this.idClase = '0';
            } else if (+this.selectedEntity.comerciales === 1) {
                this.idClase = '1';
            } else if (+this.selectedEntity.tecnicos === 1) {
                this.idClase = '2';
            } else {
                this.idClase = '-1';
            }
            this.cdr.detectChanges();
        }
    }

    hasEntity(): boolean {
        return this.selectedEntity.id !== -1 && this.selectedEntity.id !== undefined;
    }

    isArchivo(tipo) {
        return (+this.selectedEntity.video === +tipo);
    }

    setArchivo(tipo) {
        this.selectedEntity.video = tipo;
    }

    onSave() {
        if (+this.idClase === 0) {
            this.selectedEntity.primeros = 1;
        } else if (+this.idClase === 1) {
            this.selectedEntity.comerciales = 1;
        } else if (+this.idClase === 2) {
            this.selectedEntity.tecnicos = 1;
        } else {
            this.showMessage('Falta seleccionar clase');
            return;
        }
        this.recursoService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.selectedEntity.id = response;
                    this.entityUpdated.emit(true);
                    this.onCancelar();
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos del recurso!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onEliminar() {
        this.recursoService.delete(this.selectedEntity).then(
            response => {
                const resp = Number(response);
                if ( resp > 0) {
                    this.showMessage('El registro se ha eliminado correctamente');
                    this.onCancelar();
                    this.entityUpdated.emit(true);
                } else if (resp === -1) {
                    this.showMessage('El registro no se ha eliminado porque se está utilizando.');
                } else {
                    this.showMessage('El registro no se ha eliminado por error de sistema.');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onCancelar() {
        this.selectedEntity = new Recurso();
        this.idClase = '-1';
    }

    getSafeURL(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    isUrlEmpty() {
        return (this.selectedEntity.url === '' || this.selectedEntity.url === undefined);
    }

    isPdfUrlEmpty() {
        if (this.selectedEntity.url !== '' && this.selectedEntity.url !== undefined) {
            const request = new XMLHttpRequest();
            request.open('HEAD', this.selectedEntity.url, false);
            request.send();
            // if (request.status == 200) {
            //     $scope.pdfData = true;
            //     $scope.StudentPDFFile =  response.student_id+'.pdf';
            // } else {
            //     $scope.pdfData = false;
            // }
        } else {
            // $scope.pdfData = false;
        }
        return (this.selectedEntity.url === '' || this.selectedEntity.url === undefined);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
