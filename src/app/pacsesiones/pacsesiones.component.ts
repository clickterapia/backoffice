import {
    Component, OnInit,
    TemplateRef
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SesionService } from '../shared';

@Component({
    selector: 'app-pacsesiones',
    styleUrls: ['./pacsesiones.component.scss'],
    templateUrl: './pacsesiones.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        SesionService
    ]
})
export class PacSesionesComponent implements OnInit {
    public modalTitle = 'Sesiones';
    modalContent: TemplateRef<any>;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            profesional: { title: 'Profesional', filter: false, },
            fecinicio: {
                title: 'Fecha', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    dataReservas: Array<any> = Array<any>();
    selectedID;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private datePipe: DatePipe,
        private sesionService: SesionService) {
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.getReservas();
        });
    }

    getReservas() {
        return this.sesionService.getByIDPaciente(this.selectedID).then(
            response => {
                if (response) {
                    this.dataReservas = response;
                    this.source = new LocalDataSource(this.dataReservas);
                } else {
                    this.showMessage('No se han podido obtener datos de reservas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/pacsesiondetalle', event.data.id]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'usuario', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
