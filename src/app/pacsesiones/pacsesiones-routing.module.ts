import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacSesionesComponent } from './pacsesiones.component';

const routes: Routes = [
    {
        path: '',
        component: PacSesionesComponent,
        data: {
            title: 'Sesiones',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pac'},
                {title: 'Sesiones'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacSesionesRoutingModule { }
