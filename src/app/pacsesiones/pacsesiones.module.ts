import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { PacSesionesComponent } from './pacsesiones.component';
import { PacSesionesRoutingModule } from './pacsesiones-routing.module';

registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    PacSesionesRoutingModule
  ],
  declarations: [
    PacSesionesComponent],
  exports: [PacSesionesComponent]
})
export class PacSesionesModule {}
