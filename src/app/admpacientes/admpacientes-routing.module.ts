import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmPacientesComponent } from './admpacientes.component';

const routes: Routes = [
    {
        path: '',
        component: AdmPacientesComponent,
        data: {
            title: 'Pacientes',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Pacientes'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmPacientesRoutingModule { }
