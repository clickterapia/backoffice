import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyAR, NgbDateCustomParserFormatter, PacienteService, Paciente } from '../shared';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
// import { ProblemaComponent } from './components';

@Component({
    selector: 'app-admpacientes',
    // styleUrls: ['./pacsesiones.component.scss'],
    templateUrl: './admpacientes.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        PacienteService
    ]
})
export class AdmPacientesComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Pacientes';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataPacientes: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            usuario: { title: 'Usuario', filter: false, },
            sexo: { title: 'Sexo', filter: false, },
            pais: { title: 'País', filter: false, },
            provincia: { title: 'Provincia', filter: false, },
            edad: { title: 'Edad', filter: false, },
            cantidad: { title: 'Cantidad de sesiones', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private cdRef: ChangeDetectorRef,
        private pacienteService: PacienteService) {
            this.selectedEntity = new Paciente();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getPacientes();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        this.getPacientes();
    }

    getPacientes(): any {
        return this.pacienteService.getConSesiones().then(
            response => {
                if (response) {
                    this.dataPacientes = response;
                    this.source = new LocalDataSource(this.dataPacientes);
                } else {
                    this.showMessage('No se ha podido obtener datos de pacientes!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Paciente;
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'usuario', search: query },
            { field: 'sexo', search: query },
            { field: 'pais', search: query },
            { field: 'provincia', search: query },
            { field: 'edad', search: query },
            { field: 'cantidad', search: query },
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
