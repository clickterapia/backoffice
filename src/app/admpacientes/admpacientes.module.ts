import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AdmPacientesComponent } from './admpacientes.component';
import { AdmPacientesRoutingModule } from './admpacientes-routing.module';
import { PacienteComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    Ng2SmartTableModule,
    AdmPacientesRoutingModule
  ],
  declarations: [
    AdmPacientesComponent,
    PacienteComponent
    ],
  exports: [AdmPacientesComponent]
})
export class AdmPacientesModule {}
