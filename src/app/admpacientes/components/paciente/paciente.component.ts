import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Paciente, PacienteService } from '../../../shared';

@Component({
    selector: 'app-paciente',
    templateUrl: './paciente.component.html',
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        PacienteService ]
})

export class PacienteComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Paciente;
    @Output() entityUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Paciente';

    public selectedID: string;
    public fecPrimera: string;
    public fecUltima: string;
    public fecRegistro: string;

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private pacienteService: PacienteService) {
        this.selectedEntity = new Paciente();
        this.fecPrimera = '';
        this.fecUltima = '';
        this.fecRegistro = '';
    }

    public ngOnInit(): void {
        this.cancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;

            if(!TWUtils.isNullDate(this.selectedEntity.primera)) {
                this.fecPrimera = this.datepipe.transform(this.selectedEntity.primera, 'dd-MM-yyyy HH:mm');
            }
            if(!TWUtils.isNullDate(this.selectedEntity.ultima)) {
                this.fecUltima = this.datepipe.transform(this.selectedEntity.ultima, 'dd-MM-yyyy HH:mm');
            }
            if(!TWUtils.isNullDate(this.selectedEntity.fecregistro)) {
                this.fecRegistro = this.datepipe.transform(this.selectedEntity.fecregistro, 'dd-MM-yyyy HH:mm');
            }
        }
    }

    isBloqueado(value: any) {
        return +this.selectedEntity.bloqueoadministrativo === +value;
    }

    setBloqueado(activo: any) {
        const tempPac = this.selectedEntity;
        tempPac.bloqueoadministrativo = +activo;
        this.pacienteService.setBloqueo(tempPac).then(
            response => {
                if (response) {
                    this.entityUpdated.emit(true);
                } else {
                    this.showMessage('No se ha podido activar/desactivar al paciente!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new Paciente();
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
