import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { CallbackComponent } from './callback/callback.component';
import { ExtBuscarProfesionalComponent } from './extbuscarprofesional/extbuscarprofesional.component';
import { ExtPerfilProfesionalComponent } from './extperfilprofesional/extperfilprofesional.component';

export const routes: Routes = [
{
    path: '',
    component: FullComponent,
    canActivate: [AuthGuard],
    children: [
        // { path: '', redirectTo: '/dashboard/dashboard1', pathMatch: 'full' },
        { path: 'dashboard', loadChildren: './dashboards/dashboard.module#DashboardModule' },
        { path: 'starter', loadChildren: './starter/starter.module#StarterModule' },
        { path: 'component', loadChildren: './component/component.module#ComponentsModule' },
        { path: 'icons', loadChildren: './icons/icons.module#IconsModule' },
        { path: 'forms', loadChildren: './form/forms.module#FormModule' },
        { path: 'tables', loadChildren: './table/tables.module#TablesModule' },
        { path: 'charts', loadChildren: './charts/charts.module#ChartModule' },
        { path: 'widgets', loadChildren: './widgets/widgets.module#WidgetsModule' },
        { path: 'extra-component', loadChildren: './extra-component/extra-component.module#ExtraComponentsModule' },
        { path: 'apps', loadChildren: './apps/apps.module#AppsModule' },
        { path: 'sample-pages', loadChildren: './sample-pages/sample-pages.module#SamplePagesModule' },
        { path: 'perfilprofesional', loadChildren: './perfilprofesional/perfilprofesional.module#PerfilProfesionalModule' },
        { path: 'perfilprofesional/:vinculado', loadChildren: './perfilprofesional/perfilprofesional.module#PerfilProfesionalModule' },
        { path: 'prodisponibilidad', loadChildren: './prodisponibilidad/prodisponibilidad.module#ProDisponibilidadModule' },
        { path: 'procapacitacion', loadChildren: './procapacitacion/procapacitacion.module#ProCapacitacionModule' },
        { path: 'propaciente/:id', loadChildren: './propaciente/propaciente.module#ProPacienteModule' },
        { path: 'propacientes', loadChildren: './propacientes/propacientes.module#ProPacientesModule' },
        { path: 'prosesion', loadChildren: './prosesion/prosesion.module#ProSesionModule' },
        { path: 'proreservas', loadChildren: './proreservas/proreservas.module#ProReservasModule' },
        { path: 'propremium', loadChildren: './propremium/propremium.module#ProPremiumModule' },
        { path: 'proestadocuenta', loadChildren: './proestadocuenta/proestadocuenta.module#ProEstadoCuentaModule' },
        { path: 'prosesiondetalle/:id', loadChildren: './prosesiondetalle/prosesiondetalle.module#ProSesionDetalleModule' },
        { path: 'pacperfil', loadChildren: './pacperfil/pacperfil.module#PacPerfilModule' },
        { path: 'pacprofesionales', loadChildren: './pacprofesionales/pacprofesionales.module#PacProfesionalesModule' },
        { path: 'pacprofesional/:id', loadChildren: './pacprofesional/pacprofesional.module#PacProfesionalModule' },
        { path: 'pacsolicitudsesion/:id', loadChildren: './pacsolicitudsesion/pacsolicitudsesion.module#PacSolicitudSesionModule' },
        {
            path: 'pacsolicitudsesion/:id/:iddisponibilidad',
            loadChildren: './pacsolicitudsesion/pacsolicitudsesion.module#PacSolicitudSesionModule'
        },
        { path: 'pacsesiones', loadChildren: './pacsesiones/pacsesiones.module#PacSesionesModule' },
        { path: 'pacsesiondetalle/:id', loadChildren: './pacsesiondetalle/pacsesiondetalle.module#PacSesionDetalleModule' },
        { path: 'pacsesion', loadChildren: './pacsesion/pacsesion.module#PacSesionModule' },
        { path: 'admreportes', loadChildren: './admreportes/admreportes.module#AdmReportesModule' },
        { path: 'admbitacorasesiones', loadChildren: './admbitacorasesiones/admbitacorasesiones.module#AdmBitacoraSesionesModule' },
        { path: 'admregistrosproblemas', loadChildren: './admregistrosproblemas/admregistrosproblemas.module#AdmRegistrosProblemasModule' },
        { path: 'admprofesionales', loadChildren: './admprofesionales/admprofesionales.module#AdmProfesionalesModule' },
        { path: 'admprofesional/:id', loadChildren: './admprofesional/admprofesional.module#AdmProfesionalModule' },
        { path: 'admpacientes', loadChildren: './admpacientes/admpacientes.module#AdmPacientesModule' },
        { path: 'admbanners', loadChildren: './admbanners/admbanners.module#AdmBannersModule' },
        { path: 'admencuestas', loadChildren: './admencuestas/admencuestas.module#AdmEncuestasModule' },
        {
            path: 'admencuestasresultados',
            loadChildren: './admencuestasresultados/admencuestasresultados.module#AdmEncuestasResultadosModule'
        },
        { path: 'admfrases', loadChildren: './admfrases/admfrases.module#AdmFrasesModule' },
        { path: 'admrecursos', loadChildren: './admrecursos/admrecursos.module#AdmRecursosModule' },
        { path: 'admmercadopago', loadChildren: './admmercadopago/admmercadopago.module#AdmMercadoPagoModule' },
    ]
},
{
    path: '',
    component: BlankComponent,
    children: [
        {
            path: 'authentication',
            loadChildren: './authentication/authentication.module#AuthenticationModule'
        }
    ]
},
{
    path: 'extbuscarprofesional',
    component: ExtBuscarProfesionalComponent,
},
{
    path: 'extperfilprofesional/:id',
    component: ExtPerfilProfesionalComponent,
},
{ path: 'callback', component: CallbackComponent },
{
    path: '**',
    redirectTo: '404'
}];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
                onSameUrlNavigation: 'ignore',
                anchorScrolling: 'enabled',
                scrollPositionRestoration: 'enabled'
            }),
        NgbModule.forRoot()],
    exports: [RouterModule]
})
export class AppRoutingModule { }

