import { Component } from '@angular/core';
import 'rxjs/add/operator/filter';

import { AuthService } from './shared';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  hash: string;

  constructor(
    public auth: AuthService,
    private router: Router) {
       this.router.events.filter((event: any) => event instanceof NavigationStart)
            .subscribe((data: NavigationStart) => {
                if (data.url === '/access_token') {
                    this.hash = window.location.hash;
                    auth.handleAuthentication();
                    this.router.navigate([]);
                }
            });
  }
}
