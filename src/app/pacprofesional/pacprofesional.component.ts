import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { Profesional, ProfesionalService } from '../shared';

@Component({
    selector: 'app-form',
    templateUrl: './pacprofesional.component.html',
    styleUrls: ['./pacprofesional.component.scss'],
    providers: [ProfesionalService]
})

export class PacProfesionalComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil Profesional';

    public selectedID = '';
    public selectedEntity: Profesional;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public isNew() {
        return (this.selectedID.length === 0);
    }

    public hasImage() {
        return (this.selectedEntity.fotourl.trim().length > 0);
    }

    solicitarSesion() {
        this.router.navigate(['/pacsolicitudsesion', this.selectedID]);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
