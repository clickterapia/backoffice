import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { AvatarModule } from 'ngx-avatar';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { MomentModule } from 'angular2-moment';

registerLocaleData(localeEs);

// relative import
import { PacProfesionalRoutingModule } from './pacprofesional-routing.module';
import { PacProfesionalComponent } from './pacprofesional.component';
import {
    PerfilComponent,
    OpinionesComponent,
    MensajesComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        PacProfesionalRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2CompleterModule,
        AvatarModule,
        MomentModule
    ],
    declarations: [
      PacProfesionalComponent,
      PerfilComponent,
      OpinionesComponent,
      MensajesComponent
    ]
})
export class PacProfesionalModule { }
