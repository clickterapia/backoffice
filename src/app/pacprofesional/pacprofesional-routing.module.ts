import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacProfesionalComponent } from './pacprofesional.component';

const routes: Routes = [
    {
        path: '',
        component: PacProfesionalComponent,
        data: {
            title: 'Perfil del Profesional',
            urls: [
                {title: 'Panel de control', url: '/dashboard/dashboard-pac'},
                {title: 'Profesionales', url: '/pacprofesionales'},
                {title: 'Perfil'}
            ]
        }
    }
];
// const routes: Routes = [
//     {
//         path: '',
//         component: PacProfesionalComponent,
//         data: {
//             title: 'Perfil del Profesional',
//             urls: [
//                 {title: 'Profesionales', url: '/pacprofesionales'},
//                 {title: 'Perfil'}
//             ]
//         }
//     }
// ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacProfesionalRoutingModule { }
