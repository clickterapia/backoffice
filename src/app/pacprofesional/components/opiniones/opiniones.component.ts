import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import * as moment from 'moment';

import { ProfesionalService } from '../../../shared';

@Component({
    selector: 'app-opiniones',
    templateUrl: './opiniones.component.html',
    styleUrls: ['./opiniones.component.scss'],
    providers: [
        DatePipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        ProfesionalService]
})

export class OpinionesComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Opiniones';

    dataOpiniones: Array<any> = Array<any>();
    opinionesRecientes: Object[];

    selectedID: string;

    constructor(public router: Router,
        private route: ActivatedRoute,
        private dp: DatePipe,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
            this.selectedID = '';
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.getData();
        });
    }

    getData() {
        const p0 = this.profesionalService.getOpinionsById(this.selectedID);
        Promise.all([p0])
            .then(([opiniones]) => {
                this.dataOpiniones = opiniones;
                this.processOpiniones();
            }, err => this.handleError(err));
    }

    processOpiniones() {
        this.dataOpiniones.forEach(opinion => {
            this.opinionesRecientes = [];
            this.opinionesRecientes.unshift({
                id: `${opinion.id}`,
                usuario: `${opinion.usuario}`,
                observaciones: `${opinion.observaciones}`,
                fecfinstr: `${this.dp.transform(opinion.fecfin)}`,
                fecfin: `${opinion.fecfin}`
            });
        });
    }

    getSinceDate(fecha) {
        const myMoment =  moment(fecha, 'YYYYMMDD').locale('es').fromNow();
        return myMoment;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
