import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmMercadoPagoComponent } from './admmercadopago.component';

const routes: Routes = [
    {
        path: '',
        component: AdmMercadoPagoComponent,
        data: {
            title: 'Test Mecado Pago',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmMercadoPagoRoutingModule { }
