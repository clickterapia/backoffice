import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { TWUtils } from '../shared';
import { Headers, Http, Response, RequestOptions } from '@angular/http';

@Component({
    selector: 'app-form',
    templateUrl: './admmercadopago.component.html',
    // styleUrls: ['./admmercadopago.component.scss'],
    providers: [TWUtils]
})

export class AdmMercadoPagoComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Mercado Pago';
    private headers;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }
    public ngOnInit(): void {
        window.location.href = 'http://192.168.1.24/clickterapia-mp/checkout/1234';
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
