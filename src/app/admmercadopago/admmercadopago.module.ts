import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// relative import
import { AdmMercadoPagoRoutingModule } from './admmercadopago-routing.module';
import { AdmMercadoPagoComponent } from './admmercadopago.component';

@NgModule({
    imports: [
        CommonModule,
        AdmMercadoPagoRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot()
    ],
    declarations: [
      AdmMercadoPagoComponent
    ]
})
export class AdmMercadoPagoModule { }
