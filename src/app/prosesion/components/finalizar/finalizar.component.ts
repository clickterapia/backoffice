import { Component, Input, OnInit, SimpleChange, OnChanges, ChangeDetectorRef, AfterViewInit  } from '@angular/core';
import { Sesion,
    EncuestaService, Encuesta,
    EncuestaXSesion, EncuestaXSesionService } from '../../../shared';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-finalizar',
  templateUrl: './finalizar.component.html',
  styleUrls: ['./finalizar.component.scss'],
  providers: [
      EncuestaService,
      EncuestaXSesionService ]
})

export class FinalizarComponent implements OnInit, OnChanges, AfterViewInit {
    @Input() sesion: Sesion;

    public modalTitle = 'Sesión terapéutica';

    dataEncuestas: Array<any> = Array<any>();
    selectedIndex: number;
    selectedEntity: Encuesta;
    currentRate: number;
    newEncuesta: EncuestaXSesion;

    tipoEncuesta: string;

    constructor(
        public router: Router,
        private cdr: ChangeDetectorRef,
        public modal: Modal,
        private encuestaService: EncuestaService,
        private encuestaXSesionService: EncuestaXSesionService) {
        this.selectedIndex = -1;
        this.selectedEntity = new Encuesta();
        this.newEncuesta = new EncuestaXSesion();
        this.currentRate = 5;
    }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.cdr.detectChanges();
    }

    getData(): any {
        if (this.sesion.idpaciente === undefined) {
            return;
        }
        this.encuestaService.getByIdProfesional(this.sesion.idpaciente)
            .then(
                response => {
                    this.dataEncuestas = response;
                    if (this.dataEncuestas.length > 0) {
                        this.selectedIndex = 0;
                        this.setEncuestaIndex(this.selectedIndex);
                        this.cdr.detectChanges();
                    } else {
                        this.router.navigate(['/dashboard/dashboard-pac']);
                    }
                })
            .catch(e => this.handleError(e));
    }

    isCalificacion() {
        return this.selectedEntity.calificacion === 1;
    }

    isObservaciones() {
        return this.selectedEntity.observaciones === 1;
    }

    isBooleano() {
        return this.selectedEntity.booleano === 1;
    }

    isBooleanoTrue() {
        return this.newEncuesta.valorbooleano === 1;
    }

    isBooleanoFalse() {
        return this.newEncuesta.valorbooleano === 0;
    }

    setBooleano(value: number) {
        this.newEncuesta.valorbooleano = value;
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['sesion'].currentValue !== null) {
            this.sesion = changes['sesion'].currentValue;
            this.getData();
        }
    }

    onSave() {
        this.encuestaXSesionService.save(this.newEncuesta).then(
            response => {
                if (response) {
                    this.selectedIndex++;
                    if (this.selectedIndex === this.dataEncuestas.length) {
                        this.showMessage('Muchas gracias por su colaboración!');
                    } else {
                        this.setEncuestaIndex(this.selectedIndex);
                    }
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    setEncuestaIndex(index: number) {
        this.selectedEntity = this.dataEncuestas[index];
        this.newEncuesta = new EncuestaXSesion();
        this.newEncuesta.idsesion = this.sesion.id;
        this.newEncuesta.idencuesta = this.selectedEntity.id;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
        dialogRef.result
            .then( result => {
                if (this.selectedIndex === this.dataEncuestas.length) {
                    this.router.navigate(['/dashboard/dashboard-pro']);
                }
            });
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
