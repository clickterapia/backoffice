import { Component, ElementRef, AfterViewInit, ViewChild, Input, SimpleChanges, OnChanges } from '@angular/core';
import { OpentokService } from '../../../shared';

const publish = () => {
};

@Component({
    selector: 'app-proconferencia',
    templateUrl: './proconferencia.component.html',
    styleUrls: ['./proconferencia.component.scss']
})

export class ProConferenciaComponent implements OnChanges {
    @ViewChild('publisherDiv') publisherDiv: ElementRef;
    @Input() session: OT.Session;
    publisher: OT.Publisher;
    publishing: Boolean;

    constructor(private opentokService: OpentokService) {
        this.publishing = false;
    }

    // ngAfterViewInit() {
    //     const OT = this.opentokService.getOT();
    //     this.publisher = OT.initPublisher(this.publisherDiv.nativeElement, { insertMode: 'append' });

    //     if (this.session) {
    //         if (this.session['isConnected']()) {
    //             this.publish();
    //         }
    //         this.session.on('sessionConnected', () => this.publish());
    //     }
    // }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['session'].currentValue !== undefined) {
            const OT = this.opentokService.getOT();
            this.publisher = OT.initPublisher(this.publisherDiv.nativeElement, { insertMode: 'append' });

            if (this.session) {
                if (this.session['isConnected']()) {
                    this.publish();
                }
                this.session.on('sessionConnected', () => this.publish());
            }
        }
    }

    publish() {
        this.session.publish(this.publisher, (err) => {
            if (err) {
                alert(err.message);
            } else {
                this.publishing = true;
            }
        });
    }

}
