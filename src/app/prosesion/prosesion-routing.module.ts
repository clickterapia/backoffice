import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProSesionComponent } from './prosesion.component';

const routes: Routes = [
    {
        path: '',
        component: ProSesionComponent,
        data: {
            title: 'Sesión Terapéutica',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Sesión Terapéutica'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProSesionRoutingModule { }
