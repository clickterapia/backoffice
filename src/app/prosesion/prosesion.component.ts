import {
    Component, OnInit,
    TemplateRef,
    ViewEncapsulation,
    ChangeDetectorRef,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { SafePipe, PacienteService, Paciente } from '../shared';
import { LOCALE_ID } from '@angular/core';
import { SesionService, Sesion, SesionState } from '../shared';
import { compareAsc } from 'date-fns';
import { Config } from 'ngx-countdown';
import { OpentokService } from '../shared';
import * as OT from '@opentok/client';
import { CountdownComponent } from 'ngx-countdown';
import * as moment from 'moment';

import config from '../shared/opentok/config';

import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-prosesion',
    styleUrls: ['./prosesion.component.scss'],
    templateUrl: './prosesion.component.html',
    providers: [
        OpentokService,
        SafePipe,
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        SesionService,
        PacienteService
    ],
    encapsulation: ViewEncapsulation.None
})

export class ProSesionComponent implements OnInit {
    public modalTitle = 'Sesión Terapéutica';

    selectedEntity = new Sesion();
    selectedPaciente = new Paciente();
    selectedID = '';
    sesionUrl: string;

    config: Config;

    public constructor(public router: Router,
        public modal: Modal,
        public sanitizer: DomSanitizer,
        private sp: SafePipe,
        private sesionService: SesionService,
        private pacienteService: PacienteService) {
            this.selectedPaciente = new Paciente();
            this.selectedEntity = new Sesion();
            this.sesionUrl = '';
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getNextSesion();
    }

    getNextSesion() {
        return this.sesionService.getReservasByIDProfesional(this.selectedID).then(
            response => {
                if (response) {
                    if (response.length > 0) {
                        this.selectedEntity = response[0];
                        this.getPaciente();
                        if (this.hasStarted()) {
                            this.onStartSesion();
                        }
                    }
                } else {
                    this.showMessage('No existen sesiones programadas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    getPaciente() {
        this.pacienteService.getByID(this.selectedEntity.idpaciente).then(
            response => {
                if (response) {
                    this.selectedPaciente = response as Paciente;
                } else {
                    this.showMessage('No se pudo obtener datos del paciente!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    hasSession() {
        const estado = this.getSesionState();
        return (estado === SesionState.iniciada || estado === SesionState.noIniciada);
    }

    hasStarted() {
        return this.getSesionState() === +SesionState.iniciada;
    }

    finished() {
        return this.getSesionState() === +SesionState.finalizada;
    }

    getSesionState() {
        const fecinicio = new Date(this.selectedEntity.fecinicio);
        const fecfin = new Date(this.selectedEntity.fecfin);
        const now = new Date();
        const compinicio = compareAsc(now, fecinicio);
        const compfin = compareAsc(fecfin, now);
        if (this.selectedEntity.id.length === 0) {
            return +SesionState.inexistente;
        } else if (+compinicio >= 0) {
            if (+compfin >= 0) {
                return +SesionState.iniciada;
            } else {
                return +SesionState.finalizada;
            }
        } else {
            return +SesionState.noIniciada;
        }
    }

    getSessionDate() {
        return this.selectedEntity.fecinicio;
    }

    onStartSesion() {
        if (this.hasStarted()) {
        }
    }

    getUrl() {
        if (this.selectedEntity !== undefined) {
            const url = config.SESION_URL.replace('DEFAULT_ROOM', this.selectedEntity.id.toString());
            return this.sanitizer.bypassSecurityTrustResourceUrl(url);
        }
    }

    focusOutFunction() {
        this.pacienteService.putHistorial(this.selectedPaciente).then(
            response => {
                if (response) {
                } else {
                    this.showMessage('No se ha podido guardar la história clínica!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
