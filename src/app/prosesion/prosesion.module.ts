import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';

import { CountdownModule } from 'ngx-countdown';
import { ProSesionComponent } from './prosesion.component';
import { ProSesionRoutingModule } from './prosesion-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

import { ProConferenciaComponent, FinalizarComponent } from './components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProSesionRoutingModule,
    CountdownModule,
    NgbModule
  ],
  declarations: [
    ProSesionComponent,
    ProConferenciaComponent,
    FinalizarComponent ],
  exports: [ProSesionComponent]
})
export class ProSesionModule {}
