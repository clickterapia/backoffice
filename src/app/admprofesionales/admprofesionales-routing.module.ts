import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmProfesionalesComponent } from './admprofesionales.component';

const routes: Routes = [
    {
        path: '',
        component: AdmProfesionalesComponent,
        data: {
            title: 'Profesionales',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Profesionales'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmProfesioanlesRoutingModule { }
