import {
    Component, OnInit,
    TemplateRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LocalDataSource } from 'ng2-smart-table';
import { Profesional, ProfesionalService } from '../shared';

@Component({
    selector: 'app-admprofesionales',
    templateUrl: './admprofesionales.component.html',
    providers: [
        ProfesionalService
    ]
})
export class AdmProfesionalesComponent implements OnInit {
    readonly PERFILES_TODOS = 2;
    readonly PERFILES_COMPLETOS = 0;
    readonly PERFILES_INCOMPLETOS = 1;
    readonly APROBACION_TODOS = 2;
    readonly APROBACION_APROBADOS = 1;
    readonly APROBACION_NOAPROBADOS = 0;
    public modalTitle = 'Profesionales';
    modalContent: TemplateRef<any>;

    dataProfesionales: Array<any> = Array<any>();
    filtroPerfil = this.PERFILES_TODOS;
    filtroAprobados = this.APROBACION_TODOS;

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            apellido: { title: 'Apellidos', filter: false, },
            nombre: { title: 'Nombres', filter: false, },
            pais: { title: 'País', filter: false, },
            provincia: { title: 'Provincia', filter: false, },
            edad: { title: 'Edad', filter: false, },
            sexo: { title: 'Sexo', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    public constructor(public router: Router,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
    }

    public ngOnInit(): void {
        this.getProfesionales();
    }

    getProfesionales(): any {
        return this.profesionalService.getList().then(
            response => {
                if (response) {
                    this.dataProfesionales = response;
                    this.source = new LocalDataSource(this.dataProfesionales);
                } else {
                    this.showMessage('No se ha podido obtener datos de profesionales!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    isPerfil(tipo: any) {
        return this.filtroPerfil === +tipo;
    }

    setFilter() {
        // 1	Perfiles Todos	Aprobacion Todos
        let filtered = this.dataProfesionales;
        // 2	Perfiles Todos	Aprobacion Aprobados
        if (this.filtroPerfil === this.PERFILES_TODOS && this.filtroAprobados === this.APROBACION_APROBADOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.aprobado === this.APROBACION_APROBADOS);
        }
        // 3	Perfiles Todos	Aprobacion No aprobados
        if (this.filtroPerfil === this.PERFILES_TODOS && this.filtroAprobados === this.APROBACION_NOAPROBADOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.aprobado === this.APROBACION_NOAPROBADOS);
        }
        // 4	Perfiles Completos	Aprobacion Todos
        if (this.filtroPerfil === this.PERFILES_COMPLETOS && this.filtroAprobados === this.APROBACION_TODOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.incompleto === this.PERFILES_COMPLETOS);
        }
        // 5	Perfiles Completos	Aprobacion Aprobados
        if (this.filtroPerfil === this.PERFILES_COMPLETOS && this.filtroAprobados === this.APROBACION_APROBADOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.incompleto === this.PERFILES_COMPLETOS && +prof.aprobado === this.APROBACION_APROBADOS);
        }
        // 6	Perfiles Completos	Aprobacion No aprobados
        if (this.filtroPerfil === this.PERFILES_COMPLETOS && this.filtroAprobados === this.APROBACION_NOAPROBADOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.incompleto === this.PERFILES_COMPLETOS && +prof.aprobado === this.APROBACION_NOAPROBADOS);
        }
        // 7	Perfiles Incompletos	Aprobacion Todos
        if (this.filtroPerfil === this.PERFILES_INCOMPLETOS && this.filtroAprobados === this.APROBACION_TODOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.incompleto === this.PERFILES_INCOMPLETOS);
        }
        // 8	Perfiles Incompletos	Aprobacion Aprobados
        if (this.filtroPerfil === this.PERFILES_INCOMPLETOS && this.filtroAprobados === this.APROBACION_APROBADOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.incompleto === this.PERFILES_INCOMPLETOS && +prof.aprobado === this.APROBACION_APROBADOS);
        }
        // 9	Perfiles Incompletos	Aprobacion No aprobados
        if (this.filtroPerfil === this.PERFILES_INCOMPLETOS && this.filtroAprobados === this.APROBACION_NOAPROBADOS) {
            filtered = this.dataProfesionales.filter(prof =>
                +prof.incompleto === this.PERFILES_INCOMPLETOS && +prof.aprobado === this.APROBACION_NOAPROBADOS);
        }
        this.source = new LocalDataSource(filtered);
    }

    setPerfil(tipo: any) {
        this.filtroPerfil = +tipo;
        this.setFilter();
    }

    isAprobados(tipo: any) {
        return this.filtroAprobados === +tipo;
    }

    setAprobados(tipo: any) {
        this.filtroAprobados = +tipo;
        this.setFilter();
    }

    public userRowSelect(event: any): any {
        this.router.navigate(['/admprofesional', event.data.id]);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'apellido', search: query },
            { field: 'nombre', search: query },
            { field: 'pais', search: query },
            { field: 'provincia', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
