import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { Paciente, PacienteService } from '../shared';

import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './pacperfil.component.html',
    styleUrls: ['./pacperfil.component.scss'],
    providers: [PacienteService]
})

export class PacPerfilComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil Paciente';

    public selectedID = '';
    public selectedEntity: Paciente;
    public locationUrl: string;
    public habilitado: string;
    public notificaciones: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private pacienteService: PacienteService) {
        this.selectedEntity = new Paciente();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.onGetEntityData();
        });
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            this.pacienteService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Paciente;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public isNew() {
        return (this.selectedID.length === 0);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        this.modal.alert()
                .size('sm')
                .showClose(false)
                .isBlocking(true)
                .title(this.modalTitle)
                .body(msg)
                .okBtn('Aceptar')
                .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
