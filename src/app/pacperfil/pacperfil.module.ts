import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { AvatarModule } from 'ngx-avatar';

// relative import
import { PacPerfilRoutingModule } from './pacperfil-routing.module';
import { PacPerfilComponent } from './pacperfil.component';
import {
    FiliatoriosComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        PacPerfilRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2CompleterModule,
        ImageCropperModule,
        AngularMultiSelectModule,
        FileUploadModule,
        AvatarModule
    ],
    declarations: [
      PacPerfilComponent,
      FiliatoriosComponent
    ]
})
export class PacPerfilModule { }
