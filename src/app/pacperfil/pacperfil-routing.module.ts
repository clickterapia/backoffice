import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacPerfilComponent } from './pacperfil.component';

const routes: Routes = [
    {
        path: '',
        component: PacPerfilComponent,
        data: {
            title: 'Perfil del Paciente',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pac'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacPerfilRoutingModule { }
