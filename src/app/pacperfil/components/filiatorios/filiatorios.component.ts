import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Paciente, PacienteService,
    Sexo, SexoService } from '../../../shared';

@Component({
    selector: 'app-filiatorios',
    templateUrl: './filiatorios.component.html',
    styleUrls: ['./filiatorios.component.scss'],
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        PacienteService,
        SexoService]
})

export class FiliatoriosComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Datos Filiatorios';

    public dataSexos: Array<any> = Array<any>();

    public selectedEntity: Paciente;
    public selectedID: string;
    public minDate: {};
    public fecNacimiento: {};
    public notificaciones: boolean;

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private pacienteService: PacienteService,
        private sexoService: SexoService) {
        this.selectedEntity = new Paciente();
        this.notificaciones = false;
    }

    public ngOnInit(): void {
        const now = new Date();
        now.setFullYear(now.getFullYear() - 21);
        this.minDate = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    public getData() {
        const p0 = this.sexoService.getList();
        Promise.all([p0]).then( ([sexos]) => {
            this.dataSexos = sexos;
            const def = new Sexo();
            def.id = -1;
            def.sexo = 'Sexo...';
            this.dataSexos.unshift(def);
            this.onSetEntityData();
        });
    }

    public getSexos() {
        return this.sexoService.getList().then(
            response => {
                if (response) {
                    this.dataSexos = response;
                    const def = new Sexo();
                    def.id = -1;
                    def.sexo = 'Sexo...';
                    this.dataSexos.unshift(def);
                } else {
                    alert('No se ha podido obtener datos de sexos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.pacienteService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Paciente;
                    this.notificaciones = (this.selectedEntity.notificacion === 1);
                    this.fecNacimiento = TWUtils.stringDateToJson(this.selectedEntity.fecnacimiento);
                }
            ).catch(e => this.handleError(e));
        }
    }

    public onSave() {
        this.selectedEntity.fecnacimiento = TWUtils.arrayDateToString(this.fecNacimiento);
        this.selectedEntity.notificacion = (this.notificaciones) ? 1 : 0;
        this.pacienteService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
