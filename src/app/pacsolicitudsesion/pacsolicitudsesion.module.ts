import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';

import { CalendarModule, DateAdapter } from 'angular-calendar';
// import { CalendarHeaderModule } from './calendarheader/calendarheader.module';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ContextMenuModule } from 'ngx-contextmenu';

import { PacSolicitudSesionComponent } from './pacsolicitudsesion.component';
import { PacSolicitudSesionRoutingModule } from './pacsolicitudsesion-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    PacSolicitudSesionRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    }),
    // CalendarHeaderModule
  ],
  declarations: [
    PacSolicitudSesionComponent],
  exports: [PacSolicitudSesionComponent]
})
export class PacSolicitudSesionModule {}
