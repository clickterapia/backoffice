import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Profesional, ProfesionalService,
    DisponibilidadService,
    SesionService,
    Sesion } from '../shared';
import { CustomDateFormatter } from './custom-date-formatter.provider';
import { Subject } from 'rxjs';
import {
    CalendarEvent,
    CalendarView,
    CalendarDateFormatter
} from 'angular-calendar';
import {
    endOfWeek,
    endOfDay,
    endOfMonth,
    subDays,
    subWeeks,
    subMonths,
    compareAsc
} from 'date-fns';
import { environment } from '../../environments/environment';

type CalendarPeriod = 'day' | 'week' | 'month';

export const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#98FB98',
    secondary: '#98FB98'
  }
};

@Component({
    selector: 'app-pacsolicitudsesion',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./pacsolicitudsesion.component.scss'],
    templateUrl: './pacsolicitudsesion.component.html',
    providers: [
        {
            provide: CalendarDateFormatter,
            useClass: CustomDateFormatter
        },
        ProfesionalService,
        DisponibilidadService,
        SesionService ]
})

export class PacSolicitudSesionComponent implements OnInit {
    modalTitle = 'Disponibilidad Horaria';
    viewDate = new Date();

    events: CalendarEvent[] = [];
    refreshWeek: Subject<any> = new Subject();

    dataDisponibilidades: Array<any> = Array<any>();
    dataSesiones: Array<any> = Array<any>();

    selectedEntity: Profesional;
    selectedID: string;
    selectedIDProfesional: string;
    selectedIDDisponibilidad: string;

    public constructor(
        private route: ActivatedRoute,
        private modal: Modal,
        private profesionalService: ProfesionalService,
        private disponibilidadService: DisponibilidadService,
        private sesionService: SesionService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.selectedIDProfesional = params['id'];
            if (params['iddisponibilidad'] !== null) {
                this.selectedIDDisponibilidad = params['iddisponibilidad'];
            }
            this.getDisponibilidades();
        });
    }

    getDisponibilidades() {
        return this.disponibilidadService.getNewsByIDProfesional(this.selectedIDProfesional).then(
            response => {
                if (response) {
                    this.dataDisponibilidades = response;
                    this.dispToEventos();
                } else {
                    this.showMessage('No se ha podido obtener datos de países!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    dispToEventos(): any {
        this.events = this.dataDisponibilidades.map(o => {
            return {
                id: o.id,
                reservado: o.reservado,
                start: new Date(o.inicio),
                end: new Date(o.fin),
                title: (o.reservado) ? ('Sesión Reservada') : ('Disponible'),
                color: (o.reservado) ? (colors.blue) : (o.gratuito) ? (colors.red) : (colors.yellow),
            };
        });
        this.refreshWeek.next();
        if (this.selectedIDDisponibilidad !== undefined ) {
            this.reservarFromExt(this.selectedIDDisponibilidad);
        }
    }

    reservarFromExt(idDisponibilidad: string) {
        const disponibilidad = this.dataDisponibilidades.find(dispo => {
            return dispo.id === idDisponibilidad;
        });
        const sesion = new Sesion();
        sesion.iddisponibilidad = idDisponibilidad;
        sesion.idpaciente = this.selectedID;
        sesion.idprofesional = this.selectedIDProfesional;
        sesion.fecinicio = disponibilidad.inicio;
        sesion.fecfin = disponibilidad.fin;
        sesion.precio = 0.00;
        sesion.observaciones = '';
        this.sesionService.save(sesion).then(
            response => {
                if (response) {
                    let url = environment.apiMercadoPago.checkOutBaseUrl;
                    url = url.replace('[idpaciente]', this.selectedID);
                    url = url.replace('[idprofesional]', this.selectedIDProfesional);
                    this.showMessage('Los cambios se guardaron correctamente');
                    window.location.href = url;
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    eventClicked({ event }: { event: CalendarEvent }): void {
        const date = new Date(event.start);
        const sesion = this.events.find(ev => compareAsc(ev.start, date) === 0);
        if (sesion.color === colors.yellow) {
            const index = this.events.indexOf(sesion);
            sesion.color = colors.green;
            sesion.title = 'Reservando';
            this.events[index] = sesion;
            this.reservarSesion(sesion.id + '');
        } else if (sesion.color === colors.green) {
            const index = this.events.indexOf(sesion);
            sesion.color = colors.yellow;
            sesion.title = 'Disponible';
            this.events[index] = sesion;
            this.quitarReservaSesion(sesion.id + '');
        }
        this.refreshWeek.next();
    }

    reservarSesion(id: string): any {
        const disponibilidad = this.dataDisponibilidades.find(dispo => {
            return dispo.id === id;
        });
        const sesion = new Sesion();
        sesion.iddisponibilidad = id;
        sesion.idpaciente = this.selectedID;
        sesion.idprofesional = this.selectedIDProfesional;
        sesion.fecinicio = disponibilidad.inicio;
        sesion.fecfin = disponibilidad.fin;
        sesion.precio = 0.00;
        sesion.observaciones = '';
        this.dataSesiones.unshift(sesion);
    }

    quitarReservaSesion(id: string): any {
        const sesion = this.dataSesiones.find(ses => {
            return ses.id === id;
        });
        const index = this.dataSesiones.indexOf(sesion);
        if (index !== -1) {
            this.dataSesiones.splice(index, 1);
        }
    }

    public onSave() {
        this.eventosToDisp();
        if (this.dataSesiones.length === 0) {
            this.showMessage('No se registraron reservas.');
            return;
        }

        this.sesionService.saveSeveral(this.dataSesiones, this.selectedID).then(
            response => {
                if (response) {
                    let url = environment.apiMercadoPago.checkOutBaseUrl;
                    url = url.replace('[idpaciente]', this.selectedID);
                    url = url.replace('[idprofesional]', this.selectedIDProfesional);
                    this.showMessage('Los cambios se guardaron correctamente');
                    window.location.href = url;
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onCancelar() {
        this.events = [];
    }

    eventosToDisp(): any {
        this.dataDisponibilidades = this.events.map(o => {
            return {
                id: o.id,
                inicio: o.start,
                fin: o.end,
                gratuito: (o.color === colors.red) ? 1 : 0,
                reservado: (o.color === colors.green) ? 1 : 0
            };
        });
    }

    testPrevBtnVisible(date: Date) {
        const viewL = CalendarView.Week;
        const val = this.dateIsValid(this.endOfPeriod(viewL, this.subPeriod(viewL, this.viewDate, 1)));
        return val;
    }

    dateIsValid(date: Date): boolean {
        return date >= (new Date());
    }

    endOfPeriod(period: CalendarPeriod, date: Date): Date {
        return {
            day: endOfDay,
            week: endOfWeek,
            month: endOfMonth
        }[period](date);
    }

    subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
        return {
            day: subDays,
            week: subWeeks,
            month: subMonths
        }[period](date, amount);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
