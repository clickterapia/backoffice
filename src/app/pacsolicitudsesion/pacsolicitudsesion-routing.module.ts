import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacSolicitudSesionComponent } from './pacsolicitudsesion.component';

const routes: Routes = [
    {
        path: '',
        component: PacSolicitudSesionComponent,
        data: {
            title: 'Solicitud de sesión',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pac'},
                {title: 'Disponibilidad'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacSolicitudSesionRoutingModule { }
