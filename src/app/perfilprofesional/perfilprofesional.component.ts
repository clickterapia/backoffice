import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { Profesional, ProfesionalService, MercadoPagoService } from '../shared';
import { environment } from '../../environments/environment';

import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'app-form',
    templateUrl: './perfilprofesional.component.html',
    styleUrls: ['./perfilprofesional.component.scss'],
    providers: [
        ProfesionalService,
        MercadoPagoService],
    // changeDetection: ChangeDetectionStrategy.OnPush
})

export class PerfilProfesionalComponent implements OnInit, AfterViewInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil Profesional';

    public selectedID = '';
    public selectedEntity: Profesional;
    public urlMap: string;
    public habilitado: string;
    public notificaciones: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer,
        public modal: Modal,
        private cdRef: ChangeDetectorRef,
        private profesionalService: ProfesionalService,
        private mercadoPagoService: MercadoPagoService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            let vinculado = false;
            if (params['vinculado'] !== undefined) {
                vinculado = true;
                // this.showMessage('Cuenta de mercado pago vinculada.');
            }
            this.onGetEntityData(vinculado);
        });
    }

    ngAfterViewInit(): void {
    }

    public onGetEntityData(vinculado: boolean): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.urlMap = environment.urlMap + this.selectedEntity.provincia + '+'
                        + this.selectedEntity.pais;
                    if (vinculado) {
                        this.getToken();
                    }
                }
            ).catch(e => this.handleError(e));
        }
    }

    private getToken() {
        this.mercadoPagoService.getAccessToken(this.selectedEntity.mp_authorization_code)
        .then(
            response => {
                this.showMessage('Token adquirido');
            })
        .catch(e => this.handleError(e));

    }

    public isNew() {
        return (this.selectedID.length === 0);
    }

    public hasImage() {
        return (this.selectedEntity.fotourl !== '') ? true : false;
    }

    // public getMap() {
    //     const map = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBiDmX5nFGBGp-XYjYNhmC8iQWTxHz7-Sg&q=' + this.locationUrl;
    //     return this.sanitizer.bypassSecurityTrustResourceUrl(map);
    // }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
