import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Profesional, ProfesionalService } from '../../../shared';

@Component({
    selector: 'app-pagos',
    templateUrl: './pagos.component.html',
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        ProfesionalService]
})

export class PagosComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Pagos recibidos';

    public selectedEntity: Profesional;
    public selectedID: string;
    public minDate: {};
    public fecNacimiento: {};

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private profesionalService: ProfesionalService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        const now = new Date();
        now.setFullYear(now.getFullYear() - 21);
        this.minDate = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    public getData() {
        this.getEntityData();
        // const p0 = this.getProvincias();
        // const p1 = this.getPaises();
        // const p2 = this.getSexos();
        // Promise.all([
        //     p0, p1, p2
        // ]).then(
        //     this.onSetEntityData()
        // );
    }

    public getEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.fecNacimiento = TWUtils.stringDateToJson(this.selectedEntity.fecnacimiento);
                }
            ).catch(e => this.handleError(e));
        }
    }

    public onSave() {
        this.selectedEntity.fecnacimiento = TWUtils.arrayDateToString(this.fecNacimiento);
        this.profesionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
