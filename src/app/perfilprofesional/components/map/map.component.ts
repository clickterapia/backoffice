import { Component, Input, SimpleChange, OnChanges, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class MapComponent implements OnChanges {
    @Input() urlMap: string;
    localUrlMap: SafeResourceUrl;

    public constructor(public router: Router,
        private cdRef: ChangeDetectorRef,
        private sanitizer: DomSanitizer) {
        this.localUrlMap = '';
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['urlMap'].currentValue !== null) {
            this.localUrlMap = this.sanitizer.bypassSecurityTrustResourceUrl(changes['urlMap'].currentValue);
            this.cdRef.detectChanges();
        }
    }
}
