import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    Profesional, ProfesionalService } from '../../../shared';

@Component({
    selector: 'app-foto',
    templateUrl: './foto.component.html',
    styleUrls: ['./foto.component.scss'],
    providers: [
        ProfesionalService]
})

export class FotoComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil';

    public selectedEntity: Profesional;
    public selectedID: string;

    imageChangedEvent: any = '';
    croppedImage: any = '';

    fileSelected: File;

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    public getData() {
        // const p0 = this.getProvincias();
        // Promise.all([
            // p0
        // ]).then(
            this.onSetEntityData();
        // );
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public onSave() {
        if (this.fileSelected === undefined) {
            this.showMessage('No se ha seleccionado la imagen');
            return;
        }
        this.profesionalService.uploadFoto(this.selectedEntity.id, this.fileSelected).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar el perfil!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.fileSelected = fileList[0] as File;
        }
    }

    imageCropped(image: string) {
        this.croppedImage = image;
    }

    imageCroppedFile(image: File) {
        this.fileSelected = image;
    }

    imageLoaded() {
    }

    loadImageFailed() {
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
