export * from './filiatorios/filiatorios.component';
export * from './foto/foto.component';
export * from './perfil/perfil.component';
export * from './documentacion/documentacion.component';
export * from './map/map.component';
export * from './pagos/pagos.component';
