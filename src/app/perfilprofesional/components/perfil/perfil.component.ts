import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    Profesional, ProfesionalService,
    EspecialidadService } from '../../../shared';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.scss'],
    providers: [
        ProfesionalService,
        EspecialidadService
    ]
})

export class PerfilComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Perfil';

    public dataEspecialidades = [{'id': -1, 'itemName': 'seleccionar'}];
    selectedItems = [];
    dropdownSettings = {};

    public selectedEntity: Profesional;
    public selectedID: string;
    sesionesgratis: boolean;

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private profesionalService: ProfesionalService,
        private especialidadService: EspecialidadService) {
        this.selectedEntity = new Profesional();
        this.selectedItems = [];
        this.dropdownSettings = {
            singleSelection:  false,
            text: 'Seleccione Especialidades',
            selectAllText: 'Seleccionar Todas',
            unSelectAllText: 'Desdeleccionar Todas',
            enableSearchFilter:  false,
            lazyLoading: true,
        };
        this.sesionesgratis = false;
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    public getData() {
        const p0 = this.getEspecialidades();
        Promise.all([
            p0
        ]).then(
            this.getEntityData()
        );
    }

    public getEspecialidades() {
        return this.especialidadService.getList().then(
            response => {
                if (response) {
                    this.dataEspecialidades = response.map(o => {
                        return { 'id': o.id, 'itemName': o.especialidad };
                    });
                } else {
                    alert('No se ha podido obtener datos de provincias!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.sesionesgratis = (+this.selectedEntity.sesionesgratis === 1);
                    // this.cdRef.detectChanges();
                    this.processEspecialidades();
                }
            ).catch(e => this.handleError(e));
        }
    }

    processEspecialidades(): any {
        if (this.selectedEntity.especialidades.length < 1) {
            return;
        }
        const selectedItems = JSON.parse(this.selectedEntity.especialidades);
        this.selectedItems = selectedItems.map(o => {
            return { 'id': o.id, 'itemName': o.especialidad };
        });
    }

    public onSave() {
        this.selectedEntity.especialidades = this.getSelectedEspecialidades();
        this.selectedEntity.sesionesgratis = (this.sesionesgratis) ? 1 : 0;
        this.profesionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onLink() {
        let uri = environment.apiMercadoPago.redirect_uri;
        uri = uri.replace('[idprofesional]', this.selectedEntity.id);
        uri = uri.replace('[precio]', this.selectedEntity.precio.toString());
        let url = environment.apiMercadoPago.linkAccount.replace('[appID]', environment.apiMercadoPago.appID);
        url = url.replace('[redirect_uri]', encodeURIComponent(uri));
        this.showMessage('Los cambios se guardaron correctamente');
        window.location.href = url;
    }

    getSelectedEspecialidades() {
        let ids = '';
        for (const entry of this.selectedItems) {
            ids += entry.id + ',';
        }
        return ids.slice(0, -1);
    }

    onItemSelect(item: any) {
        // console.log(item);
        // console.log(this.selectedItems);
    }

    OnItemDeSelect(item: any) {
        // console.log(item);
        // console.log(this.selectedItems);
    }

    onSelectAll(items: any) {
        // console.log(items);
    }

    onDeSelectAll(items: any) {
        // console.log(items);
    }

    hasMPAuthCode() {
        return (this.selectedEntity.mp_authorization_code !== undefined) ? (this.selectedEntity.mp_authorization_code.length > 0) : false;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}

interface City {
    name: string;
    code: string;
}
