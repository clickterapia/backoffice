import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Profesional, ProfesionalService,
    Provincia, ProvinciaService,
    Pais, PaisService,
    Sexo, SexoService } from '../../../shared';

@Component({
    selector: 'app-filiatorios',
    templateUrl: './filiatorios.component.html',
    styleUrls: ['./filiatorios.component.scss'],
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        ProfesionalService,
        ProvinciaService,
        PaisService,
        SexoService]
})

export class FiliatoriosComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Datos Filiatorios';

    public dataProvincias: Array<any> = Array<any>();
    public dataProvinciasFilt: Array<any> = Array<any>();
    public dataPaises: Array<any> = Array<any>();
    public dataSexos: Array<any> = Array<any>();

    public selectedEntity: Profesional;
    public selectedID: string;
    public minDate: {};
    public fecNacimiento: {};

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private profesionalService: ProfesionalService,
        private provinciaService: ProvinciaService,
        private paisService: PaisService,
        private sexoService: SexoService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        const now = new Date();
        now.setFullYear(now.getFullYear() - 21);
        this.minDate = TWUtils.stringDateToJson(this.datepipe.transform(now, 'yyyy-MM-dd'));
        this.selectedID = localStorage.getItem('user');
        this.getData();
    }

    public getData() {
        const p0 = this.getProvincias();
        const p1 = this.getPaises();
        const p2 = this.getSexos();
        Promise.all([
            p0, p1, p2
        ]).then(
            this.onSetEntityData()
        );
    }

    public getPaises() {
        return this.paisService.getList().then(
            response => {
                if (response) {
                    this.dataPaises = response;
                    const def = new Pais();
                    def.id = -1;
                    def.pais = 'País...';
                    this.dataPaises.unshift(def);
                } else {
                    alert('No se ha podido obtener datos de países!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getProvincias() {
        return this.provinciaService.getList().then(
            response => {
                if (response) {
                    this.dataProvincias = response;
                    const def = new Provincia();
                    def.id = -1;
                    def.provincia = 'Provincia...';
                    this.dataProvincias.unshift(def);
                    this.dataProvinciasFilt = this.dataProvincias;
                } else {
                    alert('No se ha podido obtener datos de provincias!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public getSexos() {
        return this.sexoService.getList().then(
            response => {
                if (response) {
                    this.dataSexos = response;
                    const def = new Sexo();
                    def.id = -1;
                    def.sexo = 'Sexo...';
                    this.dataSexos.unshift(def);
                } else {
                    alert('No se ha podido obtener datos de sexos!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.fecNacimiento = TWUtils.stringDateToJson(this.selectedEntity.fecnacimiento);
                }
            ).catch(e => this.handleError(e));
        }
    }

    public onPaisChange() {
        this.dataProvinciasFilt = this.dataProvincias.filter(
            prov =>
            +prov.idpais === +this.selectedEntity.idpais || +prov.idpais === -1);
        if (this.dataProvinciasFilt.length === 0) {
            this.showMessage('País sin provincias');
        }
        this.selectedEntity.idprovincia = -1;
    }

    public onSave() {
        this.selectedEntity.fecnacimiento = TWUtils.arrayDateToString(this.fecNacimiento);
        this.profesionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
