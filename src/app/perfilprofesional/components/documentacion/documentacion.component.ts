import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    Profesional, ProfesionalService } from '../../../shared';

@Component({
    selector: 'app-documentacion',
    templateUrl: './documentacion.component.html',
    styleUrls: ['./documentacion.component.scss'],
    providers: [
        ProfesionalService]
})

export class DocumentacionComponent implements OnInit {
    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Documentación';

    hasBaseDropZoneOver = false;
    hasAnotherDropZoneOver = false;

    public selectedEntity: Profesional;
    public selectedID: string;

    dniUrl: any;

    public constructor(public router: Router,
        public modal: Modal,
        private profesionalService: ProfesionalService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.onSetEntityData();
    }

    public onSetEntityData(): any {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                    this.dniUrl = this.selectedEntity.dniurl;
                }
            ).catch(e => this.handleError(e));
        }
    }

    public onSave() {
        this.profesionalService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    alert('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    isSeted(url: string) {
        if  (url === undefined) {
            return false;
        } else if (url === null) {
            return false;
        } else if (url.length < 1) {
            return false;
        }

        return true;

        // return this.selectedEntity.dniurl !== null && this.selectedEntity.dniurl
    }

    fileChangeDni(event: any): void {
        // if (event.target.files && event.target.files[0]) {
        //     const reader = new FileReader();
        //     reader.onload = (eventLoc: ProgressEvent) => {
        //         this.dniUrl = (<FileReader>eventLoc.target).result;
        //     };
        //     reader.readAsDataURL(event.target.files[0]);
        // }
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.profesionalService.uploadDni(this.selectedEntity.id, file).then(
                response => {
                    this.selectedEntity.dniurl = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    fileChangeTitulo(event: any): void {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.profesionalService.uploadTitulo(this.selectedEntity.id, file).then(
                response => {
                    this.selectedEntity.titulourl = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    fileChangeMatricula(event: any): void {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            this.profesionalService.uploadMatricula(this.selectedEntity.id, file).then(
                response => {
                    this.selectedEntity.matriculaurl = response.generatedName;
                }).catch(e => this.handleError(e));
        }
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
