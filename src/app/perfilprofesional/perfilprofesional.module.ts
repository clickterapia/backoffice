import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { AvatarModule } from 'ngx-avatar';

// relative import
import { PerfilProfesionalRoutingModule } from './perfilprofesional-routing.module';
import { PerfilProfesionalComponent } from './perfilprofesional.component';
import {
    FiliatoriosComponent,
    FotoComponent,
    PerfilComponent,
    DocumentacionComponent,
    MapComponent,
    PagosComponent
} from './components/';

@NgModule({
    imports: [
        CommonModule,
        PerfilProfesionalRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2CompleterModule,
        ImageCropperModule,
        AngularMultiSelectModule,
        FileUploadModule,
        AvatarModule
    ],
    declarations: [
      PerfilProfesionalComponent,
      FiliatoriosComponent,
      FotoComponent,
      PerfilComponent,
      DocumentacionComponent,
      MapComponent,
      PagosComponent
    ]
})
export class PerfilProfesionalModule { }
