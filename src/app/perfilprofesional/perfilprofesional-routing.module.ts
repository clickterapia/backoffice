import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilProfesionalComponent } from './perfilprofesional.component';

const routes: Routes = [
    {
        path: '',
        component: PerfilProfesionalComponent,
        data: {
            title: 'Perfil del Profesional',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard1'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PerfilProfesionalRoutingModule { }
