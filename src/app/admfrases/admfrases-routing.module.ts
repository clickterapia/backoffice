import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmFrasesComponent } from './admfrases.component';

const routes: Routes = [
    {
        path: '',
        component: AdmFrasesComponent,
        data: {
            title: 'Frases',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Frases'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmFrasesRoutingModule { }
