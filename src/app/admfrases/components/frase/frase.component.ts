import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    FraseProfesional, FraseProfesionalService } from '../../../shared';

@Component({
    selector: 'app-frase',
    templateUrl: './frase.component.html',
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        FraseProfesionalService ]
})

export class FraseComponent implements OnInit, OnChanges {
    @Input() selectedEntity: FraseProfesional;
    @Output() entityUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Frase';

    selectedID: string;
    fecInicio = {};
    fecFin = {};

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private fraseService: FraseProfesionalService) {
            this.selectedEntity = new FraseProfesional();
            this.fecInicio = {};
            this.fecFin = {};
    }

    public ngOnInit(): void {
        this.onCancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;

            if (!TWUtils.isNullDate(this.selectedEntity.fecinicio)) {
                this.fecInicio = TWUtils.stringDateToJson(this.selectedEntity.fecinicio);
            }
            if (!TWUtils.isNullDate(this.selectedEntity.fecfin)) {
                this.fecFin = TWUtils.stringDateToJson(this.selectedEntity.fecfin);
            }
        }
    }

    hasEntity(): boolean {
        return this.selectedEntity.id !== '' && this.selectedEntity.id !== undefined;
    }

    onSave() {
        this.selectedEntity.fecinicio = TWUtils.arrayDateToString(this.fecInicio);
        this.selectedEntity.fecfin = TWUtils.arrayDateToString(this.fecFin);
        if (this.selectedEntity.fecinicio.length === 0 || this.selectedEntity.fecfin.length === 0) {
            this.showMessage('Faltan completar datos!!');
            return;
        }
        this.fraseService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.selectedID = response;
                    this.selectedEntity.id = response;
                    this.entityUpdated.emit(true);
                    this.onCancelar();
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos de frases!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onEliminar() {
        this.fraseService.delete(this.selectedEntity).then(
            response => {
                const resp = Number(response);
                if ( resp > 0) {
                    this.showMessage('El registro se ha eliminado correctamente');
                    this.onCancelar();
                    this.entityUpdated.emit(true);
                } else if (resp === -1) {
                    this.showMessage('El registro no se ha eliminado porque se está utilizando.');
                } else {
                    this.showMessage('El registro no se ha eliminado por error de sistema.');
                }
            }
        ).catch(e => this.handleError(e));
    }

    onCancelar() {
        this.selectedEntity = new FraseProfesional();
        this.fecInicio = {};
        this.fecFin = {};
    }

    isAlza(value: any) {
        return +this.selectedEntity.tendencia === +value;
    }

    setAlza(activo: any) {
        this.selectedEntity.tendencia = +activo;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
