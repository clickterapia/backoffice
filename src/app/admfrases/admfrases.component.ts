import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyAR, NgbDateCustomParserFormatter,
    FraseProfesional, FraseProfesionalService } from '../shared';

@Component({
    selector: 'app-admfrases',
    // styleUrls: ['./admfrases.component.scss'],
    templateUrl: './admfrases.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        FraseProfesionalService
    ]
})
export class AdmFrasesComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Frases';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataFrases: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            frase: { title: 'Frase', filter: false, },
            tendencia: { title: 'Tendencia', filter: false, },
            fecinicio: {
                title: 'Desde', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            },
            fecfin: {
                title: 'Hasta', filter: false,
                type: 'html',
                width: '10%',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy') + '</div>';
                }
            }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public ngxScrollToDestination: string;

    public constructor(public router: Router,
        private scrollToService: ScrollToService,
        public modal: Modal,
        private datePipe: DatePipe,
        private cdRef: ChangeDetectorRef,
        private fraseService: FraseProfesionalService) {
            this.selectedEntity = new FraseProfesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getFrases();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        this.getFrases();
    }

    getFrases(): any {
        return this.fraseService.getList().then(
            response => {
                if (response) {
                    this.dataFrases = response;
                    this.source = new LocalDataSource(this.dataFrases);
                } else {
                    this.showMessage('No se ha podido obtener datos de frases!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as FraseProfesional;
        const config: ScrollToConfigOptions = {
                target: 'item'
            };
        this.scrollToService.scrollTo(config);
    }

    onNew() {
        this.selectedEntity = new FraseProfesional();
        const config: ScrollToConfigOptions = {
                target: 'item'
            };
        this.scrollToService.scrollTo(config);
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'consulta', search: query },
            { field: 'tipo', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
