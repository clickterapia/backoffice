import { Component, OnInit,
  TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { TWUtils, SesionService } from '../shared';

@Component({
  selector: 'app-proreservas',
  styleUrls: ['./proreservas.component.scss'],
  templateUrl: './proreservas.component.html',
  providers: [
    DatePipe,
    {provide: LOCALE_ID, useValue: 'es-AR'},
    SesionService
  ]
})
export class ProReservasComponent implements OnInit {
  public modalTitle = 'Reservas';
  modalContent: TemplateRef<any>;

  settings = {
    noDataMessage: 'Datos no disponibles',
    pager: { display: true, perPage: 10 },
    columns: {
        usuario: { title: 'Paciente', filter: false, },
        fecinicio: {
          title: 'Fecha', filter: false,
          type: 'html',
          valuePrepareFunction: (value) => {
            return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
          }
        },
        ultima: {
          title: 'Última sesión', filter: false,
          type: 'html',
          valuePrepareFunction: (value) => {
            return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
          }
        },
    },
    actions: { add: false, edit: false, delete: false, },
    attr: {
        class: 'table dataTable table-striped table-bordered',
    }
  };
  source: LocalDataSource;

  dataReservas: Array<any> = Array<any>();
  selectedID;

  public constructor(public router: Router,
    private route: ActivatedRoute,
      public modal: Modal,
      private datePipe: DatePipe,
      private sesionService: SesionService) {
  }

  public ngOnInit(): void {
    this.selectedID = localStorage.getItem('user');
    this.route.params.subscribe(params => {
        this.getReservas();
    });
  }

  getReservas() {
      return this.sesionService.getReservasByIDProfesional(this.selectedID).then(
          response => {
              if (response) {
                this.dataReservas = response;
                this.source = new LocalDataSource(this.dataReservas);
              } else {
                this.showMessage('No se han podido obtener datos de reservas!!');
              }
          }
      ).catch(e => this.handleError(e));
  }

  public userRowSelect(event: any): any {
      this.router.navigate(['/propaciente', event.data.idpaciente]);
  }

  onSearch(query: string) {
      if (query.length === 0) {
          this.source.setFilter([]);
          return;
      }

      this.source.setFilter([
          { field: 'usuario', search: query }
      ], false);
  }

  showMessage(message) {
    const msg = `<p class="font-weight-normal">` + message + `</p>`;
    const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
  }

  private handleError(error: any): Promise<any> {
      this.showMessage('Error: ' + error);
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
  }
}
