import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProReservasComponent } from './proreservas.component';

const routes: Routes = [
    {
        path: '',
        component: ProReservasComponent,
        data: {
            title: 'Reservas',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Reservas'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProReservasRoutingModule { }
