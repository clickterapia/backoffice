import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';

import { PacSesionDetalleComponent } from './pacsesiondetalle.component';
import { PacSesionDetalleRoutingModule } from './pacsesiondetalle-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    PacSesionDetalleRoutingModule
  ],
  declarations: [
    PacSesionDetalleComponent],
  exports: [PacSesionDetalleComponent]
})
export class PacSesionDetalleModule {}
