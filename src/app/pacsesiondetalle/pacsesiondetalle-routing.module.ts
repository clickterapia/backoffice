import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacSesionDetalleComponent } from './pacsesiondetalle.component';

const routes: Routes = [
    {
        path: '',
        component: PacSesionDetalleComponent,
        data: {
            title: 'Detalle de Sesión Terapéutica',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pac'},
                {title: 'Detalle de Sesión Terapéutica'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacSesionDetalleRoutingModule { }
