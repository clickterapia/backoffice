import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProPremiumComponent } from './propremium.component';

const routes: Routes = [
    {
        path: '',
        component: ProPremiumComponent,
        data: {
            title: 'Premium',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Premium'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProPremiumRoutingModule { }
