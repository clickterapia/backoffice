import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

// relative import
import { ProPremiumRoutingModule } from './propremium-routing.module';
import { ProPremiumComponent } from './propremium.component';
import {
    PremiumComponent
} from './components/premium.component';

@NgModule({
    imports: [
        CommonModule,
        Ng2SmartTableModule,
        ProPremiumRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot()
    ],
    declarations: [
        ProPremiumComponent,
        PremiumComponent
    ]
})
export class ProPremiumModule { }
