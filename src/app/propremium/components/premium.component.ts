import { Component, OnInit, Input, Output, EventEmitter, SimpleChange  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { LOCALE_ID } from '@angular/core';

import {
    Premium, PremiumService } from '../../shared';

@Component({
    selector: 'app-premium',
    templateUrl: './premium.component.html',
    styleUrls: ['./premium.component.scss'],
    providers: [
        PremiumService,
        {provide: LOCALE_ID, useValue: 'es-AR'}]
})

export class PremiumComponent implements OnInit {
    @Input() selectedEntity: Premium;
    @Output() itemUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Premium';

    public selectedIDProfesional: number;

    public constructor(public router: Router, private route: ActivatedRoute,
        private modal: Modal,
        private premiumService: PremiumService) {
        this.selectedEntity = new Premium();
    }

    public ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.selectedIDProfesional = +params['id'];
        });
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;
        }
    }

    isContratado() {
        return this.selectedEntity.contratado === 1;
    }

    public onContratar() {
        // TODO: desarrollar compra.
        // TODO: agregar campo de precio.
        this.showMessage('Proceso de compra premium no implementado');
        // this.premiumService.save(this.selectedEntity).then(
        //     response => {
        //         if (response) {
        //             this.selectedIDProfesional = Number(response);
        //             this.showMessage('La cobranza se guardó correctamente');
        //         } else {
        //             alert('No se ha podido guardar la cobranza!!');
        //         }
        //     }
        // ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
