import { Component, OnInit, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { Premium, PremiumService, TWUtils } from '../shared';

@Component({
    selector: 'app-form',
    templateUrl: './propremium.component.html',
    styleUrls: ['./propremium.component.scss'],
    providers: [
        PremiumService,
        CurrencyPipe,
        {provide: LOCALE_ID, useValue: 'es-AR'} ]
})

export class ProPremiumComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'ProPremium';

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            titulo: { title: 'Paquete', filter: false, },
            paquetepremium: { title: 'Descripción', filter: false, },
            duracion: { title: 'Duración', filter: false, },
            contratado: {
                title: 'Contratado',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    if (+value === 1) {
                        return `<i class="fa fa-check"></i>`;
                    } else {
                        return ``;
                    }
                }
            },
            costo: {
                title: 'costo',
                filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return this.cp.transform(value, 'ARS', true, '1.2-2');
                }}
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered table-hover',
        },
    };
    source: LocalDataSource;
    public data: Array<any> = Array<any>();
    public selectedEntity: Premium = null;
    public selectedIDProfesional: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private cp: CurrencyPipe,
        private premiumService: PremiumService) {
        this.selectedEntity = new Premium();
    }

    public ngOnInit(): void {
        this.selectedIDProfesional = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.getData();
        });
    }

    handleItemUpdated(user) {
        this.getData();
    }

    public getData() {
        if (this.selectedIDProfesional.length > 0) {
            this.premiumService.getByIDProfesional(this.selectedIDProfesional).then(
                response => {
                    if (response) {
                        this.data = response;
                        this.source = new LocalDataSource(this.data);
                    } else {
                        this.showMessage('No se pudo obtener los datos de paquetes premium!!');
                    }
                }
                ).catch(e => this.handleError(e));
        }
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }
        this.source.setFilter([
            { field: 'titulo', search: query, },
            { field: 'paquetepremium', search: query, }
        ], false);
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data;
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
