import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ViewEncapsulation,
    OnInit
} from '@angular/core';
import {
    CalendarEvent,
    CalendarEventTitleFormatter,
    CalendarView
} from 'angular-calendar';
import { DayViewHourSegment } from 'calendar-utils';
import { Subject } from 'rxjs';
import {
    compareAsc,
    endOfWeek,
    isWithinRange,
    endOfDay,
    endOfMonth,
    subDays,
    subWeeks,
    subMonths,
    addWeeks,
    differenceInCalendarWeeks,
    addHours
} from 'date-fns';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { Profesional, ProfesionalService, DisponibilidadService } from '../shared';
import { Router, ActivatedRoute } from '@angular/router';

type CalendarPeriod = 'day' | 'week' | 'month';

export const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
    selector: 'app-prodisponibilidad',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./prodisponibilidad.component.scss'],
    templateUrl: 'prodisponibilidad.component.html',
    providers: [
        ProfesionalService,
        DisponibilidadService ],
    encapsulation: ViewEncapsulation.None
})

export class ProDisponibilidadComponent implements OnInit {
    modalTitle = 'Disponibilidad Horaria';
    viewDate = new Date();

    events: CalendarEvent[] = [];
    copyEventList: CalendarEvent[] = [];

    dragToCreateActive = false;
    refreshWeek: Subject<any> = new Subject();

    dataDisponibilidades: Array<any> = Array<any>();
    selectedEntity: Profesional;
    selectedID: string;
    newEvent: boolean;

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        private cdr: ChangeDetectorRef,
        private modalDBox: Modal,
        private profesionalService: ProfesionalService,
        private disponibilidadService: DisponibilidadService) {
        this.selectedEntity = new Profesional();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getDisponibilidades();
    }

    getDisponibilidades() {
        return this.disponibilidadService.getNewsByIDProfesional(this.selectedID).then(
            response => {
                if (response) {
                    this.dataDisponibilidades = response;
                    this.dispToEventos();
                } else {
                    this.showMessage('No se ha podido obtener datos de países!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    dispToEventos(): any {
        this.events = this.dataDisponibilidades.map(o => {
            return {
                id: o.id,
                start: new Date(o.inicio),
                end: new Date(o.fin),
                title: 'Disponible',
                color: colors.blue, // (o.reservado) ? ('Sesión Reservada') : (o.gratuito) ? ('Sesión Gratuita') : ('Sesión Regular'),
                // color: (o.reservado) ? (colors.blue) : (o.gratuito) ? (colors.red) : (colors.yellow),
                // actions: (o.reservado) ? [] : this.actions
            };
        });
        this.refreshWeek.next();
    }

    startDragToCreate(
        segment: DayViewHourSegment,
        mouseDownEvent: MouseEvent,
        segmentElement: HTMLElement
    ) {
        if (this.dateIsValid(segment.date)) {
            const endDate =  addHours(segment.date, 1);
            const dragToSelectEvent: CalendarEvent = {
                id: this.events.length,
                title: 'Disponible',
                start: segment.date,
                end: endDate,
                meta: {
                    tmpEvent: true
                }
            };
            this.events = [...this.events, dragToSelectEvent];
            const segmentPosition = segmentElement.getBoundingClientRect();
            this.dragToCreateActive = true;
            const endOfView = endOfWeek(this.viewDate);
        }
    }

    dateIsValid(date: Date): boolean {
        return date >= (new Date());
    }

    setNewOrDelete(isNew: boolean) {
        this.newEvent = isNew;
    }

    eventClicked(event) {
        this.events = this.events.filter(
            ev => {
                const result = compareAsc(ev.start, event.event.start);
                return (result !== 0);
            });
        this.refreshWeek.next();
    }

    copiarSemana() {
        const findeSemana = endOfWeek(new Date());
        this.copyEventList = this.events.filter(ev => {
            const result = isWithinRange(ev.start, new Date(), findeSemana);
            return result;
        });
    }

    pegarSemana() {
        const firstDate = this.copyEventList[0];
        const dif = differenceInCalendarWeeks(this.viewDate, firstDate.start);
        this.copyEventList.forEach(evc => {
            const newEvent = Object.assign({}, evc); // Object.create(evc);
            newEvent.id = this.events.length;
            newEvent.start = addWeeks(evc.start, dif);
            newEvent.end = addWeeks(evc.end, dif);
            const indx = this.events.findIndex(ev => compareAsc(ev.start, newEvent.start) === 0);
            if (indx === -1) {
                this.events.push(newEvent);
            }
        });
        this.refreshWeek.next();
    }

    testPrevBtnVisible(date: Date) {
        const viewL = CalendarView.Week;
        const val = this.dateIsValid(this.endOfPeriod(viewL, this.subPeriod(viewL, this.viewDate, 1)));
        return val;
    }

    endOfPeriod(period: CalendarPeriod, date: Date): Date {
        return {
            day: endOfDay,
            week: endOfWeek,
            month: endOfMonth
        }[period](date);
    }

    subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
        return {
            day: subDays,
            week: subWeeks,
            month: subMonths
        }[period](date, amount);
    }

    eventosToDisp(): any {
        this.dataDisponibilidades = this.events.map(o => {
            return {
                id: o.id,
                inicio: o.start,
                fin: o.end,
                reservado: 0,
                gratuito: 0,
            };
        });
    }

    public onSave() {
        this.eventosToDisp();
        this.disponibilidadService.saveSeveral(this.selectedID, this.dataDisponibilidades).then(
            response => {
                if (response) {
                    this.showMessage('Los cambios se guardaron correctamente');
                } else {
                    this.showMessage('No se ha podido guardar los datos filiatorios!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modalDBox.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
