import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';

import { ContextMenuModule } from 'ngx-contextmenu';

import { ProDisponibilidadComponent } from './prodisponibilidad.component';
import { ProDisponibilidadRoutingModule } from './prodisponibilidad-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    ProDisponibilidadRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    })
  ],
  declarations: [
    ProDisponibilidadComponent],
  exports: [ProDisponibilidadComponent]
})
export class ProDisponibilidadModule {}
