import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProDisponibilidadComponent } from './prodisponibilidad.component';

const routes: Routes = [
    {
        path: '',
        component: ProDisponibilidadComponent,
        data: {
            title: 'Disponibilidad del Profesional',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Disponibilidad'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProDisponibilidadRoutingModule { }
