import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { Router } from '@angular/router';
import { Profile } from '../guard/auth.guard';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Profesional, ProfesionalService,
    Paciente, PacienteService,
    Usuario, UsuarioService } from '../';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  providers: [
    ProfesionalService,
    PacienteService,
    UsuarioService]
})

export class NavigationComponent implements OnInit, AfterViewInit {
  name: string;
  public config: PerfectScrollbarConfigInterface = {};
  public selectedID = '';
  public selectedEntity: Profesional | Paciente | Usuario;
  nombrePerfil: string;

    constructor(
        private router: Router,
        public modal: Modal,
        private profesionalService: ProfesionalService,
        private pacienteService: PacienteService,
        private usuarioService: UsuarioService) {
            switch (localStorage.getItem('profile')) {
                case Profile.professional:
                    this.selectedEntity = new Profesional();
                    break;
                case Profile.patient:
                    this.selectedEntity = new Paciente();
                    break;
                case Profile.admin:
                    this.selectedEntity = new Usuario();
                    break;
            }
    }

    // This is for Notifications
    notifications: Object[] = [{
        round: 'round-danger',
        icon: 'ti-link',
        title: 'Luanch Admin',
        subject: 'Just see the my new admin!',
        time: '9:30 AM'
    }, {
        round: 'round-success',
        icon: 'ti-calendar',
        title: 'Event today',
        subject: 'Just a reminder that you have event',
        time: '9:10 AM'
    }, {
        round: 'round-info',
        icon: 'ti-settings',
        title: 'Settings',
        subject: 'You can customize this template as you want',
        time: '9:08 AM'
    }, {
        round: 'round-primary',
        icon: 'ti-user',
        title: 'Pavan kumar',
        subject: 'Just see the my admin!',
        time: '9:00 AM'
    }];

    // This is for Mymessages
    mymessages: Object[] = [{
        useravatar: 'assets/images/users/1.jpg',
        status: 'online',
        from: 'Pavan kumar',
        subject: 'Just see the my admin!',
        time: '9:30 AM'
    }, {
        useravatar: 'assets/images/users/2.jpg',
        status: 'busy',
        from: 'Sonu Nigam',
        subject: 'I have sung a song! See you at',
        time: '9:10 AM'
    }, {
        useravatar: 'assets/images/users/2.jpg',
        status: 'away',
        from: 'Arijit Sinh',
        subject: 'I am a singer!',
        time: '9:08 AM'
    }, {
        useravatar: 'assets/images/users/4.jpg',
        status: 'offline',
        from: 'Pavan kumar',
        subject: 'Just see the my admin!',
        time: '9:00 AM'
    }];

    ngAfterViewInit() {
        const set = function() {
            const width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
            const topOffset = 0;
            if (width < 1170) {
                $('#main-wrapper').addClass('mini-sidebar');
            } else {
                $('#main-wrapper').removeClass('mini-sidebar');
            }
        };
        $(window).ready(set);
        $(window).on('resize', set);

        $('.search-box a, .search-box .app-search .srh-btn').on('click', function () {
            $('.app-search').toggle(200);
        });

        $('body').trigger('resize');
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.onGetEntityData();
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            switch (localStorage.getItem('profile')) {
                case Profile.professional:
                    this.getProfesional();
                    break;
                case Profile.patient:
                    this.getPaciente();
                    break;
                case Profile.admin:
                    this.getAdmin();
                    break;
            }
        }
    }

    getProfesional() {
        this.profesionalService.getByID(this.selectedID).then(
            response => {
                this.selectedEntity = response as Profesional;
                this.nombrePerfil = this.selectedEntity.nombre;
            }
        ).catch(e => this.handleError(e));
    }

    getPaciente() {
        this.pacienteService.getByID(this.selectedID).then(
            response => {
                this.selectedEntity = response as Paciente;
                this.nombrePerfil = this.selectedEntity.usuario;
            }
        ).catch(e => this.handleError(e));
    }

    getAdmin() {
        this.usuarioService.getByID(+this.selectedID).then(
            response => {
                this.selectedEntity = response as Usuario;
                this.nombrePerfil = this.selectedEntity.usuario;
                this.selectedEntity.fotourl = '';
            }
        ).catch(e => this.handleError(e));
    }

    goToPerfil() {
        switch (localStorage.getItem('profile')) {
            case Profile.professional:
                this.router.navigate(['/perfilprofesional']);
                break;
            case Profile.patient:
                this.router.navigate(['/pacperfil']);
                break;
            case Profile.admin:
                this.router.navigate(['/admperfil']);
                break;
        }
    }

    public hasImage() {
        return (this.selectedEntity.fotourl) ? (this.selectedEntity.fotourl.length > 0) : false;
    }

    getName() {
        let value = '';
        switch (localStorage.getItem('profile')) {
            case Profile.professional:
                const profesional = this.selectedEntity as Profesional;
                value = profesional.nombre + ' ' + profesional.apellido;
                break;
            case Profile.patient:
                const paciente = this.selectedEntity as Paciente;
                value = paciente.usuario;
                break;
            case Profile.admin:
                const usuario = this.selectedEntity as Usuario;
                value = usuario.usuario;
                break;
        }
        return value;
    }

    public onLogout() {
        switch (localStorage.getItem('profile')) {
            case Profile.admin:
            this.router.navigate(['/authentication/login-adm']);
            break;
        case Profile.professional:
            this.router.navigate(['/authentication/login-pro']);
            break;
        case Profile.patient:
            this.router.navigate(['/authentication/login-pac']);
            break;
        default:
            this.router.navigate(['/authentication/login-adm']);
            break;
        }
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
