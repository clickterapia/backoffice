import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

export enum Profile {
    admin = '0',
    professional = '1',
    patient = '2',
    loggedout = '-1'
}

@Injectable()
export class AuthGuard implements CanActivate {
    readonly MINUTES_UNITL_AUTO_LOGOUT = 60; // in mins
    readonly CHECK_INTERVAL = 1000; // in ms
    readonly STORE_KEY = 'lastAction';

    constructor(private router: Router) {
        this.check();
        this.initListener();
        this.initInterval();
    }

    canActivate() {
        if (localStorage.getItem('isLoggedin')) {
            // if (localStorage.getItem('profile') !== Profile.loggedout) {
                return true;
            // }
        }

        switch (localStorage.getItem('profile')) {
            case Profile.admin:
            this.router.navigate(['/authentication/login-adm']);
            break;
            case Profile.professional:
            this.router.navigate(['/authentication/login-pro']);
            break;
            case Profile.patient:
            this.router.navigate(['/authentication/login-pac']);
            break;
            default:
            this.router.navigate(['/authentication/login-adm']);
            break;
        }
        return false;
    }

    get lastAction() {
        return parseInt(localStorage.getItem(this.STORE_KEY), 10);
    }

    set lastAction(value) {
        localStorage.setItem(this.STORE_KEY, value + '');
    }

    initListener() {
        document.body.addEventListener('click', () => this.reset());
    }

    reset() {
        this.lastAction = Date.now();
    }

    initInterval() {
        setInterval(() => {
            this.check();
      }, this.CHECK_INTERVAL);
    }

    check() {
      const now = Date.now();
      const timeleft = this.lastAction + this.MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
      const diff = timeleft - now;
      const isTimeout = diff < 0;

      if (isTimeout && localStorage.getItem('isLoggedin')) {
        // this.router.navigate(['/authentication/login-pro']);
        switch (localStorage.getItem('profile')) {
            case Profile.admin:
            this.router.navigate(['/authentication/login-adm']);
            break;
            case Profile.professional:
            this.router.navigate(['/authentication/login-pro']);
            break;
            case Profile.patient:
            this.router.navigate(['/authentication/login-pac']);
            break;
            default:
            this.router.navigate(['/authentication/login-adm']);
            break;
        }
      }
    }
}
