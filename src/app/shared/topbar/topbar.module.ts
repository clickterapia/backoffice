import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TopBarComponent } from './topbar.component';

@NgModule({
    imports: [
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [TopBarComponent],
    exports: [TopBarComponent]
})

export class TopBarModule {
}
