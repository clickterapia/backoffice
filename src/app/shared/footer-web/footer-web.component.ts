import { Component, OnInit } from '@angular/core';

import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { ContactoWebService, ContactoWeb } from '../api-services/contactoweb.service';

@Component({
    selector: 'app-footerweb',
    templateUrl: './footer-web.component.html',
    styleUrls: ['./footer-web.component.scss'],
    providers: [ ContactoWebService ]
})

export class FooterWebComponent implements OnInit {
    modalTitle: 'Clickterapia';
    mail: string;

    constructor(
        public modal: Modal,
        private contactoWebService: ContactoWebService) {
        this.mail = '';
    }

    ngOnInit() {
    }

    onEnviar() {
        const contactoWeb = new ContactoWeb();
        contactoWeb.email = this.mail;
        this.contactoWebService.save(contactoWeb).then(
            response => {
                if (response) {
                    this.mail = '';
                    this.showMessage('Muchas gracias. Pronto nos pondremos en contacto.');
                } else {
                    this.showMessage('No se ha podido enviar tu mail. Por favor intentar mas tarde.');
                }
            }
        ).catch(e => this.handleError(e));
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
        .size('sm')
        .showClose(false)
        .isBlocking(true)
        .title(this.modalTitle)
        .body(msg)
        .okBtn('Aceptar')
        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
