import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { FooterWebComponent } from './footer-web.component';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule,
        NgbModule.forRoot(),
        FormsModule
    ],
    declarations: [FooterWebComponent],
    exports: [FooterWebComponent]
})
export class FooterWebModule {
}
