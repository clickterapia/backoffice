import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar-web.component.html',
    styleUrls: ['./navbar-web.component.scss']
})

export class NavbarWebComponent implements OnInit {
    isCollapsed = true;
    constructor(public router: Router) { }

    ngOnInit() {
    }

    onGoToPage2() {
        return true;
    }
}
