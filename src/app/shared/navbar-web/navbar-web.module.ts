import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarWebComponent } from './navbar-web.component';

@NgModule({
    imports: [
        NgbModule.forRoot()
    ],
    declarations: [NavbarWebComponent],
    exports: [NavbarWebComponent]
})

export class NavbarWebModule {
}
