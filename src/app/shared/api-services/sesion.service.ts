import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export enum SesionState {
    noIniciada = 0,
    iniciada = 1,
    finalizada = 2,
    inexistente = -1
}

export class Sesion {
    public id: string;
    public fecinicio: string;
    public fecfin: string;
    public iddisponibilidad: string;
    public idprofesional: string;
    public nombre: string;
    public apellido: string;
    public idpaciente: string;
    public usuario: string;
    public observaciones: string;
    public precio: number;
    public pagado: number;
    public realizado: number;
    public ultima: string;
    public profesional: string;
    public titulo: string;

    constructor() {
        this.id = '';
        this.fecinicio = '0000-00-00 00:00:00';
        this.fecfin = '';
        this.titulo = '';
    }
}

export class EstadoCuenta {
    public idprofesional: string;
    public fecinicio: string;
    public fecfin: string;
    public cantidad: number;
    public monto: number;
}

@Injectable()
export class SesionService {
    readonly GET_BYIDPACIENTEEIDPROFESIONAL = 'byidpacienteeidprofesional';
    readonly GET_RESERVASBYIDPROFESIONAL = 'reservasbyidprofesional';
    readonly GET_ESTADOCUENTABYIDPROFESIONAL = 'estadocuentabyidprofesional';
    readonly GET_BYIDPACIENTE = 'byidpaciente';
    readonly GET_SIGUIENTE = 'siguiente';
    readonly POST_VARIOS = 'varios';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'sesion';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Sesion[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Sesion[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByID(id: string): Promise<Sesion> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Sesion;
                return entity;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByIDPacienteeIDProfesional(idpaciente: string, idprofesional: string): Promise<Sesion[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDPACIENTEEIDPROFESIONAL}/${idpaciente}/${idprofesional}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Sesion[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getReservasByIDProfesional(idprofesional: string): Promise<Sesion[]> {
        const customUrl = `${this.apiUrl}/${this.GET_RESERVASBYIDPROFESIONAL}/${idprofesional}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Sesion[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getEstadoCuentaByIDProfesional(entity: EstadoCuenta): Promise<EstadoCuenta> {
        // tslint:disable-next-line:max-line-length
        const customUrl = `${this.apiUrl}/${this.GET_ESTADOCUENTABYIDPROFESIONAL}/${entity.idprofesional}/${entity.fecinicio}/${entity.fecfin}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                return body[0] as EstadoCuenta;
            }
            )
            .catch(e => this.handleError(e));
    }

    getByIDPaciente(idpaciente: string): Promise<Sesion[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDPACIENTE}/${idpaciente}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Sesion[];
                return entities;
            }
            )
            .catch(e => this.handleError(e));
    }

    getNextSesion(idpaciente: string): Promise<Sesion> {
        const customUrl = `${this.apiUrl}/${this.GET_SIGUIENTE}/${idpaciente}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Sesion;
                return entity;
            })
            .catch(e => this.handleError(e));
    }

    save(entity: Sesion): Promise<any> {
        if (entity.id.length > 0) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Sesion): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    private put(entity: Sesion): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    public saveSeveral(entities: Sesion[], idpaciente: string): Promise<any> {
        const url = `${this.apiUrl}/${this.POST_VARIOS}/${idpaciente}`;
        return this.http
            .post(url, JSON.stringify(entities), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    delete(entity: Sesion): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }
            })
            .catch(error => this.handleError(error));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
