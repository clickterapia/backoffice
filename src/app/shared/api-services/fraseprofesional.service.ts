import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class FraseProfesional {
    public id: string;
    public frase: string;
    public tendencia: number;
    public fecinicio: string;
    public fecfin: string;
    constructor() {
        this.frase = '';
        this.tendencia = 0;
    }
}

@Injectable()
export class FraseProfesionalService {
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'fraseaprofesional';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<FraseProfesional[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as FraseProfesional[];
                return entities; })
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<FraseProfesional> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as FraseProfesional;
                return entity; })
            .catch(e => this.handleError(e));
    }

    save(entity: FraseProfesional): Promise<any> {
        if (entity.id) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: FraseProfesional): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }})
            .catch(e => this.handleError(e));
    }

    private put(entity: FraseProfesional): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }})
            .catch(e => this.handleError(e));
    }

    delete(entity: FraseProfesional): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }})
            .catch(
                error =>
                    this.handleError(error)
            );
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
