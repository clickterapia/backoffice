import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export enum TipoLogin {
  normal,
  social,
  temporal
}

export class Profesional {
  public id: string;
  public idsexo: number;
  public idpais: number;
  public pais: string;
  public idprovincia: number;
  public provincia: string;
  public usuario: string;
  public email: string;
  public contrasena: string;
  public apellido: string;
  public nombre: string;
  public dni: string;
  public fecnacimiento: string;
  public notificacion: number;
  public titulo: string;
  public universidad: string;
  public microbiografia: string;
  public credenciales: string;
  public precio: number;
  public fotourl: string;
  public dniurl: string;
  public titulourl: string;
  public matriculaurl: string;
  public fecaprobado: string;
  public fecmodificado: string;
  public fecregistro: string;
  public bloqueoadministrativo: number;
  public especialidades: string;
  public pacientes: number;
  public aprobado: string;
  public incompleto: string;
  public visualizaciones: number;
  public mp_authorization_code: string;
  public sesionesgratis: number;
  public idsocial: string;
  public issocial: number;
  public atendidos: number;

  constructor() {
    this.idsexo = -1;
    this.idpais = -1;
    this.idprovincia = -1;

    // datos para que en login no de error
    this.email = '';
    this.apellido = '';
    this.nombre = '';
    this.dni = '';
    this.fecnacimiento = '';
    this.notificacion = 0;

    // datos para que en login social no de error
    this.contrasena = '';

    // datos en blanco en registro inicial
    this.titulo = '';
    this.universidad = '';
    this.microbiografia = '';
    this.credenciales = '';
    this.precio = 0;
    this.fotourl = '';
    this.dniurl = '';
    this.titulourl = '';
    this.matriculaurl = '';
    this.fecaprobado = '';
    this.fecmodificado = '';
    this.fecregistro = '';
    this.bloqueoadministrativo = 0;
    this.especialidades = '';
    this.visualizaciones = 0;
    this.sesionesgratis = 0;
    this.issocial = 0;
    this.atendidos = 0;
    this.idsocial = '';
  }
}

export class Opinion {
  public id: string;
  public usuario: string;
  public observaciones: string;
  public fecfin: string;
}

export class Reporte {
  public total: string;
  public consesiones: string;
  public sinsesiones: string;
  public noaprobados: string;
  public pagas: string;
  public gratuitas: string;
}

export class Rendimiento {
  public cantidad: number;
  public precio: number;
}

export class Resumenes {
  public ausencias: number;
  public incompleto: number;
  public plazo: number;
}

@Injectable()
export class ProfesionalService {
  public static readonly ERROR_CLAVE_DUPLICADA = 205;
  public static readonly ERROR_NO_AUTORIZADO = 401;
  readonly GET_BYIDSOCIAL = 'byidsocial';
  readonly GET_HABILITADOS = 'habilitados';
  readonly GET_PENDIENTES = 'pendientes';
  readonly GET_OPINIONESBYID = 'opinionesbyid';
  readonly GET_REPORTE_PROFESIONALES = 'reporte_profesionales';
  readonly GET_REPORTE_PREMIUMS = 'reporte_premiums';
  readonly GET_REPORTE_BASICOS = 'reporte_basicos';
  readonly GET_RENDIMIENTO_GENERAL = 'rendimiento_general';
  readonly GET_RENDIMIENTO_PROFESIONAL = 'rendimiento_profesional';
  readonly GET_REPORTE_HORAS_OFRECIDAS = 'reporte_horas_ofrecidas';
  readonly GET_RESUMENES = 'resumenes';
  readonly PUT_LOGIN = 'login';
  readonly PUT_LOGINSOCIAL = 'loginsocial';
  readonly PUT_BLOQUEO = 'bloqueo';
  readonly PUT_APROBACION = 'aprobacion';
  readonly POST_FOTO = 'foto';
  readonly POST_DNI = 'dni';
  readonly POST_TITULO = 'titulo';
  readonly POST_MATRICULA = 'matricula';
  private apiUrl = '';
  private headers;
  constructor(private http: Http) {
    this.apiUrl = environment.apiUrl + 'profesional';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Profesional[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Profesional[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getHabilitados(): Promise<Profesional[]> {
    const customUrl = `${this.apiUrl}/${this.GET_HABILITADOS}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Profesional[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Profesional> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Profesional;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByIdSocial(idsocial: string): Promise<Profesional> {
    const customUrl = `${this.apiUrl}/${this.GET_BYIDSOCIAL}/${idsocial}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Profesional;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getPendientes(): Promise<Profesional[]> {
    const customUrl = `${this.apiUrl}/${this.GET_PENDIENTES}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Profesional[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getOpinionsById(id: string): Promise<Opinion[]> {
    const customUrl = `${this.apiUrl}/${this.GET_OPINIONESBYID}/${id}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Opinion[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getReporteProfesionales(): Promise<Reporte> {
    const customUrl = `${this.apiUrl}/${this.GET_REPORTE_PROFESIONALES}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Reporte;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getReportePremiums(desde: string, hasta: string): Promise<Reporte> {
    const customUrl = `${this.apiUrl}/${this.GET_REPORTE_PREMIUMS}/${desde}/${hasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Reporte;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getReporteBasicos(desde: string, hasta: string): Promise<Reporte> {
    const customUrl = `${this.apiUrl}/${this.GET_REPORTE_BASICOS}/${desde}/${hasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Reporte;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getReporteRendimientoGral(desde: string, hasta: string): Promise<Rendimiento[]> {
    const customUrl = `${this.apiUrl}/${this.GET_RENDIMIENTO_GENERAL}/${desde}/${hasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Rendimiento[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getReporteRendimientoProfesional(id: string, desde: string, hasta: string): Promise<Rendimiento[]> {
    const customUrl = `${this.apiUrl}/${this.GET_RENDIMIENTO_PROFESIONAL}/${id}/${desde}/${hasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Rendimiento[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getReporteHorasOfrecidas(id: string, desde: string, hasta: string): Promise<Reporte> {
    const customUrl = `${this.apiUrl}/${this.GET_REPORTE_HORAS_OFRECIDAS}/${id}/${desde}/${hasta}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Reporte;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getResumenes(id: string): Promise<Resumenes> {
    const customUrl = `${this.apiUrl}/${this.GET_RESUMENES}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Resumenes;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Profesional): Promise<any> {
    if (entity.issocial === 1) {
      return this.post(entity);
    }
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Profesional): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          const body = response.json();
          return body.message;
        } else if (response.status === ProfesionalService.ERROR_NO_AUTORIZADO) {
          return ProfesionalService.ERROR_NO_AUTORIZADO;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Profesional): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === ProfesionalService.ERROR_CLAVE_DUPLICADA) {
          return ProfesionalService.ERROR_CLAVE_DUPLICADA;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  public setBloqueo(entity: Profesional): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_BLOQUEO}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === ProfesionalService.ERROR_CLAVE_DUPLICADA) {
          return ProfesionalService.ERROR_CLAVE_DUPLICADA;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  public setAprobacion(entity: Profesional): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_APROBACION}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === ProfesionalService.ERROR_CLAVE_DUPLICADA) {
          return ProfesionalService.ERROR_CLAVE_DUPLICADA;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  login(entity: Profesional): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_LOGIN}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          const body = response.json();
          return body.message[0];
        } else {
          return false;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  loginSocial(entity: Profesional): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_LOGINSOCIAL}`;
    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          const body = response.json();
          return (response.status === 200);
        } else {
          return false;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Profesional): Promise<Response> {
    const customUrl = `${this.apiUrl}/${entity.id}`;

    return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 204) {
          return response.ok;
        } else if (response.status === 202) {
          return -1;
        }
      })
      .catch(
        error =>
          this.handleError(error)
      );
  }

  public uploadFoto(id: string, file: File): Promise<any> {
    return this.upload(id, file, this.POST_FOTO);
  }

  public uploadDni(id: string, file: File): Promise<any> {
    return this.upload(id, file, this.POST_DNI);
  }

  public uploadTitulo(id: string, file: File): Promise<any> {
    return this.upload(id, file, this.POST_TITULO);
  }

  public uploadMatricula(id: string, file: File): Promise<any> {
    return this.upload(id, file, this.POST_MATRICULA);
  }

  private upload(id: string, file: File, api: string): Promise<any> {
    const customUrl = `${this.apiUrl}/${api}/${id}`;

    const fileName = `${id}.png`;

    const headersLoc = new Headers();
    const options = new RequestOptions({ headers: headersLoc });
    const formData: FormData = new FormData();
    formData.append('file', file, fileName);
    return this.http
      .post(customUrl, formData, options)
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          return response.ok;
        } else if (response.status === 204) {
          return -1;
        }
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
