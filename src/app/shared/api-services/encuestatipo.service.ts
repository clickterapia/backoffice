import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class EncuestaTipo {
  public id: string;
  public encuestatipo: string;
  public calificacion: string;
  public observaciones: string;
  public booleano: string;
  constructor() {
      this.id = '';
      this.encuestatipo = 'Seleccionar';
  }
}

@Injectable()
export class EncuestaTipoService {
  private apiUrl = '';
  private headers;
  constructor(private http: Http) {
    this.apiUrl = environment.apiUrl + 'encuestatipo';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<EncuestaTipo[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as EncuestaTipo[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<EncuestaTipo> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as EncuestaTipo;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: EncuestaTipo): Promise<any> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: EncuestaTipo): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: EncuestaTipo): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: EncuestaTipo): Promise<Response> {
    const customUrl = `${this.apiUrl}/${entity.id}`;

    return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 204) {
          return response.ok;
        } else if (response.status === 202) {
          return -1;
        }
      })
      .catch(
        error =>
          this.handleError(error)
      );
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
