import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';
import { HttpParams, HttpHeaders } from '@angular/common/http';

export class PagosMP {
  public id: string;
  // public titulo: string;
  // public fecinicio: string;
  // public fecfin: string;
  // public imagenurl: string;

  constructor() {
    // this.imagenurl = '';
  }
}

@Injectable()
export class MercadoPagoService {
  readonly GET_LISTPAGOS = 'listpagos';

  private apiUrl = '';
  private headers;
  constructor(private http: Http) {
    this.apiUrl = environment.apiUrl + 'mercadopago';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<PagosMP[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PagosMP[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<PagosMP> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as PagosMP;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getListPagos(): Promise<PagosMP[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTPAGOS}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as PagosMP[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  public getAccessToken(profesionalCode: string): Promise<any> {
    const params = new HttpParams({
          fromObject: {
            client_id: environment.apiMercadoPago.appID,
            client_secret: environment.apiMercadoPago.secretKey,
            grant_type: 'authorization_code',
            code: profesionalCode,
            redirect_uri: 'https://clickterapia.com/mercadopago/marketplace.php',
          }
        });

    const requestOptions = new RequestOptions();
    requestOptions.headers = new Headers();
    requestOptions.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    requestOptions.headers.append('Cache-Control', 'no-cache');
    requestOptions.headers.append('Pragma', 'no-cache');

    return this.http
      .post('https://api.mercadolibre.com/oauth/token', params, requestOptions)
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  /*
  x-www-form
  */
  // save(entity: Banner): Promise<any> {
  //   if (entity.id) {
  //     return this.put(entity);
  //   }
  //   return this.post(entity);
  // }

  // private post(entity: Banner): Promise<any> {
  //   return this.http
  //     .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
  //     .toPromise()
  //     .then(response => {
  //       const body = response.json();
  //       if (body.status === 'success') {
  //         return body.message;
  //       } else if (body.status === 'duplicado') {
  //         return body.message;
  //       } else {
  //         return -1;
  //       }
  //     }
  //     )
  //     .catch(e => this.handleError(e));
  // }

  // private put(entity: Banner): Promise<any> {
  //   const url = `${this.apiUrl}/${entity.id}`;

  //   return this.http
  //     .put(url, JSON.stringify(entity))
  //     .toPromise()
  //     .then(response => {
  //       const body = response.json();
  //       if (body.status === 'success') {
  //         return body.message;
  //       } else if (body.status === 'duplicado') {
  //         return body.message;
  //       } else {
  //         return -1;
  //       }
  //     }
  //     )
  //     .catch(e => this.handleError(e));
  // }

  // delete(entity: Banner): Promise<Response> {
  //   const customUrl = `${this.apiUrl}/${entity.id}`;

  //   return this.http
  //     .delete(customUrl)
  //     .toPromise()
  //     .then(response => {
  //       if (response.status === 204) {
  //         return response.ok;
  //       } else if (response.status === 202) {
  //         return -1;
  //       }
  //     })
  //     .catch(
  //       error =>
  //         this.handleError(error)
  //     );
  // }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
