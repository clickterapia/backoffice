import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Mensaje {
    public id: string;
    public idorigen: string;
    public origen: string;
    public iddestino: string;
    public destino: string;
    public avatar: string;
    public tipoorigen: number;
    public tipodestino: number;
    public mensaje: string;
    public fechahora: string;
    public leido: number;

    constructor() {
        this.id = '';
    }
}

@Injectable()
export class MensajeService {
    readonly GET_BYIDPROFESIONALXIDPACIENTE = 'byidprofesionalidpaciente';
    readonly GET_BYIDPACIENTEXIDPROFESIONAL = 'byidpacienteidprofesional';
    readonly GET_BYADMINXIDPROFESIONAL = 'byadminidprofesional';
    readonly GET_LISTADMIN = 'listadmin';
    readonly GET_LISTPROFESIONAL = 'listprofesional';
    readonly GET_LISTPACIENTE = 'listpaciente';
    private apiUrl = '';
    private headers;
    constructor (private http: Http) {
        this.apiUrl = environment.apiUrl + 'mensaje';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Mensaje[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
        .get(customUrl, { headers: this.headers })
        .toPromise()
        .then(response => {
            const body = response.json();
            const entities = body || {} as Mensaje[];
            return entities; })
        .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Mensaje> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Mensaje;
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getListByIdProfesionalXIdPaciente(idprofesional: string, idpaciente: string): Promise<Mensaje[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDPROFESIONALXIDPACIENTE}/${idprofesional}/${idpaciente}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Mensaje[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getListByIdPacienteXIdProfesional(idpaciente: string, idprofesional: string): Promise<Mensaje[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDPACIENTEXIDPROFESIONAL}/${idpaciente}/${idprofesional}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Mensaje[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getListByIdAdminXProfesional(idprofesional: string): Promise<Mensaje[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYADMINXIDPROFESIONAL}/${idprofesional}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Mensaje[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getListAdmin(): Promise<Mensaje[]> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTADMIN}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Mensaje[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getListProfesional(idprofesional: string): Promise<Mensaje[]> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTPROFESIONAL}/${idprofesional}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Mensaje[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getListPaciente(idpaciente: string): Promise<Mensaje[]> {
        const customUrl = `${this.apiUrl}/${this.GET_LISTPACIENTE}/${idpaciente}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Mensaje[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: Mensaje): Promise<any> {
        if (entity.id && entity.id !== '') {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Mensaje): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }})
            .catch(e => this.handleError(e));
    }

    private put(entity: Mensaje): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }})
            .catch(e => this.handleError(e));
    }

    delete(entity: Mensaje): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response =>  {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }})
            .catch(
                error =>
                this.handleError(error));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
