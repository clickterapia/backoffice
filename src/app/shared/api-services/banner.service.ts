import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Banner {
  public id: string;
  public titulo: string;
  public fecinicio: string;
  public fecfin: string;
  public imagenurl: string;

  constructor() {
    this.imagenurl = '';
  }
}

@Injectable()
export class BannerService {
  readonly GET_LISTFORPACIENTES = 'listforpacientes';
  readonly POST_IMAGEN = 'imagen';

  private apiUrl = '';
  private headers;
  constructor(private http: Http) {
    this.apiUrl = environment.apiUrl + 'banner';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Banner[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Banner[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Banner> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Banner;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getListForPacientes() : Promise<Banner[]> {
    const customUrl = `${this.apiUrl}/${this.GET_LISTFORPACIENTES}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Banner[];
        return entities;
      }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Banner): Promise<any> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Banner): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Banner): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Banner): Promise<Response> {
    const customUrl = `${this.apiUrl}/${entity.id}`;

    return this.http
      .delete(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 204) {
          return response.ok;
        } else if (response.status === 202) {
          return -1;
        }
      })
      .catch(
        error =>
          this.handleError(error)
      );
  }

  public uploadImagen(id: string, file: File): Promise<any> {
    return this.upload(id, file, this.POST_IMAGEN);
  }

  private upload(id: string, file: File, api: string): Promise<any> {
    const customUrl = `${this.apiUrl}/${api}/${id}`;

    const fileName = `${id}.png`;

    const headersLoc = new Headers();
    const options = new RequestOptions({ headers: headersLoc });
    const formData: FormData = new FormData();
    formData.append('file', file, fileName);
    return this.http
      .post(customUrl, formData, options)
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          return response.ok;
        } else if (response.status === 204) {
          return -1;
        }
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
