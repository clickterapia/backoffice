import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Paciente {
  public id: string;
  public idsexo: number;
  public sexo: string;
  public idpais: number;
  public pais: string;
  public idprovincia: number;
  public provincia: string;
  public fecnacimiento: string;
  public edad: string;
  public usuario: string;
  public email: string;
  public contrasena: string;
  public notificacion: number;
  public historialclinico: string;
  public fecregistro: string;
  public bloqueoadministrativo: number;
  public primera: number;
  public ultima: number;
  public cantidad: number;
  public fotourl: string;
  public nombre: string;
  public issocial: number;
  public idsocial: number;

  constructor () {
    this.historialclinico = '';
    this.fecregistro = '';
    this.bloqueoadministrativo = 0;
    this.primera = 0;
    this.ultima = 0;
    this.cantidad = 0;
    this.notificacion = 1;
    this.fotourl = '';
    this.nombre = '';

    // datos para que en login social no de error
    this.contrasena = '';
    this.issocial = 0;
  }
}

export class ResumenesPac {
  public ausencias: number;
  public incompleto: number;
  public plazo: number;
}

@Injectable()
export class PacienteService {
  public static readonly ERROR_CLAVE_DUPLICADA = 205;
  public static readonly ERROR_NO_AUTORIZADO = 401;
  readonly GET_BYIDSOCIAL = 'byidsocial';
  readonly GET_BYIDPROFESIONAL = 'byidprofesional';
  readonly GET_BYIDEIDPROFESIONAL = 'byideidprofesional';
  readonly GET_CONSESIONES = 'consesiones';
  readonly GET_RESUMENES = 'resumenes';
  readonly PUT_HISTORIAL = 'historial';
  readonly PUT_LOGIN = 'login';
  readonly PUT_LOGINSOCIAL = 'loginsocial';
  readonly PUT_BLOQUEO = 'bloqueo';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'paciente';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Paciente[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Paciente[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: string): Promise<Paciente> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Paciente;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByIdSocial(idsocial: string): Promise<Paciente> {
    const customUrl = `${this.apiUrl}/${this.GET_BYIDSOCIAL}/${idsocial}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Paciente;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  getByIDProfesional(id: string): Promise<Paciente[]> {
    const customUrl = `${this.apiUrl}/${this.GET_BYIDPROFESIONAL}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Paciente[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByIdeIdProfesional(id: string, idprofesional: string): Promise<Paciente> {
    const customUrl = `${this.apiUrl}/${this.GET_BYIDEIDPROFESIONAL}/${id}/${idprofesional}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as Paciente;
        return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getConSesiones(): Promise<Paciente[]> {
    const customUrl = `${this.apiUrl}/${this.GET_CONSESIONES}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Paciente[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getResumenes(id: string): Promise<ResumenesPac> {
    const customUrl = `${this.apiUrl}/${this.GET_RESUMENES}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entity = body[0] as ResumenesPac;
        return entity;
      }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Paciente): Promise<any> {
    if (entity.issocial === 1) {
      return this.post(entity);
    }
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Paciente): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  login(entity: Paciente): Promise<any> {
      const url = `${this.apiUrl}/${this.PUT_LOGIN}`;

      return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          if (response.status === 200) {
            const body = response.json();
            return body.message[0];
          } else {
            return false;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  loginSocial(entity: Paciente): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_LOGINSOCIAL}`;
    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        if (response.status === 200) {
          const body = response.json();
          return (response.status === 200);
        } else {
          return false;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Paciente): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  public putHistorial(entity: Paciente): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  public setBloqueo(entity: Paciente): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_BLOQUEO}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        if (body.status === 'success') {
          return body.message;
        } else if (body.status === PacienteService.ERROR_CLAVE_DUPLICADA) {
          return PacienteService.ERROR_CLAVE_DUPLICADA;
        } else if (body.status === 'duplicado') {
          return body.message;
        } else {
          return -1;
        }
      }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Paciente): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              if (response.status === 204) {
                return response.ok;
              } else if (response.status === 202) {
                return -1;
              }
          })
          .catch(
            error =>
            this.handleError(error)
          );
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
