import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Problema {
    public id: string;
    public idsesion: string;
    public horasesion: string;
    public fecinicio: string;
    public idorigen: string;
    public tipoorigen: number;
    public idproblematipo: number;
    public problematipo: string;
    public idproblemaestado: number;
    public problemaestado: string;
    public problema: string;
    public acciones: string;
    public fecultmodif: string;
    public emisor: string;
    public paciente: string;
    public profesional: string;
}

@Injectable()
export class ProblemaService {
    readonly GET_BYIDSESION = 'byidsesion';
    readonly GET_NOTRATADOS = 'notratados';
    readonly GET_SANCIONESBYID = 'sancionesbyid';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'problema';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Problema[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Problema[];
                return entities;
            })
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Problema> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Problema;
                return entity;
            })
            .catch(e => this.handleError(e));
    }

    getByIdSesion(idsesion: string): Promise<Problema[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDSESION}/${idsesion}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Problema[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getNoTratados(): Promise<Problema[]> {
        const customUrl = `${this.apiUrl}/${this.GET_NOTRATADOS}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Problema[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    getSancionesByID(idprofesional: string) {
        const customUrl = `${this.apiUrl}/${this.GET_SANCIONESBYID}/${idprofesional}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body || {} as Problema[];
                return entity;
                }
            )
            .catch(e => this.handleError(e));
    }

    save(entity: Problema): Promise<any> {
        if (entity.id) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Problema): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    private put(entity: Problema): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            }
            )
            .catch(e => this.handleError(e));
    }

    delete(entity: Problema): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }
            })
            .catch(
                error =>
                    this.handleError(error)
            );
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
