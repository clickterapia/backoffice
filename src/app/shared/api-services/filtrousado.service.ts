import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class FiltroUsado {
    public sexo: number;
    public edad: number;
    public especialidad: number;
    public horario: number;
    public total: number;

    constructor() {
        this.sexo = 0;
        this.edad = 0;
        this.especialidad = 0;
        this.horario = 0;
        this.total = 0;
    }
}

@Injectable()
export class FiltroUsadoService {
    private apiUrl = '';
    private headers;
    constructor (private http: Http) {
        this.apiUrl = environment.apiUrl + 'filtrousado';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<FiltroUsado[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
        .get(customUrl, { headers: this.headers })
        .toPromise()
        .then(response => {
            const body = response.json();
            const entities = body || {} as FiltroUsado[];
            return entities;
        })
        .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<FiltroUsado> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
        .get(customUrl, { headers: this.headers })
        .toPromise()
        .then(response => {
            const body = response.json();
            const entity = body[0] as FiltroUsado;
            return entity;
        })
        .catch(e => this.handleError(e));
    }

    save(entity: FiltroUsado): Promise<any> {
        return this.put(entity);
    }

    private put(entity: FiltroUsado): Promise<any> {
        const url = `${this.apiUrl}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }
            })
            .catch(e => this.handleError(e));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
