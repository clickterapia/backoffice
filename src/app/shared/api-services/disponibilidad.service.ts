import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Disponibilidad {
  public id: number;
  public idprofesional: string;
  public inicio: string;
  public fin: string;
  public gratuito: number;
  public temporal: number;
  public reservado: number;
}

@Injectable()
export class DisponibilidadService {
  readonly GET_NEWSBYIDPROFESIONAL = 'newsbyidprofesional';
  readonly GET_DISPONIBILIDADACTUAL = 'disponibilidadactual';
  readonly PUT_VARIOS = 'varios';
  private apiUrl = '';
  private headers;
  constructor (private http: Http) {
    this.apiUrl = environment.apiUrl + 'disponibilidad';
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Cache-Control', 'no-cache');
    this.headers.append('Pragma', 'no-cache');
  }

  getList(): Promise<Disponibilidad[]> {
    const customUrl = `${this.apiUrl}`;

    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Disponibilidad[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getByID(id: number): Promise<Disponibilidad> {
    const customUrl = `${this.apiUrl}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          const entity = body[0] as Disponibilidad;
          return entity;
        }
      )
      .catch(e => this.handleError(e));
  }

  getNewsByIDProfesional(id: string): Promise<Disponibilidad[]> {
    const customUrl = `${this.apiUrl}/${this.GET_NEWSBYIDPROFESIONAL}/${id}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Disponibilidad[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  getDisponibilidadActual(): Promise<Disponibilidad[]> {
    const customUrl = `${this.apiUrl}/${this.GET_DISPONIBILIDADACTUAL}`;
    return this.http
      .get(customUrl, { headers: this.headers })
      .toPromise()
      .then(response => {
        const body = response.json();
        const entities = body || {} as Disponibilidad[];
        return entities;
        }
      )
      .catch(e => this.handleError(e));
  }

  save(entity: Disponibilidad): Promise<any> {
    if (entity.id) {
      return this.put(entity);
    }
    return this.post(entity);
  }

  private post(entity: Disponibilidad): Promise<any> {
    return this.http
      .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  private put(entity: Disponibilidad): Promise<any> {
    const url = `${this.apiUrl}/${entity.id}`;

    return this.http
      .put(url, JSON.stringify(entity), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  public saveSeveral(idprofesional: string, entities: Disponibilidad[]): Promise<any> {
    const url = `${this.apiUrl}/${this.PUT_VARIOS}/${idprofesional}`;
    return this.http
      .put(url, JSON.stringify(entities), { headers: this.headers })
      .toPromise()
      .then(response => {
          const body = response.json();
          if (body.status === 'success') {
            return body.message;
          } else if (body.status === 'duplicado') {
            return body.message;
          } else {
            return -1;
          }
        }
      )
      .catch(e => this.handleError(e));
  }

  delete(entity: Disponibilidad): Promise<Response> {
      const customUrl = `${this.apiUrl}/${entity.id}`;

      return this.http
          .delete(customUrl, { headers: this.headers })
          .toPromise()
          .then(response =>  {
              if (response.status === 204) {
                return response.ok;
              } else if (response.status === 202) {
                return -1;
              }
          })
          .catch(
            error =>
            this.handleError(error)
          );
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
