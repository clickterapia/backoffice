import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

export class Encuesta {
    public id: string;
    public idencuestatipo: string;
    public encuestatipo: string;
    public calificacion: number;
    public observaciones: number;
    public booleano: number;
    public encuesta: string;
    public apacientes: number;
    public aprofesionales: number;
    public booleanoverdadero: string;
    public booleanofalso: string;
    public fecinicio: string;
    public fecfin: string;
    constructor() {
        this.encuesta = '';
        this.calificacion = 0;
        this.observaciones = 0;
        this.booleano = 0;
        this.booleanoverdadero = '';
        this.booleanofalso = '';
    }
}

@Injectable()
export class EncuestaService {
    readonly GET_BYIDPROFESIONAL = 'byidprofesional';
    readonly GET_BYIDPACIENTE = 'byidpaciente';
    private apiUrl = '';
    private headers;
    constructor(private http: Http) {
        this.apiUrl = environment.apiUrl + 'encuesta';
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Cache-Control', 'no-cache');
        this.headers.append('Pragma', 'no-cache');
    }

    getList(): Promise<Encuesta[]> {
        const customUrl = `${this.apiUrl}`;

        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Encuesta[];
                return entities; })
            .catch(e => this.handleError(e));
    }

    getByID(id: number): Promise<Encuesta> {
        const customUrl = `${this.apiUrl}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entity = body[0] as Encuesta;
                return entity; })
            .catch(e => this.handleError(e));
    }

    getByIdProfesional(id: string): Promise<Encuesta[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDPROFESIONAL}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Encuesta[];
                return entities; })
            .catch(e => this.handleError(e));
    }

    getByIdPacientes(id: string): Promise<Encuesta[]> {
        const customUrl = `${this.apiUrl}/${this.GET_BYIDPACIENTE}/${id}`;
        return this.http
            .get(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                const entities = body || {} as Encuesta[];
                return entities; })
            .catch(e => this.handleError(e));
    }

    save(entity: Encuesta): Promise<any> {
        if (entity.id) {
            return this.put(entity);
        }
        return this.post(entity);
    }

    private post(entity: Encuesta): Promise<any> {
        return this.http
            .post(this.apiUrl, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }})
            .catch(e => this.handleError(e));
    }

    private put(entity: Encuesta): Promise<any> {
        const url = `${this.apiUrl}/${entity.id}`;

        return this.http
            .put(url, JSON.stringify(entity), { headers: this.headers })
            .toPromise()
            .then(response => {
                const body = response.json();
                if (body.status === 'success') {
                    return body.message;
                } else if (body.status === 'duplicado') {
                    return body.message;
                } else {
                    return -1;
                }})
            .catch(e => this.handleError(e));
    }

    delete(entity: Encuesta): Promise<Response> {
        const customUrl = `${this.apiUrl}/${entity.id}`;

        return this.http
            .delete(customUrl, { headers: this.headers })
            .toPromise()
            .then(response => {
                if (response.status === 204) {
                    return response.ok;
                } else if (response.status === 202) {
                    return -1;
                }})
            .catch(
                error =>
                    this.handleError(error)
            );
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
