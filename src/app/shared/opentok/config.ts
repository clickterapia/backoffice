export default {
    // Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
    // Do not include the trailing slash. See the README for more information:
    SERVER_BASE_URL: 'https://clickterapia-api.herokuapp.com',
    // OR, if you have not set up a web server that runs the learning-opentok-php code,
    // set these values to OpenTok API key, a valid session ID, and a token for the session.
    // For test purposes, you can obtain these from https://tokbox.com/account.
    API_KEY: '46244452',
    SESSION_ID: '',
    TOKEN: '',
    SESION_URL: 'https://tokbox.com/embed/embed/ot-embed.js?embedId=1e1c2797-966f-4225-9c7d-99b00f574633&room=DEFAULT_ROOM&iframe=true'
    // 'https://tokbox.com/embed/embed/ot-embed.js?embedId=1e1c2797-966f-4225-9c7d-99b00f574633&room=DEFAULT_ROOM'
    // 'https://tokbox.com/embed/embed/ot-embed.js?embedId=74396038-48fc-4f98-9eb7-b63fa11b0772&room=DEFAULT_ROOM&iframe=true'
};
