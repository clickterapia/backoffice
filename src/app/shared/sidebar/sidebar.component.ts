import { Component, AfterViewInit, OnInit } from '@angular/core';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ROUTES } from './menu-items';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';

import { Profile } from '../guard/auth.guard';
import { Profesional, ProfesionalService, Paciente, PacienteService, Usuario, UsuarioService } from '../';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [
      ProfesionalService,
      PacienteService,
      UsuarioService]
})

export class SidebarComponent implements OnInit {
    showMenu = '';
    showSubMenu = '';
    public sidebarnavItems: any[];

    selectedID = '';
    selectedProfile: Profile;
    selectedEntity: Profesional | Paciente | Usuario;
    nombrePerfil: string;
    locacionPerfil: string;

    // this is for the open close
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    addActiveClass(element: any) {
        if (element === this.showSubMenu) {
            this.showSubMenu = '0';
        } else {
            this.showSubMenu = element;
        }
    }

    constructor(private modalService: NgbModal, private router: Router,
        private route: ActivatedRoute,
        private profesionalService: ProfesionalService,
        private pacienteService: PacienteService,
        private usuarioService: UsuarioService) {
            switch (localStorage.getItem('profile')) {
                case Profile.professional:
                    this.selectedEntity = new Profesional();
                    break;
                case Profile.patient:
                    this.selectedEntity = new Paciente();
                    break;
                case Profile.admin:
                    this.selectedEntity = new Usuario();
                    break;
            }
    }

    // End open close
    ngOnInit() {

        this.sidebarnavItems = ROUTES.filter(sidebarnavItem =>
            sidebarnavItem.profile === localStorage.getItem('profile'));
        $(function () {
            $('.sidebartoggler').on('click', function() {
                if ($('#main-wrapper').hasClass('mini-sidebar')) {
                    $('body').trigger('resize');
                    $('#main-wrapper').removeClass('mini-sidebar');
                } else {
                    $('body').trigger('resize');
                    $('#main-wrapper').addClass('mini-sidebar');
                }
            });
        });
        this.selectedID = localStorage.getItem('user');
        this.onGetEntityData();
    }

    public onGetEntityData(): any {
        if (this.selectedID !== '') {
            switch (localStorage.getItem('profile')) {
                case Profile.professional:
                    this.getProfesional();
                    break;
                case Profile.patient:
                    this.getPaciente();
                    break;
                case Profile.admin:
                    this.getAdmin();
                    break;
            }
        }
    }

    getProfesional() {
        this.profesionalService.getByID(this.selectedID).then(
            response => {
                this.selectedEntity = response as Profesional;
                this.nombrePerfil = this.selectedEntity.nombre + ' ' + this.selectedEntity.apellido;
                this.locacionPerfil = this.selectedEntity.provincia + ', ' + this.selectedEntity.pais;
            }
        ).catch(e => this.handleError(e));
    }

    getPaciente() {
        this.pacienteService.getByID(this.selectedID).then(
            response => {
                this.selectedEntity = response as Paciente;
                this.nombrePerfil = this.selectedEntity.usuario;
                this.locacionPerfil = '';
            }
        ).catch(e => this.handleError(e));
    }

    getAdmin() {
        this.usuarioService.getByID(+this.selectedID).then(
            response => {
                this.selectedEntity = response as Usuario;
                this.nombrePerfil = this.selectedEntity.usuario;
                this.selectedEntity.fotourl = '';
                this.locacionPerfil = '';
            }
        ).catch(e => this.handleError(e));
    }

    public onLogout() {
      switch (localStorage.getItem('profile')) {
        case Profile.admin:
        this.router.navigate(['/authentication/login-adm']);
        break;
        case Profile.professional:
        this.router.navigate(['/authentication/login-pro']);
        break;
        case Profile.patient:
        this.router.navigate(['/authentication/login-pac']);
        break;
        default:
        this.router.navigate(['/authentication/login-adm']);
        break;
      }
      localStorage.clear();
    }

    public hasImage() {
        return (this.selectedEntity.fotourl) ? (this.selectedEntity.fotourl.length > 0) : false;
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
