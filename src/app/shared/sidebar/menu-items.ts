import { RouteInfo } from './sidebar.metadata';
import { Profile } from '../../shared/guard/auth.guard';

export const ROUTES: RouteInfo[] = [
    // Administradores
    {
        path: '', title: 'General', icon: '', profile: '',
        class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    },
    {
        path: '/dashboard/dashboard-adm', title: 'Panel de Control', icon: 'mdi mdi-gauge',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admreportes', title: 'Reportes', icon: 'mdi mdi-poll',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admbitacorasesiones', title: 'Bitácora de Sesiones', icon: 'mdi mdi-clipboard-account',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admregistrosproblemas', title: 'Reportes de Problemas', icon: 'mdi mdi-calendar-text',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admprofesionales', title: 'Profesionales', icon: 'mdi mdi-clipboard-account',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admpacientes', title: 'Pacientes', icon: 'mdi mdi-ambulance',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admbanners', title: 'Banners', icon: 'mdi mdi-bulletin-board',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '', title: 'Encuestas', icon: 'mdi mdi-lead-pencil',
        class: 'has-arrow', label: '',  profile: Profile.admin,
        labelClass: '', extralink: false,
        submenu: [
            { path: '/admencuestasresultados', title: 'Resultados', icon: '',
                class: '', label: '',  profile: Profile.admin, labelClass: '', extralink: false, submenu: [] },
            { path: '/admencuestas', title: 'ABM Encuestas', icon: '',
                class: '', label: '',  profile: Profile.admin, labelClass: '', extralink: false, submenu: [] },
        ]
    },
    {
        path: '/admfrases', title: 'Frases', icon: 'mdi mdi-comment-text-outline',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/tables/smarttable', title: 'Premium', icon: 'mdi mdi-diamond',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/admrecursos', title: 'Recursos', icon: 'mdi mdi-file-video',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/forms/formvalidation', title: 'Usuarios', icon: 'mdi mdi-contacts',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/forms/formvalidation', title: 'Sistema', icon: 'mdi mdi-desktop-mac',
        class: '', label: '', profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/authentication/login-adm', title: 'Terminar Sesión', icon: 'mdi mdi-logout',
        class: '', label: '',  profile: Profile.admin,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    // Pacientes
    {
        path: '/dashboard/dashboard-pac', title: 'Panel de Control', icon: 'mdi mdi-gauge',
        class: '', label: '',  profile: Profile.patient,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/pacperfil', title: 'Mi Perfil', icon: 'mdi mdi-account-settings',
        class: '', label: '',  profile: Profile.patient,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/pacsesion', title: 'Sesión Terapéutica', icon: 'mdi mdi-headset',
        class: '', label: '',  profile: Profile.patient,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/pacprofesionales', title: 'Profesionales', icon: 'mdi mdi-clipboard-account',
        class: '', label: '',  profile: Profile.patient,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/pacsesiones', title: 'Sesiones', icon: 'mdi mdi-calendar-text',
        class: '', label: '',  profile: Profile.patient,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/authentication/login-pac', title: 'Terminar Sesión', icon: 'mdi mdi-logout',
        class: '', label: '',  profile: Profile.patient,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    // Profesionales
    {
        path: '/dashboard/dashboard-pro', title: 'Panel de Control', icon: 'mdi mdi-gauge',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/perfilprofesional', title: 'Mi Perfil', icon: 'mdi mdi-account-settings',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/prosesion', title: 'Sesión Terapéutica', icon: 'mdi mdi-headset',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/prodisponibilidad', title: 'Mi Agenda', icon: 'mdi mdi-calendar-text',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/procapacitacion', title: 'Capacitación', icon: 'mdi mdi-airplay',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/proestadocuenta', title: 'Estado de Cuenta', icon: 'mdi mdi-currency-usd',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/propacientes', title: 'Mis Pacientes', icon: 'mdi mdi-account-multiple',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/proreservas', title: 'Reservas', icon: 'mdi mdi-calendar-multiple',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    {
        path: '/authentication/login-pro', title: 'Terminar Sesión', icon: 'mdi mdi-logout',
        class: '', label: '',  profile: Profile.professional,
        labelClass: 'label label-rouded label-themecolor pull-right line-separator',
        extralink: false,
        submenu: []
    },
    // Administrativos
    // {
    //     path: '', title: 'General', icon: '',
    //     class: 'nav-small-cap', label: '',  profile: Profile.professional,
    //     labelClass: '', extralink: true, submenu: []
    // },
    // {
    //     path: '', title: 'Panel de Control', icon: 'mdi mdi-gauge',
    //     class: '', label: '',  profile: Profile.professional,
    //     labelClass: 'label label-rouded label-themecolor pull-right',
    //     extralink: false,
    //     submenu: []
    // },
    // {
    //     path: '', title: 'Problemas', icon: 'mdi mdi-apps',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/apps/email', title: 'Mailbox', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/apps/fullcalendar', title: 'Calendar', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/apps/taskboard', title: 'Taskboard', icon: '', class: '',
    //             label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'UI Components', icon: '',
    //     class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    // },
    // {
    //     path: '', title: 'Component', icon: 'mdi mdi-bullseye',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/component/accordion', title: 'Accordion', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/alert', title: 'Alert', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/carousel', title: 'Carousel', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/dropdown', title: 'Dropdown', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/modal', title: 'Modal', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/pagination', title: 'Pagination', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/poptool', title: 'Popover & Tooltip',
    //             icon: '', class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/progressbar', title: 'Progressbar', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/rating', title: 'Ratings', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/tabs', title: 'Tabs', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/timepicker', title: 'Timepicker', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/buttons', title: 'Button', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/cards', title: 'Card', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Extra Components', icon: 'mdi mdi-dropbox',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/extra-component/toastr', title: 'Toastr', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/extra-component/upload', title: 'File Upload', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/extra-component/editor', title: 'Editor', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/extra-component/dragndrop', title: 'Drag n Drop', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Forms & Tables', icon: '',
    //         class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    // },
    // {
    //     path: '', title: 'Forms', icon: 'mdi mdi-file',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/forms/basicform', title: 'Basic Forms', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/forms/formvalidation', title: 'Form Validation', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/typehead', title: 'Form Typehead', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/component/datepicker', title: 'Datepicker', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Tables', icon: 'mdi mdi-table',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/tables/basictable', title: 'Basic Tables', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/tables/smarttable', title: 'Smart Tables', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/tables/datatable', title: 'Data Tables', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '/widgets', title: 'Widgets', icon: 'mdi mdi-widgets',
    //     class: '', label: '', labelClass: '', extralink: false, submenu: []
    // },
    // {
    //     path: '', title: 'Charts & Icons', icon: '',
    //     class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    // },
    // {
    //     path: '', title: 'Charts', icon: 'mdi mdi-chart-arc',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/charts/chartjs', title: 'Chart Js', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/charts/chartistjs', title: 'Chartist Js', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Icons', icon: 'mdi mdi-brush',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/icons/fontawesome', title: 'Fontawesome', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/icons/simpleline', title: 'Simple Line Icons', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/icons/material', title: 'Material Icons', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Pages', icon: '',
    //     class: 'nav-small-cap', label: '', labelClass: '', extralink: true, submenu: []
    // },
    // {
    //     path: '', title: 'Authentication', icon: 'mdi mdi-lock',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/authentication/login', title: 'Login', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/authentication/login2', title: 'Login 2', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/authentication/signup', title: 'Register', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/authentication/signup2', title: 'Register 2', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/authentication/404', title: '404', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/authentication/lock', title: 'Lockscreen', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Sample Pages', icon: 'mdi mdi-file',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: '/sample-pages/timeline', title: 'Timeline', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/sample-pages/profile', title: 'Profile', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/sample-pages/pricing', title: 'Pricing', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/sample-pages/invoice', title: 'Invoice', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/sample-pages/helperclasses', title: 'Helper Classes', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //         { path: '/starter', title: 'Starter Page', icon: '',
    //             class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //     ]
    // },
    // {
    //     path: '', title: 'Menu Levels', icon: 'mdi mdi-arrange-send-backward',
    //     class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //     submenu: [
    //         { path: 'javascript:void(0);', title: 'Second Level', icon: '',
    //             class: '', label: '', labelClass: '', extralink: true, submenu: [] },
    //         {
    //             path: '', title: 'Second Child', icon: '',
    //             class: 'has-arrow', label: '', labelClass: '', extralink: false,
    //             submenu: [
    //                 { path: 'javascript:void(0);', title: 'Third 1.1', icon: '',
    //                     class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //                 { path: 'javascript:void(0);', title: 'Third 1.2', icon: '',
    //                     class: '', label: '', labelClass: '', extralink: false, submenu: [] },
    //             ]
    //         },
    //     ]
    // }
];
