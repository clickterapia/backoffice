import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExtPerfilProfesionalComponent } from './extperfilprofesional.component';

const routes: Routes = [
    {
        path: '',
        component: ExtPerfilProfesionalComponent,
        data: {
            title: 'Profesionales',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExtPerfilProfesionalRoutingModule { }
