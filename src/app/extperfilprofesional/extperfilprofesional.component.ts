import { Component, OnInit,
    TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR, Profesional } from '../shared';
import {
    subMonths, subDays, subWeeks,
    addMonths, addDays, addWeeks, addMinutes,
    compareAsc,
    endOfMonth,
    endOfWeek,
    endOfDay, isSameDay } from 'date-fns';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarDateFormatter,
    DAYS_OF_WEEK,
    CalendarMonthViewDay } from 'angular-calendar';
import {
    ProfesionalService,
    EspecialidadService,
    DisponibilidadService } from '../shared';

type CalendarPeriod = 'day' | 'week' | 'month';

function addPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
    return {
        day: addDays,
        week: addWeeks,
        month: addMonths
    }[period](date, amount);
}

function endOfPeriod(period: CalendarPeriod, date: Date): Date {
    return {
        day: endOfDay,
        week: endOfWeek,
        month: endOfMonth
    }[period](date);
}

function subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
    return {
        day: subDays,
        week: subWeeks,
        month: subMonths
    }[period](date, amount);
}

@Component({
    selector: 'app-extperfilprofesional',
    templateUrl: './extperfilprofesional.component.html',
    styleUrls: ['./extperfilprofesional.component.scss'],
    providers: [
        DatePipe,
        CurrencyPipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        ProfesionalService,
        EspecialidadService,
        DisponibilidadService ]
})

export class ExtPerfilProfesionalComponent implements OnInit {
    public modalTitle = 'Profesionales';
    view: CalendarPeriod = 'month';
    locale = 'es';

    public dataEspecialidades = [{'id': -1, 'itemName': 'seleccionar'}];
    prevBtnDisabled = false;
    selectedIdDisponibilidad;

    precio: string;

    viewDate: Date = new Date();
    minDate: Date = new Date();

    selectedID: string;
    selectedEntity: Profesional;
    dataProfesionales: Array<any> = Array<any>();
    dataProfesionalesFilt: Array<any> = Array<any>();
    dataEspecialidadesProf: Array<any> = Array<any>();
    dataDisponibilidades: Array<any> = Array<any>();
    dataDisponibilidadesDia: Array<any> = Array<any>();

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private cp: CurrencyAR,
        public modal: Modal,
        private profesionalService: ProfesionalService,
        private especialidadService: EspecialidadService,
        private disponibilidadService: DisponibilidadService) {
            this.selectedEntity = new Profesional();
            this.selectedIdDisponibilidad = '';
            this.precio = '';
    }

    public ngOnInit(): void {
         this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.onGetEntityData();
        });
    }

    public onGetEntityData() {
        if (this.selectedID !== '') {
            this.profesionalService.getByID(this.selectedID).then(
                response => {
                    this.selectedEntity = response as Profesional;
                }
            ).catch(e => this.handleError(e));
        }

        const p0 =  this.profesionalService.getByID(this.selectedID);
        const p1 = this.especialidadService.getList();
        const p2 = this.disponibilidadService.getNewsByIDProfesional(this.selectedID);
        Promise.all([p0, p1, p2]).then( ([
            profesional,
            especialidades,
            disponibilidades]) => {
            this.selectedEntity = profesional as Profesional;
            this.precio = this.cp.transform(this.selectedEntity.precio);
            this.dataEspecialidadesProf = especialidades;
            this.dataEspecialidades = especialidades.map(o => {
                return { 'id': o.id, 'itemName': o.especialidad };
            });
            this.dataDisponibilidades = disponibilidades;
            const especialidad = JSON.parse(this.selectedEntity.especialidades);
            let descripcion = '';
            especialidad.forEach(esp => {
                descripcion += esp.especialidad + ', ';
            });
            descripcion = (descripcion.length > 0) ? descripcion.substring(0, descripcion.length - 2) : '';
            this.selectedEntity.especialidades = `${descripcion}`;
        });
    }

    increment(): void {
      this.changeDate(addPeriod(this.view, this.viewDate, 1));
    }

    decrement(): void {
      this.changeDate(subPeriod(this.view, this.viewDate, 1));
    }

    changeDate(date: Date): void {
        this.viewDate = date;
        this.dateOrViewChanged();
    }

    dateOrViewChanged(): void {
        this.prevBtnDisabled = !this.dateIsValid(
            endOfPeriod(this.view, subPeriod(this.view, this.viewDate, 1))
        );
        if (this.viewDate < this.minDate) {
            this.changeDate(this.minDate);
        }
    }

    dateIsValid(date: Date): boolean {
        return date >= this.minDate;
    }

    dayClicked(event): void {
        const day = new Date(event.day.date);
        this.dataDisponibilidadesDia = this.dataDisponibilidades.filter(
            disp => isSameDay(disp.inicio, day)
        );

        if (this.dataDisponibilidades.length > 0) {
            this.selectedIdDisponibilidad = (this.dataDisponibilidades[0]).id;
        }
    }

    getDispoSelect(id) {
        return (this.selectedIdDisponibilidad === id);
    }

    setDispoSelect(id) {
        this.selectedIdDisponibilidad = id;
    }

    onAgendar() {
        localStorage.setItem('agendar', this.selectedID);
        localStorage.setItem('agendardisp', this.selectedIdDisponibilidad);
        this.router.navigate(['/authentication/login-pac']);
    }

    onVolver() {
        this.router.navigate(['/extbuscarprofesional']);
    }

    showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
              .size('sm')
              .showClose(false)
              .isBlocking(true)
              .title(this.modalTitle)
              .body(msg)
              .okBtn('Aceptar')
              .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
