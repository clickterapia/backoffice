import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule, DateAdapter } from 'angular-calendar';
// import { CalendarHeaderModule } from './calendarheader/calendarheader.module';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

import { NavbarWebModule } from '../shared/navbar-web/navbar-web.module';
import { TopBarModule } from '../shared/topbar/topbar.module';
import { FooterWebModule } from '../shared/footer-web/footer-web.module';

// relative import
import { ExtPerfilProfesionalRoutingModule } from './extperfilprofesional-routing.module';
import { ExtPerfilProfesionalComponent } from './extperfilprofesional.component';

@NgModule({
    imports: [
        CommonModule,
        ExtPerfilProfesionalRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        }),
        // CalendarHeaderModule,
        NavbarWebModule,
        TopBarModule,
        FooterWebModule
    ],
    declarations: [
        ExtPerfilProfesionalComponent
    ]
})
export class ExtPerfilProfesionalModule { }
