import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';

import { ProSesionDetalleComponent } from './prosesiondetalle.component';
import { ProSesionDetalleRoutingModule } from './prosesiondetalle-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    ProSesionDetalleRoutingModule
  ],
  declarations: [
    ProSesionDetalleComponent],
  exports: [ProSesionDetalleComponent]
})
export class ProSesionDetalleModule {}
