import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProSesionDetalleComponent } from './prosesiondetalle.component';

const routes: Routes = [
    {
        path: '',
        component: ProSesionDetalleComponent,
        data: {
            title: 'Detalle de Sesión Terapéutica',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Detalle de Sesión Terapéutica'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProSesionDetalleRoutingModule { }
