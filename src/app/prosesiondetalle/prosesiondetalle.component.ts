import { Component, OnInit,
    TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ProblemaService, SesionService, Sesion, TWUtils } from '../shared';
import { CurrencyAR } from '../shared';

@Component({
    selector: 'app-prosesiondetalle',
    styleUrls: ['./prosesiondetalle.component.scss'],
    templateUrl: './prosesiondetalle.component.html',
    providers: [
        DatePipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        SesionService,
        ProblemaService,
        CurrencyAR
    ]
})

export class ProSesionDetalleComponent implements OnInit {
    modalTitle = 'Detalle de Sesión Terapéutica';
    modalContent: TemplateRef<any>;

    settings = {
        noDataMessage: 'Sin problemas registrados',
        pager: { display: true, perPage: 10 },
        columns: {
            problematipo: { title: 'Tipo', filter: false, },
            problema: { title: 'Problema', filter: false, },
            problemaestado: { title: 'Estado', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;

    dataProblemas: Array<any> = Array<any>();
    selectedID = '';
    selectedEntity: Sesion;
    fecInicio: string;
    fecFin: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private dp: DatePipe,
        private sesionService: SesionService,
        private problemaService: ProblemaService) {
            this.selectedEntity = new Sesion();
            this.fecInicio = '';
            this.fecFin = '';
    }

    public ngOnInit(): void {
    //   this.selectedID = localStorage.getItem('user');
        this.route.params.subscribe(params => {
            this.selectedID = params['id'];
            this.getData();
            // this.onGetEntityData();
        });
    }

    getData(): any {
        const p0 = this.sesionService.getByID(this.selectedID);
        Promise.all([
            p0
        ]).then( ([sesion]) => {
            this.selectedEntity = sesion;
            this.fecInicio = !TWUtils.isNullDate(this.selectedEntity.fecinicio) 
                ? this.dp.transform(this.selectedEntity.fecinicio, 'dd/MM/yyyy HH:mm') : '';
            this.fecFin = !TWUtils.isNullDate(this.selectedEntity.fecfin) 
                ? this.dp.transform(this.selectedEntity.fecfin, 'dd/MM/yyyy HH:mm') : '';
            this.getProblemas();
        }).catch(e => this.handleError(e));
    }

    getProblemas(): any {
        if (this.selectedEntity.id.length > 0) {
            this.problemaService.getByIdSesion(this.selectedEntity.id).then(
                response => {
                    this.dataProblemas = response;
                    this.source = new LocalDataSource(this.dataProblemas);
                }
            ).catch(e => this.handleError(e));
        }
    }

    // onGetEntityData(): any {
    //     if (this.selectedID !== '') {
    //         this.sesionService.getByID(this.selectedID).then(
    //             response => {
    //                 this.selectedEntity = response as Sesion;
    //             }
    //         ).catch(e => this.handleError(e));
    //     }
    // }

    onRequest() {
        this.router.navigate(['/pacsolicitudsesion', this.selectedEntity.idprofesional]);
    }

    showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
              .size('sm')
              .showClose(false)
              .isBlocking(true)
              .title(this.modalTitle)
              .body(msg)
              .okBtn('Aceptar')
              .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
