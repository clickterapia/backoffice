import { Component, OnInit,
    TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import { CurrencyAR, FiltroUsado } from '../shared';

import { ProfesionalService,
    EspecialidadService,
    DisponibilidadService,
    FiltroUsadoService
    } from '../shared';

@Component({
    selector: 'app-pacprofesionales',
    templateUrl: './pacprofesionales.component.html',
    styleUrls: ['./pacprofesionales.component.scss'],
    providers: [
        DatePipe,
        CurrencyPipe,
        {provide: LOCALE_ID, useValue: 'es-AR'},
        CurrencyAR,
        ProfesionalService,
        EspecialidadService,
        DisponibilidadService,
        FiltroUsadoService ]
})

export class PacProfesionalesComponent implements OnInit {
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Profesionales';

    public dataEspecialidades = [{'id': -1, 'itemName': 'seleccionar'}];
    selectedItems = [];
    dropdownSettings = {};

    dataProfesionales: Array<any> = Array<any>();
    dataProfesionalesFilt: Array<any> = Array<any>();
    dataEspecialidadesProf: Array<any> = Array<any>();
    dataDisponibilidades: Array<any> = Array<any>();
    recentcomments: Object[];

    varon: boolean;
    mujer: boolean;
    rangoEdadValue: number;
    rangoEdad: string;
    rangoPrecio: number;
    rangoPrecio1Value: number;
    rangoPrecio2Value: number;
    rangoPrecio3Value: number;
    rangoPrecioStr: string;
    rangoPrecio1: string;
    rangoPrecio2: string;
    rangoPrecio3: string;
    franjaHorariaStr: string;
    franjaHoraria: number;
    nombre: string;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        private cp: CurrencyAR,
        public modal: Modal,
        private profesionalService: ProfesionalService,
        private especialidadService: EspecialidadService,
        private disponibilidadService: DisponibilidadService,
        private filtroUsadoService: FiltroUsadoService) {
            this.varon = true;
            this.mujer = true;
            this.rangoEdadValue = 0;
            this.rangoEdad = 'Cualquiera';
            this.rangoPrecioStr = 'Cualquiera';
            this.rangoPrecio1 = '';
            this.rangoPrecio2 = '';
            this.rangoPrecio3 = '';
            this.dropdownSettings = {
                singleSelection:  false,
                text: 'Especialidades',
                selectAllText: 'Seleccionar Todas',
                unSelectAllText: 'Desdeleccionar Todas',
                enableSearchFilter:  false,
            };
            this.franjaHorariaStr = 'Cualquiera';
    }

    public ngOnInit(): void {
        this.getData();
    }

    public getData() {
        // TODO: agrgar nueva consulta para obtener los profesionales disponibles para
        // atención (los que han pagado y han establecido fechas de disponibilidad).
        const p0 = this.profesionalService.getHabilitados();
        const p1 = this.especialidadService.getList();
        const p2 = this.disponibilidadService.getDisponibilidadActual();
        Promise.all([p0, p1, p2]).then( ([
            profesionales,
            especialidades,
            disponibilidades]) => {
            this.dataProfesionales = profesionales;
            this.dataProfesionalesFilt = this.dataProfesionales;
            this.dataEspecialidadesProf = especialidades;
            this.dataEspecialidades = especialidades.map(o => {
                return { 'id': o.id, 'itemName': o.especialidad };
            });
            this.selectedItems = especialidades.map(o => {
                return { 'id': o.id, 'itemName': o.especialidad };
            });
            this.dataDisponibilidades = disponibilidades;
            this.processProfesionales(true);
        });
    }

    processProfesionales(init: boolean): any {
        this.recentcomments = [];
        let lowestPrice = Number.MAX_VALUE;
        let highestPrice = 0;
        this.dataProfesionalesFilt.forEach(profesional => {
            lowestPrice = (lowestPrice > profesional.precio) ? (profesional.precio) : lowestPrice;
            highestPrice = (highestPrice < profesional.precio) ? (profesional.precio) : highestPrice;
            this.recentcomments.unshift({
                id: `${profesional.id}`,
                foto: profesional.fotourl,
                nombre: `${profesional.apellido}, ${profesional.nombre}`,
                titulo: `${profesional.titulo}`,
                especialidades: `${this.getEspecialidades(profesional.especialidades)}`,
                atendidos: `${profesional.atendidos}`,
                microbio: profesional.microbiografia,
                edad: `${this.calculateAge(profesional.fecnacimiento)} años`,
                precio: `Precio por sesión ${this.cp.transform(profesional.precio)}`,
                labelcolor: 'label-light-info'
            });
        });

        if (init) {
            const media = (+highestPrice - +lowestPrice) / 3;
            this.rangoPrecio = 0;
            this.rangoPrecio1Value = Math.floor(+lowestPrice + media);
            this.rangoPrecio1 = 'Hasta ' + this.cp.transform(this.rangoPrecio1Value);
            this.rangoPrecio2Value = Math.floor(+highestPrice) - 1;
            this.rangoPrecio2 = this.cp.transform(this.rangoPrecio1Value + 1) + ' a ' + this.cp.transform(this.rangoPrecio2Value);
            this.rangoPrecio3Value = Math.floor(+highestPrice);
            this.rangoPrecio3 = this.cp.transform(this.rangoPrecio3Value) + '+';
            this.franjaHoraria = 0;
        }
    }

    getEspecialidades(idespecialidades: string) {
        const ids = idespecialidades.replace(' ', '').split(';');
        let descripciones = '';
        this.dataEspecialidadesProf.forEach(esp => {
            if (ids.findIndex(espid => this.findEsp(espid, esp.id)) > -1) {
                descripciones += esp.especialidad + ', ';
            }
        });
        return descripciones;
    }

    isVaron() {
        return this.varon;
    }

    findEsp(id, idd) {
        return id === idd;
    }

    /** Eventos vinculados a controles UI*/
    updateEdad(value) {
        this.rangoEdadValue = +value;
        switch (+value) {
            case 0:
                this.rangoEdad = 'Cualquiera';
                break;
            case 1:
                this.rangoEdad = '20-35';
                break;
            case 2:
                this.rangoEdad = '36-50';
                break;
            case 3:
                this.rangoEdad = '50+';
                break;
        }
    }

    onRangoPrecios(value) {
        this.rangoPrecio = +value;
        switch (value) {
            case 0:
                this.rangoPrecioStr = 'Cualquiera';
                break;
            case 1:
                this.rangoPrecioStr = this.rangoPrecio1;
                break;
            case 2:
                this.rangoPrecioStr = this.rangoPrecio2;
                break;
            case 3:
                this.rangoPrecioStr = this.rangoPrecio3;
                break;
        }
    }

    updateFranja(value) {
        this.franjaHoraria = +value;
        switch (value) {
            case '0':
                this.franjaHorariaStr = 'Cualquiera';
                break;
            case '1':
                this.franjaHorariaStr = 'Mañana 6:01 a 12:00';
                break;
            case '2':
                this.franjaHorariaStr = 'Tarde 12:01 a 18:00';
                break;
            case '3':
                this.franjaHorariaStr = 'Noche 18:01 a 24:00';
                break;
            case '4':
                this.franjaHorariaStr = 'Madrugada 0:01 a 6:00';
                break;
        }
    }

    onItemSelect(item: any) {
        this.onChange();
    }

    OnItemDeSelect(item: any) {
        this.onChange();
    }

    onSelectAll(items: any) {
        this.onChange();
    }

    onDeSelectAll(items: any) {
        this.onChange();
    }

    onChange() {
        this.dataProfesionalesFilt = this.dataProfesionales.filter(profesional =>
            this.testNombre(profesional.nombre + profesional.apellido + profesional.titulo) &&
            this.testSexo(profesional.idsexo) &&
            this.testEdad(profesional.fecnacimiento) &&
            this.testPrecio(profesional.precio) &&
            this.testEspecialidades(profesional.especialidades) &&
            this.testFranjaHoraria(profesional.id)
        );
        this.processProfesionales(false);
        const filtros = new FiltroUsado();
        filtros.sexo = (this.varon || this.mujer) ? 1 : 0;
        filtros.edad = (this.rangoEdadValue > 0) ? 1 : 0;
        filtros.especialidad = (this.selectedItems.length !== this.dataEspecialidades.length) ? 1 : 0;
        filtros.horario = (this.franjaHoraria > 0) ? 1 : 0;
        this.filtroUsadoService.save(filtros);
    }

    agendarSesion(id: string) {
        this.router.navigate(['/pacsolicitudsesion', id]);
    }

    showProfesional(id) {
        this.router.navigate(['/pacprofesional', id]);
    }

    /** Funciones que asisten al filtrado */
    testNombre(nombre): boolean {
        return (nombre.search(new RegExp(this.nombre, 'i')) > -1);
    }

    testSexo(idsexo): boolean {
        if (this.varon && this.mujer) {
            return true;
        }
        if (this.varon && !this.mujer && +idsexo === 1) {
            return true;
        }
        if (!this.varon && this.mujer && +idsexo === 2) {
            return true;
        }
        return false;
    }

    testEdad(fecnacimiento) {
        const edad = this.calculateAge(fecnacimiento);
        switch (+this.rangoEdadValue) {
            case 0:
                return true;
            case 1:
                return (edad >= 20 && edad <= 35);
            case 2:
                return (edad >= 36 && edad <= 50);
            case 3:
                return (edad > 50);
        }
    }

    testPrecio(precio): boolean {
        switch (+this.rangoPrecio) {
            case 0:
                return true;
            case 1:
                return (+precio < (this.rangoPrecio1Value + 1));
            case 2:
                return (+precio > this.rangoPrecio1Value && +precio < (this.rangoPrecio2Value + 1));
            case 3:
                return (+precio > this.rangoPrecio2Value);
        }
    }

    testEspecialidades(especialidades): boolean {
        const espsProf = especialidades.split(';');
        if (this.selectedItems.length === this.dataEspecialidades.length) {
            return true;
        }
        let array1;
        espsProf.forEach((element) => {
            array1 = this.selectedItems.filter((element1) => {
              return element1.id === element.trim();
            });
        });
        if (array1.length > 0) {
            return true;
        }
        return false;
    }

    testFranjaHoraria(idprofesional): boolean {
        let disps;
        switch (+this.franjaHoraria) {
            case 0:
                return true;
            case 1:
                disps = this.dataDisponibilidades.filter(disponibilidad => {
                    return ((disponibilidad.idprofesional === idprofesional) && this.isInRange(6, 12, disponibilidad.inicio));
                });
                break;
            case 2:
                disps = this.dataDisponibilidades.filter(disponibilidad => {
                    return ((disponibilidad.idprofesional === idprofesional) && this.isInRange(12, 18, disponibilidad.inicio));
                });
                break;
            case 3:
                disps = this.dataDisponibilidades.filter(disponibilidad => {
                    return ((disponibilidad.idprofesional === idprofesional) && this.isInRange(18, 24, disponibilidad.inicio));
                });
                break;
            case 4:
                disps = this.dataDisponibilidades.filter(disponibilidad => {
                    return ((disponibilidad.idprofesional === idprofesional) && this.isInRange(0, 6, disponibilidad.inicio));
                });
                break;
        }
        return (disps.length > 0);
    }

    /** Funciones auxiliares */
    calculateAge(fecnacimiento) {
        if (fecnacimiento) {
            const date = new Date(fecnacimiento);
            const timeDiff = Math.abs(Date.now() - date.getTime());
            return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
        }
        return 0;
    }

    isInRange(desde: number, hasta: number, date: any): boolean {
        const dt = new Date(date);
        return (dt.getHours() > desde && dt.getHours() <= hasta);
    }

    showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
              .size('sm')
              .showClose(false)
              .isBlocking(true)
              .title(this.modalTitle)
              .body(msg)
              .okBtn('Aceptar')
              .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
