import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { SafePipe } from '../shared/formatters/safe-pipe';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { AvatarModule } from 'ngx-avatar';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

// relative import
import { PacProfesionalesRoutingModule } from './pacprofesionales-routing.module';
import { PacProfesionalesComponent } from './pacprofesionales.component';

@NgModule({
    imports: [
        CommonModule,
        PacProfesionalesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule.forRoot(),
        Ng2CompleterModule,
        ImageCropperModule,
        AngularMultiSelectModule,
        FileUploadModule,
        AvatarModule
    ],
    declarations: [
        PacProfesionalesComponent
    ]
})
export class PacProfesionalesModule { }
