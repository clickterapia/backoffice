import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PacProfesionalesComponent } from './pacprofesionales.component';

const routes: Routes = [
    {
        path: '',
        component: PacProfesionalesComponent,
        data: {
            title: 'Profesionales',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pac'},
                {title: 'Perfil'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PacProfesionalesRoutingModule { }
