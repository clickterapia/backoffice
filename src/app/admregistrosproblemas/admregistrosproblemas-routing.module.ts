import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmRegistrosProblemasComponent } from './admregistrosproblemas.component';

const routes: Routes = [
    {
        path: '',
        component: AdmRegistrosProblemasComponent,
        data: {
            title: 'Registros de problemas',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-adm'},
                {title: 'Registro de problemas'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdmRegistrosProblemasRoutingModule { }
