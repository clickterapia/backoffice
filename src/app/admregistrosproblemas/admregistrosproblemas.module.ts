import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

import { AdmRegistrosProblemasComponent } from './admregistrosproblemas.component';
import { AdmRegistrosProblemasRoutingModule } from './admregistrosproblemas-routing.module';
import { ProblemaComponent } from './components';

registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    Ng2SmartTableModule,
    AdmRegistrosProblemasRoutingModule
  ],
  declarations: [
    AdmRegistrosProblemasComponent,
    ProblemaComponent ],
  exports: [AdmRegistrosProblemasComponent]
})
export class AdmRegistrosProblemasModule {}
