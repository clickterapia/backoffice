import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import {
    NgbDateCustomParserFormatter, TWUtils,
    Problema, ProblemaService } from '../../../shared';

@Component({
    selector: 'app-problema',
    templateUrl: './problema.component.html',
    providers: [
        DatePipe,
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        Problema,
        ProblemaService ]
})

export class ProblemaComponent implements OnInit, OnChanges {
    @Input() selectedEntity: Problema;
    @Output() entityUpdated = new EventEmitter();

    readonly MESSAGE_STAYHERE = 0;
    readonly MESSAGE_GOTO = 1;
    closeResult: string;
    public modalMessage: string;
    public modalTitle = 'Problema';

    public dataEstados = [
        { 'id': '1', 'estado': 'Pendiente'},
        { 'id': '2', 'estado': 'Revisado'},
        { 'id': '3', 'estado': 'Tratado'}];

    public selectedID: string;
    public fecInicio: {};

    public constructor(public router: Router, private route: ActivatedRoute,
        public modal: Modal,
        private datepipe: DatePipe,
        private problemaService: ProblemaService) {
        this.selectedEntity = new Problema();
    }

    public ngOnInit(): void {
        this.cancelar();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        if (changes['selectedEntity'].currentValue !== null) {
            this.selectedEntity = changes['selectedEntity'].currentValue;
            if (this.selectedEntity.fecinicio !== undefined) {
                this.fecInicio = this.datepipe.transform(this.selectedEntity.fecinicio, 'dd-MM-yyyy HH:mm');
            }
        }
    }

    public onSave() {
        this.problemaService.save(this.selectedEntity).then(
            response => {
                if (response) {
                    this.cancelar();
                    this.entityUpdated.emit(true);
                } else {
                    this.showMessage('No se han podido guardar los datos del problema!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public cancelar() {
        this.selectedEntity = new Problema();
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
                        .size('sm')
                        .showClose(false)
                        .isBlocking(true)
                        .title(this.modalTitle)
                        .body(msg)
                        .okBtn('Aceptar')
                        .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
