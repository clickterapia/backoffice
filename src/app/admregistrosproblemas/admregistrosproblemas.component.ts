import {
    Component, OnInit,
    TemplateRef,
    ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DatePipe } from '@angular/common';
import { CurrencyAR, NgbDateCustomParserFormatter, TWUtils, Problema } from '../shared';
import { LOCALE_ID } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ProblemaService } from '../shared';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ProblemaComponent } from './components';

@Component({
    selector: 'app-admregistrosproblemas',
    // styleUrls: ['./pacsesiones.component.scss'],
    templateUrl: './admregistrosproblemas.component.html',
    providers: [
        DatePipe,
        { provide: LOCALE_ID, useValue: 'es-AR' },
        {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
        CurrencyAR,
        ProblemaService
    ]
})
export class AdmRegistrosProblemasComponent implements OnInit, AfterViewChecked {
    public modalTitle = 'Registros de Problemas';
    modalContent: TemplateRef<any>;

    selectedEntity: any;

    dataProblemas: Array<any> = Array<any>();

    settings = {
        noDataMessage: 'Datos no disponibles',
        pager: { display: true, perPage: 10 },
        columns: {
            fecultmodif: {
                title: 'Fecha y hora', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + this.datePipe.transform(value, 'dd-MM-yyyy HH:mm') + '</div>';
                }
            },
            tipoorigen: { 
                title: 'Origen', filter: false,
                type: 'html',
                valuePrepareFunction: (value) => {
                    return '<div class="cell_center">' + ((value === 1) ? 'Paciente' : 'Profesional') + '</div>';
                } },
            emisor: { title: 'Emisor', filter: false, },
            problematipo: { title: 'Tipo de problema', filter: false, },
            problema: { title: 'Problema', filter: false, },
            problemaestado: { title: 'Estado', filter: false, }
        },
        actions: { add: false, edit: false, delete: false, },
        attr: {
            class: 'table dataTable table-striped table-bordered',
        }
    };
    source: LocalDataSource;
    selectedID;

    public constructor(public router: Router,
        private route: ActivatedRoute,
        public modal: Modal,
        private cdRef: ChangeDetectorRef,
        private datePipe: DatePipe,
        private problemaService: ProblemaService) {
            const date = new Date();
            this.selectedEntity = new Problema();
    }

    public ngOnInit(): void {
        this.selectedID = localStorage.getItem('user');
        this.getProblemas();
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    handleItemUpdated(user) {
        this.getProblemas();
    }

    getProblemas(): any {
        return this.problemaService.getNoTratados().then(
            response => {
                if (response) {
                    this.dataProblemas = response;
                    this.source = new LocalDataSource(this.dataProblemas);
                } else {
                    this.showMessage('No se ha podido obtener datos de problemas!!');
                }
            }
        ).catch(e => this.handleError(e));
    }

    public userRowSelect(event: any): any {
        this.selectedEntity = event.data as Problema;
    }

    onSearch(query: string) {
        if (query.length === 0) {
            this.source.setFilter([]);
            return;
        }

        this.source.setFilter([
            { field: 'fecultmodif', search: query },
            { field: 'horasesion', search: query },
            { field: 'tipoorigen', search: query },
            { field: 'emisor', search: query },
            { field: 'problematipo', search: query },
            { field: 'problema', search: query },
            { field: 'problemaestado', search: query }
        ], false);
    }

    showMessage(message) {
        const msg = `<p class="font-weight-normal">` + message + `</p>`;
        const dialogRef = this.modal.alert()
            .size('sm')
            .showClose(false)
            .isBlocking(true)
            .title(this.modalTitle)
            .body(msg)
            .okBtn('Aceptar')
            .open();
    }

    private handleError(error: any): Promise<any> {
        this.showMessage('Error: ' + error);
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
