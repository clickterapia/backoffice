import { Component, OnInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { Recurso, RecursoService } from '../shared';

@Component({
  selector: 'app-procapacitacion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./procapacitacion.component.scss'],
  templateUrl: './procapacitacion.component.html',
  providers: [
    RecursoService
  ]
})
export class ProCapacitacionComponent implements OnInit {
  public modalTitle = 'Capacitación';
  modalContent: TemplateRef<any>;

  prevBtnDisabled = false;

  dataRecursos: Array<any> = Array<any>();
  dataPrimeros: Array<any> = Array<any>();
  dataComerciales: Array<any> = Array<any>();
  dataTecnicos: Array<any> = Array<any>();
  safeURL: any;

  public constructor(public router: Router,
      private cdr: ChangeDetectorRef,
      private sanitizer: DomSanitizer,
      public modal: Modal,
      private recursoService: RecursoService) {
        this.safeURL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube-nocookie.com/embed/y9ANOzmSKQg?rel=0');
  }

  public ngOnInit(): void {
      this.getRecursos();
  }

  getRecursos() {
      return this.recursoService.getList().then(
          response => {
              if (response) {
                this.dataRecursos = response;
                this.filterData();
              } else {
                this.showMessage('No se han podido obtener datos de recursos!!');
              }
          }
      ).catch(e => this.handleError(e));
  }

  filterData(): any {
    this.dataPrimeros = this.dataRecursos.filter(data => +data.video === 1 && +data.primeros === 1);
    this.dataComerciales = this.dataRecursos.filter(data => +data.comerciales === 1);
    this.dataTecnicos =  this.dataRecursos.filter(data => +data.tecnicos === 1);
    this.cdr.detectChanges();
  }

  getSafeURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  showMessage(message) {
      const msg = `<p class="font-weight-normal">` + message + `</p>`;
      const dialogRef = this.modal.alert()
                      .size('sm')
                      .showClose(false)
                      .isBlocking(true)
                      .title(this.modalTitle)
                      .body(msg)
                      .okBtn('Aceptar')
                      .open();
  }

  private handleError(error: any): Promise<any> {
      this.showMessage('Error: ' + error);
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
  }
}
