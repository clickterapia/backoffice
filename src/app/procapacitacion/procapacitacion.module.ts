import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEs from '@angular/common/locales/es';
import { ContextMenuModule } from 'ngx-contextmenu';

import { ProCapacitacionComponent } from './procapacitacion.component';
import { ProCapacitacionRoutingModule } from './procapacitacion-routing.module';

registerLocaleData(localeFr);
registerLocaleData(localeEs);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ProCapacitacionRoutingModule,
    ContextMenuModule.forRoot({
      useBootstrap4: true
    })
  ],
  declarations: [
    ProCapacitacionComponent],
  exports: [ProCapacitacionComponent]
})
export class ProCapacitacionModule {}
