import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProCapacitacionComponent } from './procapacitacion.component';

const routes: Routes = [
    {
        path: '',
        component: ProCapacitacionComponent,
        data: {
            title: 'Recursos de Capacitacion',
            urls: [
                {title: 'Panel de Control', url: '/dashboard/dashboard-pro'},
                {title: 'Capacitacion'}
            ]
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProCapacitacionRoutingModule { }
