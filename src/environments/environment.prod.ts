export const environment = {
  production: true,
  apiUrl: 'https://clickterapia.com/' + 'admin/api/',
  mpUrl: 'https://clickterapia.com/' + 'mercadopago/',
  imagestock: 'https://clickterapia.com/imgstock/',
  callbackURL: 'https://clickterapia.com/callback',
  urlMap: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBiDmX5nFGBGp-XYjYNhmC8iQWTxHz7-Sg&q=',
  apiMercadoPago: {
    checkOutBaseUrl: 'https://clickterapia.com/mercadopago/checkout.php?idpaciente=[idpaciente]&idprofesional=[idprofesional]' ,
    appID: '5722701415640392',
    secretKey: 'gRGKaNKsL2o0F8LdPL2rwUjtNXWSxgTo',
    linkAccount: 'https://auth.mercadopago.com.ar/authorization?' +
            'client_id=[appID]' +
            '&response_type=code' +
            '&platform_id=mp' +
            '&redirect_uri=[redirect_uri]',
    redirect_uri: 'https://clickterapia.com/mercadopago/marketplace.php?idprofesional=[idprofesional]&precio=[precio]',
  },
  authConfig: {
    clientID: 'a9OWiPxe9CurgcQ3pw8diGA49MnuSFv2',
    domain: 'clickterapia.auth0.com',
    callbackURL: 'https://clickterapia.com/admin/#/callback'
  }
};
