// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'http://192.168.1.24/' + 'api-clickterapia/',
  mpUrl: 'http://192.168.1.24/' + 'clickterapia-mp/',
  imagestock: 'http://192.168.1.24/itherapy/imgs/',
  callbackURL: 'http://192.168.1.24/callback',
  urlMap: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyBiDmX5nFGBGp-XYjYNhmC8iQWTxHz7-Sg&q=',
  apiMercadoPago: {
    checkOutBaseUrl: 'https://clickterapia.com/mercadopago/checkout.php?idpaciente=[idpaciente]&idprofesional=[idprofesional]' ,
    appID: '1679477082442866',
    secretKey: 'jMuqG5qMLcpV7bgEEgg2gnxrtXw2aqVx',
    linkAccount: 'https://auth.mercadopago.com.ar/authorization?' +
            'client_id=[appID]' +
            '&response_type=code' +
            '&platform_id=mp' +
            '&redirect_uri=[redirect_uri]',
    redirect_uri: 'https://clickterapia.com/mercadopago/marketplace.php?idprofesional=[idprofesional]&precio=[precio]',
  },
  authConfig: {
    clientID: 'a9OWiPxe9CurgcQ3pw8diGA49MnuSFv2',
    domain: 'clickterapia.auth0.com',
    callbackURL: 'http://localhost:4200/callback'
  }
};
